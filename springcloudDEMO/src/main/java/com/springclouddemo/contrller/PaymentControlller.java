package com.springclouddemo.contrller;

import com.springclouddemo.frontResultData.CommonResult;
import com.springclouddemo.pojo.Payment;
import com.springclouddemo.server.PaymentServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentControlller {

    @Autowired
    private PaymentServer paymentServer;


    @PostMapping("/createPayment")
    public CommonResult<Integer> craetePayment(Payment payment)
    {
        int i = paymentServer.create(payment);
        if (i!=0)
        {
            return new CommonResult<>(200,"数据库插入成功",i);
        }
        else
        {
            return new CommonResult<>(444,"数据库插入失败",null);
        }
    }

    @GetMapping("/selectPayment/{id}")
    public CommonResult<Payment> selectPayment(@PathVariable("id") Long id)
    {
        Payment payment = paymentServer.selectPaymentById(id);
        if (payment!=null)
        {
            return new CommonResult<>(200,"查询成功",payment);
        }
        else
        {
            return new CommonResult<>(444,"查询失败,查询ID:"+id,null);
        }
    }
}
