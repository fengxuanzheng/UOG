package com.springclouddemo.dao;

import com.springclouddemo.pojo.Payment;

public interface PaymentDAO {

    public int create(Payment payment);
    public Payment getPaymentById( Long id);
}
