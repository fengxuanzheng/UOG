package algorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class HuaweiAlgorithm {

    public static void hj8(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int nextInt = Integer.parseInt(scanner.next());
        int i = 0;
        HashMap<Integer, Integer> integerIntegerIdentityHashMap = new HashMap<>();
        while (scanner.hasNext()) {
            int next = scanner.nextInt();
            int next1 = scanner.nextInt();
            Integer integer = integerIntegerIdentityHashMap.putIfAbsent(next, next1);
            if (integer != null) {
                integerIntegerIdentityHashMap.put(next, integer + next1);
            }
            if (++i >= nextInt) {
                break;
            }
        }
        integerIntegerIdentityHashMap.entrySet().stream().sorted((item1, item2) -> {
            Integer key = item1.getKey();
            Integer key1 = item2.getKey();
            if (key.equals(key1)) {
                return 0;
            }
            return key > key1 ? 1 : -1;
        }).forEach(item -> {
            System.out.printf("%d %d\n", item.getKey(), item.getValue());
        });


    }

    public static void hj9(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();
        if (next.charAt(next.length() - 1) == '0') {
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        char[] chars = next.toCharArray();
        for (int i = chars.length - 1; i >= 0; i--) {
            if (stringBuilder.indexOf(String.valueOf(chars[i])) == -1) {

                stringBuilder.append(chars[i]);
            }
        }
        System.out.println(stringBuilder.toString());
    }

    public static void hj10(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();
        char[] chars = next.toCharArray();
        HashSet<Character> stringArrayList = new HashSet<>();
        for (char ab:chars)
        {
            stringArrayList.add(ab);
        }
        System.out.println(stringArrayList.size());
        //第二种方法
        String line = scanner.next();
        //总共有128个字符。字需要用128位
        BitSet bitSet = new BitSet(128);
        for (char c : line.toCharArray()) {
            //判断字符c是否已出现
            if (!bitSet.get(c)) {
                //未出现就设置为已出现
                bitSet.set(c);
            }
        }
        //统计有多少字符已出现过
        System.out.println(bitSet.cardinality());
    }

    public static void hj11(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();
        char[] chars = next.toCharArray();
        Arrays.sort(chars);
        StringBuilder stringBuilder = new StringBuilder();
        for (char item:chars)
        {
            stringBuilder.append(item);
        }
        System.out.println(stringBuilder.reverse().toString());
    }

    public static void hj12(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();
        if (!next.equals(next.toLowerCase()))
        {
            return;
        }
        System.out.println(new StringBuffer(next).reverse().toString());
    }

    public static void hj13(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String next = scanner.nextLine();
        String[] split = next.split(" ");
        Collections.reverse(Arrays.asList(split));
        Arrays.stream(split).forEach(item->System.out.print(item+" "));
    }

    public static void hj14(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        ArrayList<String> strings = new ArrayList<>();
        for (int j=0;j<i;j++)
        {
            strings.add(scanner.next());
        }
        Collections.sort(strings);
        strings.forEach(System.out::println);
    }

    public static void hj15(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int sum=num & Integer.MAX_VALUE;
        String toString = Integer.toString(sum, 2);
        int count=0;
        for (char c : toString.toCharArray()) {
            if (c=='1')
            {
                count++;
            }
        }
        System.out.println(count);
        //另外一种解法
        Scanner sc = new Scanner(System.in);
        int num2 = sc.nextInt();
        String str = Integer.toBinaryString(num2);
        int length = str.length();
        String newStr = str.replaceAll("1", "");
        System.out.println(length - newStr.length());
    }

    public static void hj16(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int int2 = scanner.nextInt();
        ArrayList<Goods> integerArrayListHashMap = new ArrayList<>(int2);
        for (int a=0;a<int2;a++)
        {
            integerArrayListHashMap.add(new Goods());
        }
        for (int i = 0; i<int2; i++)
        {
            int price = scanner.nextInt();
            int impr = scanner.nextInt();
            int isMain = scanner.nextInt();
            if (isMain==0)
            {
                Goods goods1 = integerArrayListHashMap.get(i);

                    goods1.setMain(true);
                    goods1.setV(price);
                    goods1.setP(price*impr);

            }
            else
            {
                Goods goods3 = integerArrayListHashMap.get(i);
                goods3.setV(price);
                goods3.setP(price*impr);
                Goods goods1 = integerArrayListHashMap.get(isMain-1);

                if (goods1.getA1()==-1)
                {
                    goods1.setA1(i);
                }
               else
                {
                    goods1.setA2(i);
                }
            }
        }
        int[][] dp = new int[int2+ 1][count + 1];
        for (int i=1;i<dp.length;i++)
        {
            for (int j = 0; j < dp[0].length; j++) {
                dp[i][j] = dp[i - 1][j];
                if (!integerArrayListHashMap.get(i - 1).isMain()) {
                    continue;
                }
                if (j >= integerArrayListHashMap.get(i - 1).v) {
                    dp[i][j] = Math.max(dp[i][j], integerArrayListHashMap.get(i - 1).p + dp[i - 1][j - integerArrayListHashMap.get(i - 1).v]);
                }
                if (integerArrayListHashMap.get(i - 1).a1 != -1 && j >= integerArrayListHashMap.get(i - 1).v + integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a1).v) {
                    dp[i][j] = Math.max(dp[i][j], integerArrayListHashMap.get(i - 1).p + dp[i - 1][j - integerArrayListHashMap.get(i - 1).v - integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a1).v] + integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a1).p);
                }
                if (integerArrayListHashMap.get(i - 1).a2 != -1 && j >= integerArrayListHashMap.get(i - 1).v + integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a2).v) {
                    dp[i][j] = Math.max(dp[i][j], integerArrayListHashMap.get(i - 1).p + dp[i - 1][j - integerArrayListHashMap.get(i - 1).v - integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a2).v] + integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a2).p);
                }
                if (integerArrayListHashMap.get(i - 1).a1 != -1 && integerArrayListHashMap.get(i - 1).a2 != -1 && j >= integerArrayListHashMap.get(i - 1).v + integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a1).v + integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a2).v) {
                    dp[i][j] = Math.max(dp[i][j], integerArrayListHashMap.get(i - 1).p + dp[i - 1][j - integerArrayListHashMap.get(i - 1).v - integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a1).v - integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a2).v] + integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a1).p + integerArrayListHashMap.get(integerArrayListHashMap.get(i - 1).a2).p);
                }


            }
        }
        System.out.println(dp[int2][count]);

    }
   static class Goods {
       private int v;
       private int p;
       private boolean main = false;

       private int a1 = -1;  //定义附件1的编号
       private int a2 = -1;  //定义附件2的编号

       public Goods(int v, int p, boolean main) {
           this.v = v;
           this.p = p;
           this.main = main;
       }

       public Goods() {
       }

       public Goods(int v, int p, boolean main, int a1, int a2) {
           this.v = v;
           this.p = p;
           this.main = main;
           this.a1 = a1;
           this.a2 = a2;
       }

       @Override
       public String toString() {
           return "Goods{" +
                   "v=" + v +
                   ", p=" + p +
                   ", main=" + main +
                   ", a1=" + a1 +
                   ", a2=" + a2 +
                   '}';
       }

       public int getV() {
           return v;
       }

       public void setV(int v) {
           this.v = v;
       }

       public int getP() {
           return p;
       }

       public void setP(int p) {
           this.p = p;
       }

       public boolean isMain() {
           return main;
       }

       public void setMain(boolean main) {
           this.main = main;
       }

       public int getA1() {
           return a1;
       }

       public void setA1(int a1) {
           this.a1 = a1;
       }

       public int getA2() {
           return a2;
       }

       public void setA2(int a2) {
           this.a2 = a2;
       }
   }

    public static void hj17(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String nextLine = scanner.nextLine();
        String[] split = nextLine.split(";");
        List<String> stringList = Arrays.asList(split).stream().filter(item -> item.matches("[WASD][0-9]{1,2}")).collect(Collectors.toList());
        int[] point = {0, 0};
        //Point point = new Point(0,0);
        stringList.forEach(item->{
            if ("A".equalsIgnoreCase(item.substring(0,1)))
            {
                point[0]= point[0]-Integer.parseInt(item.substring(1));
            }
            else if ("D".equalsIgnoreCase(item.substring(0, 1)))
            {
                point[0]= point[0]+Integer.parseInt(item.substring(1));
            }
            else if ("W".equalsIgnoreCase(item.substring(0, 1)))
            {
                point[1]= point[1]+ Integer.parseInt(item.substring(1));
            }
            else
            {
                point[1]=  point[1]-Integer.parseInt(item.substring(1));
            }
        });
        System.out.println(point[0]+","+point[1]);
    }

    public static void hj18(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] ints = new int[7];
      out:while (scanner.hasNextLine())
        {
            String nextLine = scanner.nextLine();
            String[] strings = nextLine.split("~");
            String[] split = strings[0].split("\\.");
            String[] strings1 = strings[1].split("\\.");
            if (!split[0].equalsIgnoreCase(""))
            {
                Integer first = Integer.parseInt(split[0]);
                if (first==0 ||first==127)
                {
                    continue ;
                }

            }
            for (String item:split)
            {

                if ("".equalsIgnoreCase(item) || Integer.parseInt(item)<0 || Integer.parseInt(item)>255)
                {
                    ints[5]++;
                    continue out;
                }

            }
            StringBuilder stringBuilder = new StringBuilder();
            for (String value:strings1)
            {
                String toBinaryString = Integer.toBinaryString(Integer.parseInt(value));
                if (toBinaryString.length()<8)
                {
                    int add=8-toBinaryString.length();
                    while (add>0)
                    {
                        stringBuilder.append("0");
                        add--;
                    }
                    stringBuilder.append(toBinaryString);
                }
                else
                {
                    stringBuilder.append(toBinaryString);
                }
            }
            if (stringBuilder.lastIndexOf("1")>stringBuilder.indexOf("0"))
            {
                ints[5]++;
                continue ;
            }
            Integer one = Integer.parseInt(split[0]);
            Integer two = Integer.parseInt(split[1]);
            if (one>=1 && one<=126)
            {
                ints[0]++;
            }
            else if (one>=128 && one<=191)
            {
                ints[1]++;
            }
            else if (one>=192 && one<=223)
            {
                ints[2]++;
            }
            else if (one>=224 && one<=239)
            {
                ints[3]++;
            }
            else if (one>=240 && one<=255)
            {
                ints[4]++;
            }
            if (one==10 || (one==172 && two>=16 && two<=31) ||(one==192 && two==168))
            {
                ints[6]++;
            }
        }
        Arrays.stream(ints).forEach(item->System.out.print(item+" "));
    }

    public static void hj19(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LinkedHashMap<String, Integer> integerHashMap = new LinkedHashMap<>(8);
        while (scanner.hasNextLine())
        {
            String nextLine = scanner.nextLine();
            String[] nextSplit = nextLine.split(" ");
            String[] nextFirst = nextSplit[0].split("\\\\");
            String name = nextFirst[nextFirst.length - 1];
            name=name.length()<=16?name:name.substring(name.length()-16);
           String key =  name+ " " + nextSplit[1];
            integerHashMap.put(key,integerHashMap.get(key)!=null?integerHashMap.get(key)+1:1);

        }
        int open=integerHashMap.size()<=8?integerHashMap.size():integerHashMap.size()-8;
        List<Map.Entry<String, Integer>> collect = integerHashMap.entrySet().stream().collect(Collectors.toList());
        if (open==collect.size())
        {
            collect.forEach(item->System.out.println(item.getKey()+" "+item.getValue()));
        }
        else
        {
            List<Map.Entry<String, Integer>> subList = collect.subList(open, collect.size());

            subList.forEach(item->System.out.println(item.getKey()+" "+item.getValue()));
        }
    }

    public static void hj20(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine())
        {
            String nextLine = scanner.nextLine();
            int isType=0;
            boolean isLow=false;
            boolean isUp=false;
            boolean isNum=false;
            boolean isNoNumChar=false;
            boolean isSame=false;
            boolean isFirstSame=false;
            if (nextLine.length()<8)
            {
                System.out.println("NG");
                continue;
            }
            for (int i=0;i<nextLine.length();i++)
            {
               if (Character.isLowerCase(nextLine.charAt(i)))
               {
                   if (!isLow)
                   {
                       isType++;
                   }
                   isLow=true;
               }
               else if (Character.isDigit(nextLine.charAt(i)))
               {
                   if (!isNum)
                   {
                       isType++;
                   }
                   isNum=true;
               }
               else if (Character.isUpperCase(nextLine.charAt(i)))
               {
                   if (!isUp)
                   {
                       isType++;
                   }
                   isUp=true;
               }
               else if (!Character.isLetterOrDigit(nextLine.charAt(i)))
               {
                   if (!isNoNumChar)
                   {
                       isType++;
                   }
                   isNoNumChar=true;
               }
               if (isType>=3)
               {
                   break;
               }
            }
            if (isType<3)
            {
                System.out.println("NG");
                continue;
            }
            boolean string = getString(nextLine, 0, 3);
            System.out.println(string?"NG":"OK");
        }


    }
    private static boolean getString(String str, int l, int r) {
        if (r >= str.length()) {
            return false;
        }
        if (str.substring(r).contains(str.substring(l, r))) {
            return true;
        } else {
            return getString(str,l+1,r+1);
        }
    }

    public static void hj21(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LinkedHashMap<String, Integer> stringIntegerHashMap = new LinkedHashMap<>();
        stringIntegerHashMap.put("1", 1);
        stringIntegerHashMap.put("abc",2);
        stringIntegerHashMap.put("def", 3);
        stringIntegerHashMap.put("ghi", 4);
        stringIntegerHashMap.put("jkl", 5);
        stringIntegerHashMap.put("mno", 6);
        stringIntegerHashMap.put("pqrs", 7);
        stringIntegerHashMap.put("tuv", 8);
        stringIntegerHashMap.put("wxyz", 9);
        stringIntegerHashMap.put("0",0);
        StringBuilder stringBuilder = new StringBuilder();
        while (scanner.hasNextLine())
        {
            String nextLine = scanner.nextLine();
            for (int i=0;i<nextLine.length();i++)
            {
                boolean isSpilt=false;
                char charAt = nextLine.charAt(i);
                if (Character.isDigit(charAt))
                {
                    stringBuilder.append(charAt);
                    continue;
                }
                for (String s : stringIntegerHashMap.keySet()) {
                    if (isSpilt)
                    {
                        stringBuilder.append(s.charAt(0));
                        break;
                    }
                    if (Character.isLowerCase(charAt)) {
                        if (s.contains(String.valueOf(charAt))) {
                            Integer integer = stringIntegerHashMap.get(s);
                            stringBuilder.append(integer);
                            break;
                        }
                    }
                    else
                    {
                        if (s.contains(String.valueOf(charAt).toLowerCase()))
                        {
                            int indexOf = s.indexOf(String.valueOf(charAt).toLowerCase());
                            if (indexOf!=s.length()-1)
                            {
                                stringBuilder.append(s.charAt(indexOf+1));
                            }
                            else
                            {
                                if (stringIntegerHashMap.get(s)==9)
                                {
                                    stringBuilder.append('a');
                                }
                                else
                                {
                                  isSpilt=true;
                                }

                            }
                        }
                    }
                }
            }
            System.out.println(stringBuilder);
        }
    }

    public static void hj22(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input;
        while ((input=scanner.nextInt())!=0)
        {
            System.out.println((int) input/2);
        }
    }

    public static void hj23(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine())
        {
            HashMap<Character, Integer> integerHashMap = new HashMap<>();
            String nextLine = scanner.nextLine();
            for (int i=0;i<nextLine.length();i++)
            {
                integerHashMap.put(nextLine.charAt(i),integerHashMap.get(nextLine.charAt(i))!=null?integerHashMap.get(nextLine.charAt(i))+1:1);
            }
            List<Map.Entry<Character, Integer>> entries = integerHashMap.entrySet().stream().sorted((key1, key2) ->
            {
                Integer value = key1.getValue();
                Integer value1 = key2.getValue();
                if (value.equals(value1)) {
                    return 0;
                }
                return value > value1 ? 1 : -1;
            }).collect(Collectors.toList());
            int count=0;
            Integer integer = entries.get(0).getValue();
           for (Map.Entry<Character, Integer> entry:entries)
           {
               if (entry.getValue().equals(integer))
               {
                   count++;
               }
               else
               {
                   break;
               }
           }
            for (int i=0;i<count;i++)
            {
                nextLine=nextLine.replace(String.valueOf(entries.get(i).getKey()), "");
            }
            System.out.println(nextLine);
        }
    }

    public static void hj24(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i=0;i<scanner.nextInt();i++)
        {
            integers.add(scanner.nextInt());
        }

    }

    public static void hj25(String[] args) {
        LinkedHashMap<Integer, ArrayList<Integer>> arrayListHashMap = new LinkedHashMap<>();
        ArrayList<Integer> integers = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int icount=scanner.nextInt();
        List<Integer> iArray = new ArrayList<>(icount);
        for (int i=0;i<icount;i++)
        {
            iArray.add(scanner.nextInt());
        }
        int rcount = scanner.nextInt();
        List<Integer> rArray = new ArrayList<>();
        for (int i=0;i<rcount;i++)
        {
            rArray.add(scanner.nextInt());
        }

       rArray= rArray.stream().distinct().sorted().collect(Collectors.toList());
        for (Integer r:rArray)
        {
            arrayListHashMap.put(r, null);
            ArrayList<Integer> render = new ArrayList<>();
            for (int i=0;i<iArray.size();i++)
            {
                if (String.valueOf(iArray.get(i)).contains(String.valueOf(r)))
                {
                    render.add(i);
                    render.add(iArray.get(i));
                }
            }
            if (render.size() != 0)
            {
                arrayListHashMap.put(r, render);
            }
        }
        for (Map.Entry<Integer, ArrayList<Integer>> integerArrayListEntry : arrayListHashMap.entrySet()) {
            if (integerArrayListEntry.getValue()!=null)
            {
                integers.add(integerArrayListEntry.getKey());
                ArrayList<Integer> value = integerArrayListEntry.getValue();
                integers.add(value.size()/2);
                integers.addAll(value);

            }
        }
        System.out.print(integers.size()+" ");
        integers.forEach(item->{
            System.out.print(item+" ");
        });

    }

    public static void hj26(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine())
        {
            String nextLine = scanner.nextLine();
            List<Character> split =new ArrayList<>();
            for (char chars :nextLine.toCharArray()){
            if (Character.isLetter(chars))
            {
                split.add(chars);
            }
        }
            StringBuilder stringBuilder = new StringBuilder();
           /* split.sort(Comparator.comparingInt(Character::toLowerCase));
            for (int i=0,j=0;i<nextLine.length();i++)
            {
                if (Character.isLetter(nextLine.charAt(i)))
                {
                    stringBuilder.append(split.get(j++));
                }
                else
                {
                    stringBuilder.append(nextLine.charAt(i));
                }
            }
            System.out.println(stringBuilder);*/
            //*************************************第二种解法**************************
            TreeMap<String, StringBuilder> treeMap = new TreeMap<>();
            split.forEach(item->{
                String toLowerCase = String.valueOf(item).toLowerCase();
                treeMap.put(toLowerCase, treeMap.get(toLowerCase)!=null?treeMap.get(toLowerCase).append(item):new StringBuilder().append(item));
            });
            StringBuffer buffer = new StringBuffer();
            for (Map.Entry<String, StringBuilder> stringStringBuilderEntry : treeMap.entrySet()) {
                buffer.append(stringStringBuilderEntry.getValue());
            }
            for (int i=0;i<nextLine.length();i++)
            {
                if (!Character.isLetter(nextLine.charAt(i)))
                {
                    buffer.insert(i, nextLine.charAt(i));
                }

            }
            System.out.println(buffer);
        }
    }

    public static void hj27(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int nextInt = scanner.nextInt();
        ArrayList<String> integers = new ArrayList<>(nextInt+2);
        HashMap<Character, Integer> integerHashMap = new HashMap<>();
        List<String> isConfirm = new ArrayList<>();
        for (int i = 0; i < nextInt+2; i++) {
            integers.add(scanner.next());
        }
        String confirm = integers.get(integers.size() - 2);
        for (int i = 0; i < confirm.length(); i++) {
            integerHashMap.put(confirm.charAt(i),integerHashMap.get(confirm.charAt(i))!=null?integerHashMap.get(confirm.charAt(i))+1:1);
        }
        for (int i=0;i<nextInt;i++)
        {
            int length = confirm.length();
            boolean isSuccess=true;
            if (integers.get(i).length()!=length || integers.get(i).equals(confirm))
            {
                continue;
            }
            for (Map.Entry<Character,Integer> entry:integerHashMap.entrySet())
            {
                Character key = entry.getKey();
                Integer value = entry.getValue();
                String replace = integers.get(i).replace(String.valueOf(key), "");
                if (integers.get(i).length()-replace.length()!=value)
                {
                    isSuccess=false;
                    break;
                }
            }
            if (isSuccess)
            {

                isConfirm.add(integers.get(i));
            }
        }
       isConfirm= isConfirm.stream().sorted().collect(Collectors.toList());
        System.out.println(isConfirm.size());
        int i = Integer.parseInt(integers.get(integers.size() - 1)) - 1;
        if (isConfirm.size()>i)
        {

            String s = isConfirm.get(i);
            System.out.println(s);
        }
    }

    public static void hj28(String[] args) {
        //标准输入
        Scanner sc=new Scanner(System.in);
        while(sc.hasNext()){
            //输入正偶数
            int n=sc.nextInt();
            //用于记录输入的n个整数
            int[] arr=new int[n];
            //用于存储所有的奇数
            ArrayList<Integer> odds=new ArrayList<>();
            //用于存储所有的偶数
            ArrayList<Integer> evens=new ArrayList<>();
            for(int i=0;i<n;i++){
                arr[i]=sc.nextInt();
                //将奇数添加到odds
                if(arr[i]%2==1){
                    odds.add(arr[i]);
                }
                //将偶数添加到evens
                if(arr[i]%2==0){
                    evens.add(arr[i]);
                }
            }
            //下标对应已经匹配的偶数的下标，值对应这个偶数的伴侣
            int[] matcheven=new int[evens.size()];
            //记录伴侣的对数
            int count=0;
            for(int j=0;j<odds.size();j++){
                //用于标记对应的偶数是否查找过
                boolean[] v=new boolean[evens.size()];
                //如果匹配上，则计数加1
                if(find(odds.get(j),matcheven,evens,v)){
                    count++;
                }
            }
            System.out.println(count);
        }
    }
    //判断奇数x能否找到伴侣
    private static boolean find(int x,int[] matcheven,ArrayList<Integer> evens,boolean[] v){
        for(int i=0;i<evens.size();i++){
            //该位置偶数没被访问过，并且能与x组成素数伴侣
            if(isPrime(x+evens.get(i))&&v[i]==false){
                v[i]=true;
                /*如果i位置偶数还没有伴侣，则与x组成伴侣，如果已经有伴侣，并且这个伴侣能重新找到新伴侣，
                则把原来伴侣让给别人，自己与x组成伴侣*/
                if(matcheven[i]==0||find(matcheven[i],matcheven,evens,v)){
                    matcheven[i]=x;
                    return true;
                }
            }
        }
        return false;
    }
    //判断x是否是素数
    private static boolean isPrime(int x){
        if(x==1) return false;
        //如果能被2到根号x整除，则一定不是素数
        for(int i=2;i<=(int)Math.sqrt(x);i++){
            if(x%i==0){
                return false;
            }
        }
        return true;
    }

    public static void hj29(String[] args) throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        String input=null;
        int count=0;
        while ((input=br.readLine())!=null)
        {
            count++;
            if (count==1) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < input.length(); i++) {
                    if (Character.isLetter(input.charAt(i)) && Character.isLowerCase(input.charAt(i))) {
                        if (input.charAt(i) != 'z') {
                            int next = input.charAt(i) + 1;
                            stringBuilder.append(String.valueOf((char) next).toUpperCase());
                        } else {
                            stringBuilder.append('A');
                        }
                    } else if (Character.isLetter(input.charAt(i)) && Character.isUpperCase(input.charAt(i))) {
                        if (input.charAt(i) != 'Z') {
                            int next = input.charAt(i) + 1;
                            stringBuilder.append(String.valueOf((char) next).toLowerCase());
                        } else {
                            stringBuilder.append('a');
                        }
                    } else if (Character.isDigit(input.charAt(i))){
                       if (input.charAt(i) != '9')
                       {

                           stringBuilder.append((char)(input.charAt(i)+1));
                       }
                       else
                       {
                           stringBuilder.append('0');

                       }

                    }
                    else
                    {
                        stringBuilder.append(input.charAt(i));
                    }
                }
                System.out.println(stringBuilder);
            }
            else
            {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < input.length(); i++) {
                    if (Character.isLetter(input.charAt(i)) && Character.isLowerCase(input.charAt(i))) {
                        if (input.charAt(i) != 'a') {
                            int next = input.charAt(i) - 1;
                            stringBuilder.append(String.valueOf((char) next).toUpperCase());
                        } else {
                            stringBuilder.append('Z');
                        }
                    } else if (Character.isLetter(input.charAt(i)) && Character.isUpperCase(input.charAt(i))) {
                        if (input.charAt(i) != 'A') {
                            int next = input.charAt(i) - 1;
                            stringBuilder.append(String.valueOf((char) next).toLowerCase());
                        } else {
                            stringBuilder.append('z');
                        }
                    } else if (Character.isDigit(input.charAt(i))){
                        if (input.charAt(i) != '0')
                        {

                            stringBuilder.append((char)(input.charAt(i)-1));
                        }
                        else
                        {
                            stringBuilder.append('9');

                        }

                    }
                    else {
                        stringBuilder.append(input.charAt(i));
                    }
                }
                System.out.println(stringBuilder);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        String readLine = br.readLine();
        String[] strings = readLine.split(" ");
        String sublist = strings[0] + strings[1];
        ArrayList<Character> one = new ArrayList<>();
        ArrayList<Character> two = new ArrayList<>();
        for (int i = 0; i < sublist.length(); i++) {
            if (i%2==0)
            {
                one.add(sublist.charAt(i));
            }
            else
            {
                two.add(sublist.charAt(i));
            }
        }
        one.sort((value1,value2)->{
            if (value1==value2)
            {
                return 0;
            }
            return value1>value2?1:-1;
        });
        two.sort((value1,value2)->{
            if (value1==value2)
            {
                return 0;
            }
            return value1>value2?1:-1;
        });
        StringBuilder subString = new StringBuilder();
        one.forEach(subString::append);
        for (int i = 0; i < two.size(); i++) {
            if (i%2!=0)
            {
                subString.insert(i, two.get(i));
            }
        }
        System.out.println(subString);
    }
}

