package test;

public class Persons<T> {

    private String name;
    private Integer age;
    private T side;

    public T getSide() {
        return side;
    }

    public void setSide(T side) {
        this.side = side;
    }

    @Override
    public String toString() {
        return "Persons{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", side=" + side +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
