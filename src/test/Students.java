package test;

public class Students<T> extends Persons<Boolean>{

    private T names;
    private Integer ss;


    public T getNames() {
        return names;
    }

    @Override
    public String toString() {
        return "Students{" +
                "names=" + names +
                ", ss=" + ss +
                '}';
    }

    public void setName(T names) {
        this.names = names;
    }

    public Integer getSs() {
        return ss;
    }

    public void setSs(Integer ss) {
        this.ss = ss;
    }
}
