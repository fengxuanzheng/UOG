package springbootTest;

import com.startSpringBoot.boot.MainApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

@Slf4j
@SpringBootTest(classes = MainApplication.class)
public class TestSpringBooot {

    @Autowired
    private DataSource dataSource;
    @Test
    public void test()
    {
        System.out.println(dataSource.getClass());
    }
}
