package com.startSpringBoot.boot.config;

import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;

/*
重写duird监控spring配置类
 */
//@Configuration
public class DruidConfiguration{


   @Bean
   public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreators() {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }
}
