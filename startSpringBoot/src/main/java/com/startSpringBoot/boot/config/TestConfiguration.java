package com.startSpringBoot.boot.config;

import com.startSpringBoot.boot.pojo.Persons;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfiguration {

    private Persons persons;


    public TestConfiguration(Persons persons)
    {
        this.persons=persons;

    }



    @Override
    public String toString() {
        return "TestConfiguration{" +
                "persons=" + persons +
                '}';
    }
}
