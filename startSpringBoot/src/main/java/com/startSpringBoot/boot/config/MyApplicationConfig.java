package com.startSpringBoot.boot.config;


import com.startSpringBoot.boot.pojo.ApplyReport;
import com.startSpringBoot.boot.pojo.Cat;
import com.startSpringBoot.boot.pojo.Pet;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Import({MybatisConfiguration.class})
//此注解可以写在任何组件地方比如控制器和服务层(@Server),作用是给容器自动注入所声明的类型组件,导入组件的名字默认是全类名
//@ImportResource("classpath:xxx.xml")此注解是为了兼容原生spring,把原生spring的XML配置迁移过来
@Configuration(proxyBeanMethods = true)//告诉springboot这是一个配置类
@ConditionalOnBean(Pet.class)
//@AutoConfigureAfter(MyApplicationConfig2.class)
//proxyBeanMethods代理Bean方法
/*
Full(proxyBeanMethods = true)
Lite(proxyBeanMethods = false)
*/
@EnableConfigurationProperties(Cat.class)//开启Cat的配置绑定功能,同时把这个组件加入到容器中,或者也可以在Cat的JavaBean中标注@ConfigurationProperties和@Component
public class MyApplicationConfig {

 /*   @Bean
    public Pet pet()
    {
        return new Pet();
    }*/


    @Bean
    /* 给容器添加组件,以方法名作为组件默认ID,返回类型就是组件类型,返回值就算组件在容器中保存实例 */
    //配置类使用@Bean默认是单实例
    //配置类本身也是组件
    //外部无论对配置类中这个组件组成方法调用多少次获取的都是之前注册到容器中单实例

    //@ConditionalOnBean(Pet.class)//此注解作为@Conditional注解的子注解,目前此注解表示有对应Bean才把@Bean标注方法返回对象注入到容器,一般是标注到@Bean的方法上也可以标注到配置类上,
    //此注解判断某BEAN是否存在,存在顺序关系,要判断的BEAN需要把方法写在此方法前面
    //如果将注解标注到配置类上,那如果判断的bean不存在那此配置类所有 @Bean都无效,如果标注到类上需要配置控制配置类注入顺序三大注解,比如@AutoConfigureAfter,使用三大注解需要在资源文件夹META-INF下加入spring.factories
    public ApplyReport applyReport()
    {
        ApplyReport applyReport = new ApplyReport();
        applyReport.setId(100);

        //applyReport.setPet(pet());
        return applyReport;
    }


}
