package com.startSpringBoot.boot.config;

import com.startSpringBoot.boot.servlet.MyFilter;
import com.startSpringBoot.boot.servlet.MyListenter;
import com.startSpringBoot.boot.servlet.MyServlet;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

@Configuration
public class MyRejectServlet {

    @Bean
    public ServletRegistrationBean<MyServlet> myServlets()
    {
        MyServlet myServlet = new MyServlet();
        return new ServletRegistrationBean<>(myServlet,"/my","/my1");
    }

    @Bean
    public FilterRegistrationBean<MyFilter> registFilter()
    {
        //return new FilterRegistrationBean<>(new MyFilter(),myServlets());
        FilterRegistrationBean<MyFilter> myFilterFilterRegistrationBean = new FilterRegistrationBean<>(new MyFilter());
        myFilterFilterRegistrationBean.setUrlPatterns(Collections.singletonList("/*"));
        return myFilterFilterRegistrationBean;
    }
    @Bean
    public ServletListenerRegistrationBean<MyListenter> registListener()
    {
        return new ServletListenerRegistrationBean<>(new MyListenter());
    }
}
