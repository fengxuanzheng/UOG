package com.startSpringBoot.boot.config;

import com.startSpringBoot.boot.intercepter.MyIntercepter;
import com.startSpringBoot.boot.pojo.Cat;
import com.startSpringBoot.boot.utils.MyHttpMessageConverters;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.accept.HeaderContentNegotiationStrategy;
import org.springframework.web.accept.ParameterContentNegotiationStrategy;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.util.UrlPathHelper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Configuration

public class MyWebMvcConfigurer implements WebMvcConfigurer {
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        UrlPathHelper urlPathHelper = new UrlPathHelper();
        urlPathHelper.setRemoveSemicolonContent(false);
        configurer.setUrlPathHelper(urlPathHelper);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/aa/**").addResourceLocations("classpath:/abb/");
        //表示/abd/**所有请求都到classpath:/static/**下面就行匹配
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new Converter<String, Cat>() {
            @Override
            public Cat convert(String source) {
                if (!source.isEmpty()) {
                    String[] split = source.split(",");
                    Cat cat = new Cat();
                    cat.setName(split[0]);
                    cat.setAge(Integer.valueOf(split[1]));
                    cat.setAdmin(split[2]);
                    return cat;
                }
                return new Cat();
            }
        });
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new MyHttpMessageConverters());
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        HashMap<String, MediaType> objectObjectHashMap = new HashMap<>(3);
        objectObjectHashMap.put("pet",MediaType.parseMediaType("application/pet"));
        objectObjectHashMap.put("json",MediaType.APPLICATION_JSON);
        objectObjectHashMap.put("xml", MediaType.APPLICATION_XML);
        ParameterContentNegotiationStrategy parameterContentNegotiationStrategy = new ParameterContentNegotiationStrategy(objectObjectHashMap);
        HeaderContentNegotiationStrategy headerContentNegotiationStrategy = new HeaderContentNegotiationStrategy();
        configurer.strategies(Arrays.asList(parameterContentNegotiationStrategy,headerContentNegotiationStrategy));
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyIntercepter()).addPathPatterns("/**").excludePathPatterns("/");
    }
}
