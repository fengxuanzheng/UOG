package com.startSpringBoot.boot.config;

import com.startSpringBoot.boot.pojo.Color;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ProFilteConfig {

    @Bean
    @Profile({"prof","default"})
    public Color yellowerColor()
    {
       Color yes= new Color();
       yes.setColorName("白色");
       return yes;
    }

    @Bean
    @Profile("test")
    public Color blueColor()
    {
        Color yes= new Color();
        yes.setColorName("蓝色");
        return yes;
    }
}
