package com.startSpringBoot.boot.endpoint;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
@Slf4j
@Component
@Endpoint(id = "myEndPoint")
public class MyEndPoint {

    @ReadOperation
    public Map<String,Object> getPoint()
    {
        return Collections.singletonMap("asd","1234");
    }
    @WriteOperation
    public void setMap(String as)
    {
      log.error("自定义端电停止");
    }
}
