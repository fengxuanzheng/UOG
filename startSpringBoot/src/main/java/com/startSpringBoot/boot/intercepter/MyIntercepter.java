package com.startSpringBoot.boot.intercepter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*
1.实现HandlerInterceptor接口
2.在WebMvcConfigurer中注册拦截器
3.指定拦截器规则,如果是拦截所有静态资源也会拦截,要配置放行的连接
 */
@Slf4j
public class MyIntercepter implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
      log.warn("拦截器前置处理允许:"+handler.toString());
      return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.warn("方法处理完成:"+(modelAndView!=null?modelAndView.toString():null));
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.warn("视图返回");
    }
}
