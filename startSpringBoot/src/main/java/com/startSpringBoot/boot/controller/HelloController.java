package com.startSpringBoot.boot.controller;

import com.startSpringBoot.boot.dao.EmployeeDAO;
import com.startSpringBoot.boot.pojo.*;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class HelloController {

    private Counter counter;

    @Autowired
    private Person person;

    @Autowired
    private Persons persons;

    @Autowired
    private Pet cats11;

    @Autowired
    private Color color;

    @Value("${JAVA_HOME}")// @Value注解处理能取出配置文件里的值也能取出系统环境变量值
    private String envValue;

    @Value("${os.name}")
    private String osName;

    @Value("${tests.name:默认名字}")
    private String testName;
    public HelloController(MeterRegistry meterRegistry) {
        counter = meterRegistry.counter("sqlCont");
    }

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private EmployeeDAO employeeDAO;
    @GetMapping("/hello")
    public String hello()
    {
        return "start springboot";
    }

    @PutMapping("/user")
    public String putUser()
    {
        return "PutUser";
    }

    @GetMapping("/cars/{sell}")
    //矩阵变量路径一定要使用变量批量,路径不用变量会404,同时加入WebMvcConfiguration接口配置类开启矩阵功能
    public Map carSell(@MatrixVariable("low") Integer low, @MatrixVariable("brand") List<String> brand)
    {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("low",low);
        stringObjectHashMap.put("brand",brand);
        return stringObjectHashMap;
    }

    @GetMapping("/cars/{bossId}/{empId}")
public Map<String,Object> getAllMatrixVariable(@MatrixVariable(value = "age",pathVar = "bossId") Integer age1,@MatrixVariable(value = "age",pathVar = "empId") Integer age2)
    {

        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("low",age1);
        stringObjectHashMap.put("brand",age2);
        return stringObjectHashMap;
    }

    @GetMapping("/getPet")
    public Pet getPet(Pet pet)
    {
        return pet;
    }

    @GetMapping("/getFileSysRes")
    @ResponseBody
    public FileSystemResource getFileSystemResource()
    {
        return null;
    }

    @GetMapping("/mybatisTest")
    public List<Employee> mybatisTest()
    {
        counter.increment();
       return employeeDAO.getEmployeeList();
    }

    @GetMapping("/profilter")
    public String getTestName()
    {
        return testName;
    }
    @GetMapping("/getpersons")
    public Object getPerson()
    {
        System.out.println(person);
        System.out.println(person.getClass());
        System.out.println(persons.getClass());
        System.out.println(cats11);
        System.out.println(color);
        return person;

    }

    @GetMapping("/envValue")
    public String getEnvValue()
    {
        System.out.println(osName);
        return envValue;

    }

    @Bean
    public Pet cats11()
    {
        Pet pet = new Pet();
        pet.setName("巧克力");
        return pet;
    }


}
