package com.startSpringBoot.boot.controller;

import com.startSpringBoot.boot.pojo.Pet;
import com.startSpringBoot.boot.utils.MyError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

@Controller
@SessionAttributes(types = {Pet.class})
@Slf4j
public class SaticResourceController {
    private Object a1;
    private Object a2;

    //静态资源拦截器测试:spingboot默认配置静态资源拦截器,在项目类路径下的/public或/static或/META-INF/resources下的静态文件会解析成静态映射(原理:静态映射处理器默认拦截/**),
    //前提是controller中没有对应URL,原理是请求进来先看看controller能不能处理,不能处理的就交给静态资源处理器,如果静态资源处理器也找不到就404
    //添加静态资源访问前缀:spring.mvc.static-path-pattern=/res/**

    //改变默认静态资源文件夹:spring.web.resources.static-locations=classpath:/haha

    //对于webjars访问:进入webjars网站添加一些maven的前端如jquery依赖后就通过/webjars/依赖包实际路径

    //对于欢迎页有两种,
    // 1.在静态资源路径加入index.html文件,注意如果访问路径加上了前缀就不能访问了
    //2.在controller中能处理index请求

    //favicon图标功能:吧favicon.ico放在静态资源目录下就可(配置了静态资源访问前缀无效)
    @GetMapping("/aaa.html")
    @ResponseBody
    public String aaa()
    {
        return "测试";
    }

    @GetMapping("/")

    public String index()
    {
        return "forward:/res/index.html";
    }

    @GetMapping("/controtest")
    public String controtest()
    {
        return "/res/ccc.html";
    }

    @GetMapping("/PetView")
    public String getPet(Pet pet)
    {
        return "forward:/getPetView";
    }

    @GetMapping("/getPetView")
    @ResponseBody
    public Pet getPetview(@SessionAttribute(value = "pet",required = false) Pet pet, @RequestAttribute(value = "pet") Pet petofRes)
    {
        log.info("请求域"+petofRes.toString());
        return pet;
    }

    @GetMapping("/getTestView")
    public String getTestView(Pet pet, Model model)
    {
        a1=model;
        model.addAttribute("msg","测试");
        //redirectAttributes.addAttribute("msg","测试");
       // return "/templates/thyme.html";
       // return "forward:/bbb.html";
        Set<Map.Entry<String, Object>> entries = model.asMap().entrySet();
        System.out.println("大小"+entries.size());
        return "forward:/getTestViewBind";
    }
    @GetMapping("/getTestViewBind")
    @ResponseBody
    public Object getTestViewBind(Pet pet, @RequestAttribute("msg") Object ob, @RequestAttribute(value = "org.springframework.validation.BindingResult.pet",required = false) Object oa)
    {
        log.error(pet.toString());
        log.error(ob.toString());
        log.error(oa.toString());
        return "attribute";
    }


    @RequestMapping("/upload" )
    public String upload(@RequestParam(value = "username",required = false) String username, @RequestPart(value = "filer",required = false)MultipartFile multipartFile) throws IOException {
        log.info(username.toString());
        if (!multipartFile.isEmpty())
        {

                //int i=10/0;

                String originalFilename = multipartFile.getOriginalFilename();

                multipartFile.transferTo(new File("F:\\练习\\"+originalFilename));
                if (1==1)
                {
                    throw new MyError();
                }
                return "redirect:bbb.html";

        }
        return "redirect:/aaa.html";
    }
}
