package com.startSpringBoot.boot.health;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class MyHealthHealthIndicator extends AbstractHealthIndicator {
    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        //builder.up();
        HashMap<String, Object> stringObjectHashMap = new HashMap<>(2);
        stringObjectHashMap.put("count",50);
        stringObjectHashMap.put("ping",100);
        builder.status(Status.UP);
        builder.withDetails(stringObjectHashMap);
    }
}
