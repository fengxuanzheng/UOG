package com.startSpringBoot.boot;

import com.startSpringBoot.boot.pojo.Pet;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.Arrays;
import java.util.Map;

@Slf4j
@MapperScan("com.startSpringBoot.boot.dao")
@ServletComponentScan({"com.startSpringBoot"})//加上此注解原生javaweb组件才能生效
//此类一般也称为主配值类
@SpringBootApplication(scanBasePackages = "com.startSpringBoot")//默认是在boot包下的所有子包

//或者
/*@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan("com.startSpringBoot")*/
public class MainApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(MainApplication.class, args);
        String[] beanDefinitionNames = run.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(item->{
            System.out.println(item);
        });

        //ApplyReport bean = run.getBean(ApplyReport.class);
        //ApplyReport bean2 = run.getBean(ApplyReport.class);
        boolean applyReport2 = run.containsBean("applyReport");
        System.out.println("是否有applyReport:"+applyReport2);
       // System.out.println("从IOC那"+(bean2==bean));

        String[] beanNamesForType = run.getBeanNamesForType(WebMvcConfigurationSupport.class);
        System.out.println(beanNamesForType.length);
        Arrays.stream(beanNamesForType).forEach(item->{
            System.out.println("自动配置:"+item);
        });

        System.out.println("______________________________________________________");
        //DruidStatInterceptor bean = run.getBean(DruidStatInterceptor.class);
       // log.error(bean.toString());
        System.out.println("______________________________________________________");
        //TestConfiguration bean1 = run.getBean(TestConfiguration.class);
        Object bean1 = run.getBean("cats11");
        String[] beanNamesForType1 = run.getBeanNamesForType(Pet.class);
        log.error(bean1.toString());
        log.error(bean1.getClass().toString());
        log.error(Arrays.asList(beanNamesForType1).toString());
        // Cat bean1 = run.getBean(Cat.class);
        //System.out.println(bean1);
        //************************************************************************
        ConfigurableEnvironment environment = run.getEnvironment();
        MutablePropertySources propertySources = environment.getPropertySources();
        Map<String, Object> systemEnvironment = environment.getSystemEnvironment();
        System.out.println(propertySources);
        System.out.println("************************************************************************");
        System.out.println(systemEnvironment);
        
        //************************************************************************

       /* //获得配置类对象本身是由CGLIB代理的对象调用方法,springboot总会检查这个组件是否在容器中存在,从而保证组件单实例,在@Configuration(proxyBeanMethods = true)情况下
        MyApplicationConfig beanconfig = run.getBean(MyApplicationConfig.class);
        System.out.println(beanconfig.getClass());//class com.startSpringBoot.boot.config.MyApplicationConfig$$EnhancerBySpringCGLIB$$5b22c304
        ApplyReport applyReport = beanconfig.applyReport();
        ApplyReport applyReport1 = beanconfig.applyReport();
        System.out.println("从配置类"+(applyReport == applyReport1));


        //*************************************
        *//*
        如果proxyBeanMethods = true能解决组件依赖
        *//*

        ApplyReport bean1 = run.getBean(ApplyReport.class);
        Pet bean3 = run.getBean(Pet.class);
        System.out.println("用户Pet:" + (bean1.getPet() == bean3));

        System.out.println("**************************************");
        MybatisConfiguration bean4 = run.getBean(MybatisConfiguration.class);
        System.out.println(bean4);

*/
    }
}
