package com.startSpringBoot.boot.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class MyAdvice {

    @ExceptionHandler({NullPointerException.class})
    public String geterrpr(Exception exception)
    {
      log.error(exception.getMessage());
        return "thyme";
    }
}
