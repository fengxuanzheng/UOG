package com.startSpringBoot.boot.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN,reason = "自定义异常")
public class MyError extends RuntimeException{
}
