package com.startSpringBoot.boot.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

@Slf4j
public class MySpringApplicationRunListener implements SpringApplicationRunListener {

    private SpringApplication springApplication;
    private String[] arg;

    public MySpringApplicationRunListener(SpringApplication springApplication,String[] arg) {
        this.springApplication=springApplication;
        this.arg=arg;

    }

    @Override
    public void starting(ConfigurableBootstrapContext bootstrapContext) {
       log.error("MySpringApplicationRunListener ______________starting");
    }


    @Override
    public void environmentPrepared(ConfigurableBootstrapContext bootstrapContext, ConfigurableEnvironment environment) {
        log.error("MySpringApplicationRunListener ______________environmentPrepared");
    }


    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        log.error("MySpringApplicationRunListener ______________contextPrepared");
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        log.error("MySpringApplicationRunListener ______________ contextLoaded");
    }

    @Override
    public void started(ConfigurableApplicationContext context) {
        log.error("MySpringApplicationRunListener ______________started");
    }

    @Override
    public void running(ConfigurableApplicationContext context) {
        log.error("MySpringApplicationRunListener ______________running");
    }

    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        log.error("MySpringApplicationRunListener ______________failed");
    }
}
