package com.startSpringBoot.boot.servlet;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

//@WebListener
@Slf4j
public class MyListenter implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
      log.warn("原生监听器启动");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
       log.warn("原生监听器销毁");
    }
}
