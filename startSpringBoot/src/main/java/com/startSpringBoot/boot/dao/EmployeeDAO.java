package com.startSpringBoot.boot.dao;

import com.startSpringBoot.boot.pojo.Employee;

import java.util.List;


public interface EmployeeDAO {

    public List<Employee> getEmployeeList();
}
