package com.startSpringBoot.boot.pojo;

public class Color {

    private String colorName;

    public String getColorName() {
        return colorName;
    }

    @Override
    public String toString() {
        return "Color{" +
                "colorName='" + colorName + '\'' +
                '}';
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }
}
