package com.startSpringBoot.boot.pojo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("test")
@ConfigurationProperties(prefix = "persons")
public class Boos implements Person{
    private String name;
    private Integer age;

    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Boos{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
