package com.startSpringBoot.boot.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LomBokTestJavaBean {

    private String user;
    private String sex;
    private Integer age;
}
