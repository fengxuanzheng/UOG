package com.startSpringBoot.boot.pojo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile({"prof","default"}) //default是默认环境
@ConfigurationProperties(prefix = "persons")
public class Uog implements Person{
    private String name;
    private Integer age;

    @Override
    public String toString() {
        return "Uog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Uog(String name) {
        this.name = name;
    }

    public Uog() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
