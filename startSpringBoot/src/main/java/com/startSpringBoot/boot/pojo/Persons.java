package com.startSpringBoot.boot.pojo;

import org.springframework.stereotype.Component;

@Component
public class Persons  {
    private String usernames="欧尼酱";
    private Integer ages;

    @Override
    public String toString() {
        return "Persons{" +
                "usernames='" + usernames + '\'' +
                ", ages=" + ages +
                '}';
    }

    public String getUsernames() {
        return usernames;
    }

    public void setUsernames(String usernames) {
        this.usernames = usernames;
    }

    public Integer getAges() {
        return ages;
    }

    public void setAges(Integer ages) {
        this.ages = ages;
    }
}
