package com.startSpringBoot.boot.pojo;


public class ApplyReport {

  private long id;
  private String status;
  private String equipmentName;
  private String workContainer;
  private String workWaring;
  private String safety;
  private String energyType;
  private String energySource;
  private String energyManagement;
  private String workLocation;
  private String workPerson;
  private String safetyPerson;
  private long phone;
  private java.sql.Timestamp workTimeStart;
  private String username;
  private long usernameId;
  private String usernameDistrict;
  private java.sql.Timestamp applyTime;
  private java.sql.Timestamp checkTime;
  private String rejectText;
  private java.sql.Timestamp workTimeEnd;

  private Pet pet;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getEquipmentName() {
    return equipmentName;
  }

  public void setEquipmentName(String equipmentName) {
    this.equipmentName = equipmentName;
  }


  public String getWorkContainer() {
    return workContainer;
  }

  public void setWorkContainer(String workContainer) {
    this.workContainer = workContainer;
  }


  public String getWorkWaring() {
    return workWaring;
  }

  public void setWorkWaring(String workWaring) {
    this.workWaring = workWaring;
  }


  public String getSafety() {
    return safety;
  }

  public void setSafety(String safety) {
    this.safety = safety;
  }


  public String getEnergyType() {
    return energyType;
  }

  public void setEnergyType(String energyType) {
    this.energyType = energyType;
  }


  public String getEnergySource() {
    return energySource;
  }

  public void setEnergySource(String energySource) {
    this.energySource = energySource;
  }


  public String getEnergyManagement() {
    return energyManagement;
  }

  public void setEnergyManagement(String energyManagement) {
    this.energyManagement = energyManagement;
  }


  public String getWorkLocation() {
    return workLocation;
  }

  public void setWorkLocation(String workLocation) {
    this.workLocation = workLocation;
  }


  public String getWorkPerson() {
    return workPerson;
  }

  public void setWorkPerson(String workPerson) {
    this.workPerson = workPerson;
  }


  public String getSafetyPerson() {
    return safetyPerson;
  }

  public void setSafetyPerson(String safetyPerson) {
    this.safetyPerson = safetyPerson;
  }


  public long getPhone() {
    return phone;
  }

  public void setPhone(long phone) {
    this.phone = phone;
  }


  public java.sql.Timestamp getWorkTimeStart() {
    return workTimeStart;
  }

  public void setWorkTimeStart(java.sql.Timestamp workTimeStart) {
    this.workTimeStart = workTimeStart;
  }


  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }


  public long getUsernameId() {
    return usernameId;
  }

  public void setUsernameId(long usernameId) {
    this.usernameId = usernameId;
  }


  public String getUsernameDistrict() {
    return usernameDistrict;
  }

  public void setUsernameDistrict(String usernameDistrict) {
    this.usernameDistrict = usernameDistrict;
  }


  public java.sql.Timestamp getApplyTime() {
    return applyTime;
  }

  public void setApplyTime(java.sql.Timestamp applyTime) {
    this.applyTime = applyTime;
  }


  public java.sql.Timestamp getCheckTime() {
    return checkTime;
  }

  public void setCheckTime(java.sql.Timestamp checkTime) {
    this.checkTime = checkTime;
  }


  public String getRejectText() {
    return rejectText;
  }

  public void setRejectText(String rejectText) {
    this.rejectText = rejectText;
  }


  public java.sql.Timestamp getWorkTimeEnd() {
    return workTimeEnd;
  }

  public void setWorkTimeEnd(java.sql.Timestamp workTimeEnd) {
    this.workTimeEnd = workTimeEnd;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  @Override
  public String toString() {
    return "ApplyReport{" +
            "id=" + id +
            ", status='" + status + '\'' +
            ", equipmentName='" + equipmentName + '\'' +
            ", workContainer='" + workContainer + '\'' +
            ", workWaring='" + workWaring + '\'' +
            ", safety='" + safety + '\'' +
            ", energyType='" + energyType + '\'' +
            ", energySource='" + energySource + '\'' +
            ", energyManagement='" + energyManagement + '\'' +
            ", workLocation='" + workLocation + '\'' +
            ", workPerson='" + workPerson + '\'' +
            ", safetyPerson='" + safetyPerson + '\'' +
            ", phone=" + phone +
            ", workTimeStart=" + workTimeStart +
            ", username='" + username + '\'' +
            ", usernameId=" + usernameId +
            ", usernameDistrict='" + usernameDistrict + '\'' +
            ", applyTime=" + applyTime +
            ", checkTime=" + checkTime +
            ", rejectText='" + rejectText + '\'' +
            ", workTimeEnd=" + workTimeEnd +
            ", pet=" + pet +
            '}';
  }
}
