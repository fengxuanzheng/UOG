package com.springsecurity.config;

import com.springsecurity.anno.AnonymousAccess;
import org.springframework.beans.BeansException;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.RoleHierarchyVoter;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.cache.EhCacheBasedUserCache;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
@Deprecated
//@Configuration
public class SpringSecurityConfig2 extends WebSecurityConfigurerAdapter  implements ApplicationContextAware {
    //@Autowired
    private UserDetailsService userDetailsService;
    //@Autowired
    private DataSource dataSource;
    private ApplicationContext applicationContext;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        String encode = passwordEncoder().encode("123456");
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
       // auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder()); 这种为视频教程方法
        auth.authenticationProvider(preAuthenticatedAuthenticationProvider());

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin().loginPage("/login.html") //指定登入页面
                .loginProcessingUrl("/user/login")// 指定处理登入请求的路径控制器,随便写路径处理的控制器由框架自己完成
                .defaultSuccessUrl("/success.html").permitAll() //登入验证成功后跳转路径
                .and().antMatcher("/noSession.html").anonymous()
                .and().antMatcher("/").authorizeRequests()
              /*  .and().authorizeRequests().antMatchers("/","/user/login","/hello").permitAll()  //指定那些请求不需要认证,也可以加上anonymous()
                //.antMatchers("/admins/text").hasAuthority("admin")//给给定路径添加单个权限(多个做不到)
                .antMatchers("/admins/text").hasAnyAuthority("uog")
                .antMatchers("/role/text").hasRole("persons")
                .anyRequest().authenticated() */  //指定除了上书请求外都需要认证
               .and().csrf().disable() //关闭csrf

               .rememberMe().tokenRepository(persistentTokenRepository())
                 .tokenValiditySeconds(1800)
                .userDetailsService(userDetailsService);
        //配置没有权限的自定义页面
          http.exceptionHandling().accessDeniedPage("/405.html");

          //退出配置
       // http.logout().logoutUrl("/logout").logoutSuccessUrl("/login.html").permitAll();

        //前后分离退出配置
        http.logout().logoutUrl("/logout").logoutSuccessHandler(new LogoutSuccessHandler() {
            @Override
            public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("sucess",true);
                    response.setContentType("application/json;charset=UTF-8");
                    response.getWriter().write(jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e.getMessage());
                }
            }
        });
        //缓存管理器
        http.sessionManagement(session->{
            session.maximumSessions(1).expiredUrl("/noSession.html");
        });

        http.anonymous().authorities("ROLE_MY");
        //自定义授权管理器
        http.authorizeRequests(item->item.accessDecisionManager(accessDecisionManager()).antMatchers("/admins/text").hasRole("uog")
                .antMatchers("/,/user/login,/hello").anonymous().anyRequest().authenticated());

        //配置方法表达式解析器

    }


    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }

   // @Bean
    public DaoAuthenticationProvider myDaoAuthenticationProvider()
    {
        EhCacheBasedUserCache ehCacheBasedUserCache = new EhCacheBasedUserCache();
       // ehCacheBasedUserCache.setCache();

        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        //daoAuthenticationProvider.setUserCache();
        return daoAuthenticationProvider;
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository()
    {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        //jdbcTokenRepository.setCreateTableOnStartup(true);
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.expressionHandler(securityExpressionHandler());
    }
    /*
不能直接这样自定义
    @Bean
   public UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter()
    {
        UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter = new UsernamePasswordAuthenticationFilter();
       usernamePasswordAuthenticationFilter. setContinueChainBeforeSuccessfulAuthentication(true);
       return usernamePasswordAuthenticationFilter;
    }*/

    /**
     *此部分是作为测试预验证
     */
   /* @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login.html").permitAll();
        http.authorizeRequests().antMatchers("/logout").permitAll().anyRequest().authenticated();
        http.csrf().disable();
        http.addFilterAt(requestHeaderAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);


    }*/

    //@Bean
    public RequestHeaderAuthenticationFilter requestHeaderAuthenticationFilter() throws Exception {
        RequestHeaderAuthenticationFilter requestHeaderAuthenticationFilter = new RequestHeaderAuthenticationFilter();
        requestHeaderAuthenticationFilter.setAuthenticationManager(authenticationManager());
       // requestHeaderAuthenticationFilter.setAuthenticationSuccessHandler(new SavedRequestAwareAuthenticationSuccessHandler());
        requestHeaderAuthenticationFilter.setExceptionIfHeaderMissing(false);
        return requestHeaderAuthenticationFilter;

    }
    @Bean
    public PreAuthenticatedAuthenticationProvider preAuthenticatedAuthenticationProvider()
    {
        PreAuthenticatedAuthenticationProvider preAuthenticatedAuthenticationProvider = new PreAuthenticatedAuthenticationProvider();
        preAuthenticatedAuthenticationProvider.setPreAuthenticatedUserDetailsService(userDetailsByNameServiceWrapper());
        return preAuthenticatedAuthenticationProvider;
    }

    @Bean
    public UserDetailsByNameServiceWrapper userDetailsByNameServiceWrapper()
    {
        UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken> preAuthenticatedAuthenticationTokenUserDetailsByNameServiceWrapper = new UserDetailsByNameServiceWrapper<>();
        preAuthenticatedAuthenticationTokenUserDetailsByNameServiceWrapper.setUserDetailsService(userDetailsService);
        return preAuthenticatedAuthenticationTokenUserDetailsByNameServiceWrapper;
    }


    /**
     *授权-分层角色,经过不屑努力还是失败了,直接使用配置方式由于ConfigAttribute.getAttribute回直接返回空,重写相关类不在同一个包内权限不够
      */
    @Bean
    public AccessDecisionVoter hierarchyVoter() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy("ROLE_uog > ROLE_persons");
        return new RoleHierarchyVoter(roleHierarchy);
    }
    @Bean
    public AccessDecisionManager accessDecisionManager()
    {
        ArrayList<AccessDecisionVoter<?>> accessDecisionVoters = new ArrayList<>();
        accessDecisionVoters.add(new WebExpressionVoter());
        accessDecisionVoters.add(hierarchyVoter());
        AffirmativeBased affirmativeBased = new AffirmativeBased(accessDecisionVoters);

        return affirmativeBased;
    }

    /**
     *此部分配置@PreAuthorize("hasPermission()")权限表达式
     */
    @Bean
    public PermissionEvaluator myPermissionEvaluator()
    {
        return new MyPermissionEvaluator();
    }

    @Bean
    public SecurityExpressionHandler securityExpressionHandler()
    {
        DefaultWebSecurityExpressionHandler defaultWebSecurityExpressionHandler = new DefaultWebSecurityExpressionHandler();
        defaultWebSecurityExpressionHandler.setPermissionEvaluator(myPermissionEvaluator());
        defaultWebSecurityExpressionHandler.setDefaultRolePrefix("aa_");
        return defaultWebSecurityExpressionHandler;
    }
  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        super.setApplicationContext(applicationContext);
        this.applicationContext=applicationContext;
    }
    private String[] getAnonymousUrls() {
// 获取所有的 RequestMapping
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = applicationContext.getBean(RequestMappingHandlerMapping.class).getHandlerMethods();
        Set<String> allAnonymousAccess = new HashSet<>();
// 循环 RequestMapping
        for (Map.Entry<RequestMappingInfo, HandlerMethod> infoEntry : handlerMethods.entrySet()) {
            HandlerMethod value = infoEntry.getValue();
// 获取方法上 AnonymousAccess 类型的注解
            AnonymousAccess methodAnnotation = value.getMethodAnnotation(AnonymousAccess.class);
// 如果方法上标注了 AnonymousAccess 注解，就获取该方法的访问全路径
            if (methodAnnotation != null) {
                RequestMappingInfo key = infoEntry.getKey();
                allAnonymousAccess.addAll(key.getPathPatternsCondition().getDirectPaths());
            }
        }
        return allAnonymousAccess.toArray(new String[0]);
    }
}
class MyPermissionEvaluator implements PermissionEvaluator
{

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        List<? extends GrantedAuthority> collect = authorities.stream().filter(item -> {
            return permission.equals(item.getAuthority());
        }).collect(Collectors.toList());
        return collect.isEmpty()?false:true;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
      return   hasPermission(authentication,null,permission);
    }
}