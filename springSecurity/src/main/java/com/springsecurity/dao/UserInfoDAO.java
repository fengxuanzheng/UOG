package com.springsecurity.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springsecurity.pojo.UserInfo;


public interface UserInfoDAO extends BaseMapper<UserInfo> {
}
