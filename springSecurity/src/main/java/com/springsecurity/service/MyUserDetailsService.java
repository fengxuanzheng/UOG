package com.springsecurity.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.springsecurity.dao.UserInfoDAO;
import com.springsecurity.pojo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private    PasswordEncoder passwordEncoder;
    @Resource
    private UserInfoDAO userInfoDAO;

    /**
     * @param username  此方法作为配置类实现
     * @return
     * @throws UsernameNotFoundException
     */
  /*  @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("admin");
        grantedAuthorities.add(simpleGrantedAuthority );
        //SimpleGrantedAuthority simpleGrantedAuthority= AuthorityUtils.commaSeparatedStringToAuthorityList("admin"); 此为视频教程方法
        return new User("uog",passwordEncoder.encode("123456"),grantedAuthorities);

    }*/
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo username1 = userInfoDAO.selectOne(new QueryWrapper<UserInfo>().eq("username", username));
        if (username1==null)
        {
            throw new UsernameNotFoundException("用户名或密码错误");
        }
        List<GrantedAuthority> admin = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_uog");
        return new User(username1.getUsername(),passwordEncoder.encode("820606"),admin);
    }

}
