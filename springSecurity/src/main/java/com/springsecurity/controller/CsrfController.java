package com.springsecurity.controller;

import com.springsecurity.anno.AnonymousAccess;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CsrfController {

    @GetMapping("/theymel")
    public String theymel()
    {
        return "redirect:/success.html";
    }
    @GetMapping("/crsfLogin")
    public String crsfLogin()
    {
        return "/on.html";
    }
    @GetMapping("/crsfSuccess")
    public String crsfSuccess()
    {
        return "/crsfSuccess.html";
    }

    @GetMapping("/anno")
    @AnonymousAccess
    @PreAuthorize("hasAnyRole('ROLE_MY')")
    public String getanno()
    {
        return "/anno.html";
    }
}
