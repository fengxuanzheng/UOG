package com.springsecurity.controller;

import com.springsecurity.pojo.UserInfo;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello()
    {
        return "欢迎观影";
    }

    @RequestMapping("/loginSucess")
    public String login()
    {
        return "自定义登入页面成功";
    }
    @RequestMapping("/admins/text")
    public String admins()
    {
        return "管理员身份";
    }
    @RequestMapping("/role/text")
    @Secured({"ROLE_persons","ROLE_uog"})
    public String role()
    {
        System.out.println("先进去");
        return "特定角色进入";
    }
    @GetMapping("/annonrole")
    @Secured({"ROLE_abc","ROLE_bb"})
    public String annonRole()
    {
        return "返回使用注解决定角色";
    }
    @GetMapping("/annonPreAuthorize")
  //  @PreAuthorize("hasRole('ROLE_abc')")
    @PreAuthorize("hasRole('uog')")
    public String annonPreAuthorize()
    {
        return "使用PreAuthorize,返回使用注解决定角色";
    }

    @GetMapping("/postauth")
    @PostAuthorize("hasAnyAuthority('bbc')")
    public String postaouth()
    {
        System.out.println("方法正在执行");
        return "方法执行后校验成功";
    }

    @RequestMapping("/getTestPreFilter")
    @PreAuthorize("hasRole('ROLE_abc')")
    @PostFilter("filterObject.username=='admin1'")
    public List<UserInfo> getTestPreFilter(){
        ArrayList<UserInfo> list = new ArrayList<>();
        list.add(new UserInfo(1,"admin1","6666"));
        list.add(new UserInfo(2,"admin2","888"));
        System.out.println(list);
        return list;
    }
    @GetMapping("/persisiten/text")
    @PreAuthorize("hasPermission(authentication,null ,'ROLE_uog')")
    public String persisitenTest(Authentication authentication)
    {
        return "返回使用hasPermission表达式";
    }

    @GetMapping("/persisiten/text1")
   // @PreAuthorize("hasRole('p23')")
    @Secured({"ROLE_persons","ROLE_uog"})
    public String persisitenTest1(Authentication authentication)
    {
        return "返回使用hasPermission表达式";
    }

}
