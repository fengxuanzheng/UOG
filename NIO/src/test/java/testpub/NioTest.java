package testpub;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @Author:圣代桥冰织
 * @CreateTime:2023-07-14 16:25:52
 */
public class NioTest {

    @Test
    public void test() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("./01.txt", "rw");
        FileChannel channel = randomAccessFile.getChannel();
        ByteBuffer allocate1 = ByteBuffer.allocate(2);
        ByteBuffer allocate2 = ByteBuffer.allocate(4);
        ByteBuffer[] byteChannels = new ByteBuffer[]{allocate1,allocate2};
        long read = channel.read(byteChannels, 1, 1);
        byte[] array = allocate1.array();
        byte[] array1 = allocate2.array();
        System.out.println(new String(array));
        System.out.println(new String(array1));
    }
}
