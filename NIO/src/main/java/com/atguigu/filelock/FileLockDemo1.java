package com.atguigu.filelock;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileLockDemo1 {

    public static void main(String[] args) throws Exception {
        String input = "atguigu";
        System.out.println("input:"+input);

        ByteBuffer buffer = ByteBuffer.wrap(input.getBytes());

        String filePath = "01.txt";
        Path path = Paths.get(filePath);

        FileChannel channel = FileChannel.open(path, StandardOpenOption.READ, StandardOpenOption.WRITE);


        //StandardOpenOption.APPEND从末尾添加加上这个后,手动设置position无效
       channel.position(channel.size());

        //加锁
        // FileLock lock = channel.lock(0L,Long.MAX_VALUE,true);
        //独占锁
        FileLock lock = channel.lock();
        System.out.println("是否共享锁："+lock.isShared());

        channel.write(buffer);
       // channel.close();
       System.out.println(lock.isValid());
        extracted(channel);
        //读文件
       // readFile(filePath);
        lock.release();

    }

    private static void extracted(FileChannel channel) throws IOException {
        ByteBuffer readbuff=ByteBuffer.allocate(1024);
        channel.position(0);
        int read1 = channel.read(readbuff);
        while (read1!=-1)
        {
            readbuff.flip();
            byte[] bytes = new byte[readbuff.remaining()];
             readbuff.get(bytes);
            System.out.println(new String(bytes));
            readbuff.clear();
            read1= channel.read(readbuff);
        }
    }

    private static void readFile(String filePath) throws Exception {
        FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String tr = bufferedReader.readLine();
        System.out.println("读取出内容：");
        while(tr != null) {
            System.out.println(" "+tr);
            tr = bufferedReader.readLine();
        }
        fileReader.close();
        bufferedReader.close();
    }
}
