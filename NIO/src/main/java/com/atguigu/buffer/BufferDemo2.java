package com.atguigu.buffer;

import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class BufferDemo2 {

    static private final int start = 0;
    static private final int size = 3;

    //内存映射文件io
     @Test
     public void b04() throws Exception {
         RandomAccessFile raf = new RandomAccessFile("01.txt", "rw");
         FileChannel fc = raf.getChannel();
         MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_WRITE, start, size);

         mbb.put(0, (byte) 99);
         mbb.put(1, (byte) 120);
         mbb.put(2, (byte) 110);
         //mbb.put(3, (byte) 118);
         while (mbb.hasRemaining()) {
             System.out.println(mbb.get());
         }
         raf.close();
     }
    //直接缓冲区
    @Test
    public void b03() throws Exception {
        String infile = "01.txt";
        FileInputStream fin = new FileInputStream(infile);
        FileChannel finChannel = fin.getChannel();
        String outfile = "02.txt";
        FileOutputStream fout = new FileOutputStream(outfile);
        FileChannel foutChannel = fout.getChannel();

        //创建直接缓冲区
        ByteBuffer buffer = ByteBuffer.allocateDirect(1024);

     /*   while (true) {
            buffer.clear();
            int r = finChannel.read(buffer);
            if(r == -1) {
                break;
            }
            buffer.flip();
            foutChannel.write(buffer);
        }*/
        //fin.transferTo(fout);
    }

    //只读缓冲区
    @Test
    public void b02() {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        System.out.println("开始前:"+buffer.position()  );
        System.out.println("开始前:"+buffer.limit());
        for (int i = 0; i < buffer.capacity(); i++) {
            buffer.put((byte)i);
        }
        System.out.println("开始后:"+buffer.position());
        System.out.println("开始后:"+buffer.limit());
        //创建只读缓冲区
        //buffer.position(3);
        buffer.limit(7);
        System.out.println("设置后:"+buffer.position());
        System.out.println("设置后:"+buffer.limit());
        ByteBuffer readonly = buffer.asReadOnlyBuffer();
        System.out.println("只读开始前:"+readonly.position());
        System.out.println("只读开始前:"+readonly.limit());
        System.out.println("只读开始前:"+readonly.capacity());
        for (int i = 0; i < buffer.limit(); i++) {
            byte b = buffer.get(i);
            b *=10;
            buffer.put(i,b);
        }
        System.out.println("开始后11:"+buffer.position());
        System.out.println("开始后11:"+buffer.limit());
        readonly.position(0);
        readonly.limit(buffer.capacity());
        System.out.println("只读开始后:"+readonly.position());
        System.out.println("只读开始后:"+readonly.limit());
        while (readonly.remaining()>0) {
            System.out.println(readonly.get());
        }
        System.out.println("只读开始后11:"+readonly.position());
        System.out.println("只读开始后11:"+readonly.limit());
    }


    //缓冲区分片
    @Test
    public void b01() {
        ByteBuffer buffer = ByteBuffer.allocate(20);
        System.out.println("开始前:"+buffer.position());
        System.out.println("开始前:"+buffer.limit());
        for (int i = 0; i < buffer.capacity()-10; i++) {
            buffer.put((byte)i);
            System.out.println("使用中:"+buffer.position());
        }
        buffer.put(11,(byte)12);
        System.out.println("中途:"+buffer.position());
        System.out.println("开始后:"+buffer.position());
        System.out.println("开始后:"+buffer.limit());
        buffer.flip();
        System.out.println("反转后:"+buffer.position());
        System.out.println("反转后:"+buffer.limit());
        buffer.limit(20);
        System.out.println("反转设置后:"+buffer.limit());
        for (int i = 0; i < buffer.capacity(); i++) {
            if (buffer.hasRemaining())
            {
                byte b = buffer.get();
                System.out.println("使用中:"+buffer.position());
                System.out.println(b);

            }
        }
        //创建子缓冲区
        buffer.position(3);
        buffer.limit(7);
        System.out.println("设置后:"+buffer.position());
        System.out.println("设置后:"+buffer.limit());
        ByteBuffer slice = buffer.slice();

        System.out.println("子缓冲区:"+slice.position());
        System.out.println("子缓冲区:"+slice.limit());
        System.out.println("子缓冲区:"+slice.capacity());
        //改变子缓冲区内容
        for (int i = 0; i <slice.capacity() ; i++) {
            byte b = slice.get(i);
            b *=10;
            slice.put(b);
            System.out.println("子缓冲区:"+slice.position());
        }

        buffer.position(0);
        buffer.limit(buffer.capacity());
        System.out.println("初始化:"+buffer.position());
        System.out.println("初始化:"+buffer.limit());
        while(buffer.remaining()>0) {
            System.out.println(buffer.get());
        }
        System.out.println("初始化后:"+buffer.position());
        System.out.println("初始化后:"+buffer.limit());
    }
}
