package com.atguigu.buffer;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BufferDemo1 {

    @Test
    public void buffer01() throws Exception {
        //FileChannel
        RandomAccessFile aFile =
                new RandomAccessFile("01.txt","rw");
        FileChannel channel = aFile.getChannel();

        //创建buffer，大小
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        System.out.println("位置:"+buffer.position());
        //读
        int bytesRead = channel.read(buffer);

        System.out.println("位置:"+buffer.position());
        while(bytesRead != -1) {
            //read模式
            buffer.flip();

            while(buffer.hasRemaining()) {
                System.out.println((char)buffer.get());
            }
            buffer.clear();

            bytesRead = channel.read(buffer);
        }
        System.out.println("位置:"+buffer.position());
        aFile.close();
    }


    @Test
    public void buffer02() throws Exception {

//        //创建buffer
//        IntBuffer buffer = IntBuffer.allocate(8);
//
//        //buffer放
//        for (int i = 0; i < buffer.capacity(); i++) {
//            int j = 2*(i+1);
//            buffer.put(j);
//        }
//
//        //重置缓冲区
//        buffer.flip();
//
//        //获取
//        while(buffer.hasRemaining()) {
//            int value = buffer.get();
//            System.out.println(value+" ");
//        }

        // 1、获取Selector选择器
        Selector selector = Selector.open();

        // 2、获取通道
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();

        // 3.设置为非阻塞
        serverSocketChannel.configureBlocking(false);

        // 4、绑定连接
        serverSocketChannel.bind(new InetSocketAddress(9999));

        // 5、将通道注册到选择器上,并制定监听事件为：“接收”事件
        serverSocketChannel.register(selector,SelectionKey.OP_ACCEPT);

    }
    @Test
    public void test3() throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.wrap("你懂的了".getBytes(StandardCharsets.UTF_8));
        Path path = Paths.get("hello.txt");
        boolean exists = Files.exists(path);
        if (!exists)
        {
             Files.createFile(path);
        }
        System.out.println(byteBuffer.position());
        System.out.println(byteBuffer.limit());
        System.out.println(byteBuffer.capacity());
        //System.out.println(StandardCharsets.UTF_8.decode(byteBuffer).toString());

        byteBuffer.flip();
        System.out.println(byteBuffer.position());
        System.out.println(byteBuffer.limit());
        byteBuffer.put((byte) 32);
        byteBuffer.put((byte) 33);
        byteBuffer.put((byte) 38);
        byteBuffer.put((byte) 41);
    }

}
