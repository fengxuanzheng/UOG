package com.atguigu.channel;

import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class SocketChannelDemo {

    public static void main(String[] args) throws Exception {
        //创建SocketChannel
        //SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress("www.baidu.com", 80));


      SocketChannel socketChannel = SocketChannel.open();

       socketChannel.connect(new InetSocketAddress("www.baidu.com", 80));
//设置阻塞和非阻塞
        socketChannel.configureBlocking(false);
        System.out.println("直接运行");





        socketChannel.setOption(StandardSocketOptions.SO_KEEPALIVE,true);

        //读操作
        ByteBuffer byteBuffer = ByteBuffer.allocate(16);
        socketChannel.read(byteBuffer);
        socketChannel.close();
        System.out.println("read over");

    }

}
