package com.atguigu.channel;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

//通道之间数据传输
public class FileChannelDemo3 {

    //transferFrom()
    public static void main(String[] args) throws Exception {
        // 创建两个fileChannel
        RandomAccessFile aFile = new RandomAccessFile("001.txt","rw");
        FileChannel fromChannel = aFile.getChannel();
        BufferedInputStream fileInputStream =new BufferedInputStream( new FileInputStream("001.txt"));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        fileInputStream.transferTo(byteArrayOutputStream);
        System.out.println(byteArrayOutputStream.toString());
        RandomAccessFile bFile = new RandomAccessFile("002.txt","rw");
        FileChannel toChannel = bFile.getChannel();

        //fromChannel 传输到 toChannel
        long position = 0;
        long size = fromChannel.size();
        toChannel.transferFrom(fromChannel,position,size);

        aFile.close();
        bFile.close();
        System.out.println("over!");
    }
}
