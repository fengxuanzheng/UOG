package com.atguigu.channel;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

//FileChanne写操作
public class FileChannelDemo2 {

    public static void main(String[] args) throws Exception {
        // 打开FileChannel
        RandomAccessFile aFile = new RandomAccessFile("02.txt","rw");

        FileChannel channel = aFile.getChannel();
        channel.force(false);
        System.out.println(channel.position());
        System.out.println(channel.size());
        //创建buffer对象
        ByteBuffer buffer = ByteBuffer.allocate(1024);

        String newData = "data atguigu";
        buffer.clear();

        //写入内容
        buffer.put(newData.getBytes());
        buffer.flip();

        //FileChannel完成最终实现
        while (buffer.hasRemaining()) {
            channel.write(buffer);
            System.out.println(channel.position());
            System.out.println(channel.size());
        }

        //关闭
        channel.close();
    }
}
