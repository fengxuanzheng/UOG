package com.javaio;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class ServerSocker {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(17772);
        Socket accept = serverSocket.accept();
        InputStream inputStream = accept.getInputStream();
        OutputStream outputStream = accept.getOutputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        inputStream.transferTo(byteArrayOutputStream);
        System.out.println(byteArrayOutputStream.toString());
        outputStream.write("就凭你".getBytes(StandardCharsets.UTF_8));
        accept.shutdownOutput();
        serverSocket.close();
    }
}
