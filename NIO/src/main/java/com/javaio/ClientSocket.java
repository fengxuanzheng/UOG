package com.javaio;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class ClientSocket {

    public static void main(String[] args) throws IOException {

        Socket socket = new Socket("127.0.0.1", 17772);
        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write("服务器我要炸了你".getBytes(StandardCharsets.UTF_8));
        socket.shutdownOutput();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        inputStream.transferTo(byteArrayOutputStream);
        System.out.println(byteArrayOutputStream.toString());
        socket.close();

    }
}
