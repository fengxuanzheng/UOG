package com.springToMq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class RabbitMqInput {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("1.14.157.131");
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("820606");
        connectionFactory.setPort(5672);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("hello", true, false, false, null);
        channel.exchangeDeclare("first", BuiltinExchangeType.FANOUT,true);
        channel.queueBind("hello","first","");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext())
        {
            String next = scanner.next();
            channel.basicPublish("first","",null,next.getBytes());
        }

        System.out.println("消息发送完毕");
        channel.close();
        connection.close();
    }
}
