package com.springToMq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class ActiveMqMain {
    public static void main(String[] args) {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Connection connection = null;

        try {
            connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageConsumer consumer = session.createConsumer(aa);
            TextMessage receive = (TextMessage) consumer.receive();
            System.out.println(receive.getText());
            System.out.println("信息属性:" + receive.getStringProperty("name"));
        /*    while (true) {

                if (null != receive) {

                } else {
                    break;
                }
            }*/
           // consumer.close();
           // session.close();
           // connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
