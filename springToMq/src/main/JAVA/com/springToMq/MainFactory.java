package com.springToMq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class MainFactory {

    private static ActiveMQConnectionFactory activeMQConnectionFactory;

    public static void main(String[] args) {
         activeMQConnectionFactory = new ActiveMQConnectionFactory("admin","admin","tcp://1.14.157.131:8161");
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageProducer producer = session.createProducer(aa);
            for (int i=0;i<3;i++)
            {
                TextMessage textMessage = session.createTextMessage("自定义字段" + i);
                producer.send(textMessage);
            }
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
