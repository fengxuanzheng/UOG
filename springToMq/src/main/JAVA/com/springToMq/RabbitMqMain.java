package com.springToMq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Delivery;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMqMain {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("1.14.157.131");
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("820606");
        connectionFactory.setPort(5672);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        System.out.println("开始消费");
        channel.basicConsume("confirmSelectQueue",true,(String consumerTag, Delivery message)->{
            System.out.println("消费者:"+new String(message.getBody()));
        }, System.out::println);
        System.out.println("消费结束");
        //channel.close();
        //connection.close();
    }

}
