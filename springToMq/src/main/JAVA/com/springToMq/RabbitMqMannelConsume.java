package com.springToMq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Delivery;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMqMannelConsume {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("1.14.157.131");
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("820606");
        connectionFactory.setPort(5672);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        //设置不公平分发
        channel.basicQos(5);
        System.out.println("开始消费");
        channel.basicConsume("hello",false,(String consumerTag, Delivery message)->{
            System.out.println("开始休眠");
            System.out.println("休眠20S");
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("休眠结束");
            System.out.println("消费者:"+new String(message.getBody()));
            channel.basicAck(message.getEnvelope().getDeliveryTag(),false);//手动ACk
        }, System.out::println);
        System.out.println("消费结束");
    }
}
