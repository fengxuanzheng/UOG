package com.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Slf4j
public class RabbitmqComsume {

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("1.14.157.131");
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("820606");
        connectionFactory.setPort(5672);
        Connection connection = connectionFactory.newConnection();

        new Thread(()->{
            try {
                Channel channel = connection.createChannel();
                
                channel.basicConsume("hello01",true,(item,messsgae)->{
                    log.info("t1:"+new String(messsgae.getBody()));
                },error->log.error(error));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        },"t1").start();
        new Thread(()->{
            try {
                Channel channel = connection.createChannel();
                channel.basicConsume("hello01",true,(item,messsgae)->{
                    log.info("t2:"+new String(messsgae.getBody()));
                },error->log.error(error));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        },"t2").start();


    }

    public void hel2() throws IOException, TimeoutException {

    }
}
