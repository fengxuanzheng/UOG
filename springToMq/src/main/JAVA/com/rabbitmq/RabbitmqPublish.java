package com.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

@Slf4j
public class RabbitmqPublish {

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("1.14.157.131");
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("820606");
        connectionFactory.setPort(5672);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("hello01", true, false, false, null);
        channel.basicPublish("", "hello01", null, "不要做欧尼酱了".getBytes(StandardCharsets.UTF_8));
        channel.close();
        connection.close();
    }

    public void hel1() throws IOException, TimeoutException {

    }
}
