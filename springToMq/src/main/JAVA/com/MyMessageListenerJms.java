package com;

import org.springframework.jms.listener.SessionAwareMessageListener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

public class MyMessageListenerJms implements SessionAwareMessageListener {

    @Override
    public void onMessage(Message message, Session session) throws JMSException {
        if (null!=message && message instanceof TextMessage)
        {
            TextMessage message1 = (TextMessage) message;
            System.out.println("springJMS监听器:"+message1.getText());
        }
    }
}
