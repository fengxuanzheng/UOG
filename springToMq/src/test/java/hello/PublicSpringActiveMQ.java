package hello;

import org.apache.activemq.command.ActiveMQTopic;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

public class PublicSpringActiveMQ {

    private ClassPathXmlApplicationContext classPathXmlApplicationContext;
    private JmsTemplate jmsTemplate;
    @BeforeEach
    public void init()
    {
        classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        jmsTemplate=classPathXmlApplicationContext.getBean(JmsTemplate.class);
    }

    @Test
    public void test()
    {
        JmsTemplate bean = classPathXmlApplicationContext.getBean(JmsTemplate.class);
        bean.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage springJMS = session.createTextMessage("自定义springJMS");
                return springJMS;
            }
        });
    }

    @Test
    public void test2()
    {
        String str = (String) jmsTemplate.receiveAndConvert();
        System.out.println("接受到消息"+str);

    }
    @Test
    public void test3()
    {
        Message message = jmsTemplate.sendAndReceive(session ->
                session.createTextMessage("你懂的")
        );
        System.out.println(message);
    }
    @Test
    public void test4()
    {
        ActiveMQTopic bean = classPathXmlApplicationContext.getBean(ActiveMQTopic.class);
        jmsTemplate.send(bean,session -> {
            System.out.println("开始生产主题");
            return session.createTextMessage("队列消息");});
    }
    @Test
    public void test5() throws JMSException {
        ActiveMQTopic bean = classPathXmlApplicationContext.getBean(ActiveMQTopic.class);
        TextMessage receive = (TextMessage) jmsTemplate.receive(bean);

        System.out.println(receive.getText());
    }
}
