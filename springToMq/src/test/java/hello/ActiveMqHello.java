package hello;


import org.apache.activemq.*;
import org.junit.jupiter.api.Test;

import javax.jms.Message;
import javax.jms.*;
import java.io.IOException;
import java.util.UUID;

public class ActiveMqHello {

    @Test
    public void test()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "nio://1.14.157.131:61608");
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            Queue cc = session.createQueue("cc");
            MessageProducer producer = session.createProducer(null);
            //MessageProducer producer = session.createProducer(aa);
            TextMessage textMessage = session.createTextMessage("自定义字段" );
            for (int i=0;i<3;i++)
            {
                textMessage.setText("自定义字段:"+i*20);
                textMessage.setStringProperty("name","vip");
                MapMessage mapMessage = session.createMapMessage();
                mapMessage.setString("a1","v1"+i);
                //mapMessage.setJMSDestination(cc);//通关send发送没效果.需要设置生产者里面那个

                producer.send(cc,mapMessage);
                producer.send(aa,textMessage);
            }
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
            Connection connection = null;

        try {
            connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageConsumer consumer = session.createConsumer(aa);
            while (true) {
                TextMessage receive = (TextMessage) consumer.receiveNoWait();
                if (null != receive) {
                    System.out.println(receive.getText());
                    System.out.println("信息属性:" + receive.getStringProperty("name"));
                } else {
                    break;
                }
            }
            consumer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }








    }

    @Test
    public void test3()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Connection connection = null;
        try {
            connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageConsumer consumer = session.createConsumer(aa);
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    if (null!=message && message instanceof TextMessage)
                    {
                        TextMessage message1 = (TextMessage) message;
                        try {
                            String text = message1.getText();
                            System.out.println(text);
                            System.out.println("CorrelationID:"+message1.getJMSCorrelationID());
                            System.out.println("ID:"+message1.getJMSMessageID());
                        } catch (JMSException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (null!=message && message instanceof MapMessage)
                    {
                        MapMessage message1 = (MapMessage) message;
                        String a1 = null;
                        try {
                            a1 = message1.getString("a1");
                        } catch (JMSException e) {
                            e.printStackTrace();
                        }
                        System.out.println("输出字段:"+a1);
                    }
                }
            });
            try {
                System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
            consumer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

   //************************************************************************主题订阅*********************************************
    @Test
    public void test4()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic bb = session.createTopic("bb");
            MessageProducer producer = session.createProducer(bb);
            for (int i=0;i<3;i++)
            {
                TextMessage textMessage = session.createTextMessage("自定义主题" + i);
                MapMessage mapMessage = session.createMapMessage();
                mapMessage.setString("a1","v1");
                producer.send(textMessage);
            }
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all for topic");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test5()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Connection connection = null;
        try {
            connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic bb = session.createTopic("topic");
            MessageConsumer consumer = session.createConsumer(bb);
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    if (null!=message && message instanceof TextMessage)
                    {
                        TextMessage message1 = (TextMessage) message;
                        try {
                            String text = message1.getText();
                            System.out.println(text);
                        } catch (JMSException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            try {
                System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
            consumer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    //********************************主题的持久化发布和订阅模式********************************

    @Test
    public void topicProduct()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic topic = session.createTopic("topic");
            connection.start();
            MessageProducer producer = session.createProducer(topic);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            for (int i=0;i<3;i++)
            {
                TextMessage textMessage = session.createTextMessage("自定义主题" + i);
                MapMessage mapMessage = session.createMapMessage();
                mapMessage.setString("a1","v1");
                producer.send(textMessage);
            }
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all for topic");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void subsctiber()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Connection connection = null;
        Session session=null;
        try {
            connection = activeMQConnectionFactory.createConnection();
            //connection.start();
            connection.setClientID("name1");
             session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic topic = session.createTopic("topic");
                TopicSubscriber durableSubscriber = session.createDurableSubscriber(topic, "自定义>>>");
                connection.start();
                Message receive = durableSubscriber.receive(2000L);
                while (null!=receive)
                {
                    TextMessage receive1 = (TextMessage) receive;
                    System.out.println(receive1.getText());
                     receive = durableSubscriber.receive(2000L);
                }

        } catch (JMSException e) {
                e.printStackTrace();
            }
        try {
            connection.close();
            session.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }

    //**************************************额外属性测试**************************
    @Test
    public void test6()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            Queue cc = session.createQueue("cc");
            //MessageProducer producer = session.createProducer(null);
            MessageProducer producer = session.createProducer(aa);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);//设置此生产者非持久化
            for (int i=0;i<3;i++)
            {
                TextMessage textMessage = session.createTextMessage("自定义字段" + i);

                producer.send(textMessage);
            }
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    //************************************事务和签收*********************************
    /*
    * 对于事务来说,生产者和消费者事务没有绝对更新,不是说生产者或消费者开了事务另外一个一定要开,生产者开事务是为了事务原子性,一连串提交后出错可以回滚
    * 对消费者来说,开启事务后签收参数就不起作用了,所以事务大于签收,开始事务后再没提交之前可以反复消费(没提交前不算消费)提交后才算真正完成消费
    * */
    @Test
    public void test7()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Session session=null;
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
             session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            Queue cc = session.createQueue("cc");
            //MessageProducer producer = session.createProducer(null);
            MessageProducer producer = session.createProducer(aa);
            for (int i=0;i<3;i++)
            {
                TextMessage textMessage = session.createTextMessage("自定义字段" + i);
                textMessage.setStringProperty("name","vip");

                producer.send(textMessage);
            }
            //session.commit();//开启事务后关闭前一定要提交
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
            try {
                session.rollback();//开启事务后出现错误可以回滚
            } catch (JMSException jmsException) {
                jmsException.printStackTrace();
            }
        }
    }

    @Test
    public void test8()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Connection connection = null;
        Session session=null;
        try {
            connection = activeMQConnectionFactory.createConnection();
            connection.start();
             session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageConsumer consumer = session.createConsumer(aa);
            TextMessage receive = (TextMessage) consumer.receive(2000L);
            while (null!=receive) {
                System.out.println(receive.getText());
                    System.out.println("信息属性:" + receive.getStringProperty("name"));
                    //receive.acknowledge();
                receive = (TextMessage) consumer.receive(2000L);

            }
            session.commit();//如果消费者开启事务如果没真正提交就可以重复消费，提交后才能真正完成消费
            consumer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test9()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Session session=null;
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            Queue cc = session.createQueue("cc");
            //MessageProducer producer = session.createProducer(null);
            MessageProducer producer = session.createProducer(aa);
            for (int i=0;i<3;i++)
            {
                TextMessage textMessage = session.createTextMessage("自定义字段" + i);
                textMessage.setStringProperty("name","vip");

                producer.send(textMessage);
            }

            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();

        }
    }

    @Test
    public void test10()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Connection connection = null;
        Session session=null;
        try {
            connection = activeMQConnectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageConsumer consumer = session.createConsumer(aa);
            TextMessage receive = (TextMessage) consumer.receive(2000L);
            while (null!=receive) {
                System.out.println(receive.getText());
                System.out.println("信息属性:" + receive.getStringProperty("name"));
                receive.acknowledge();//开启手动签收后,客户端收到消息需要收到签收

                receive = (TextMessage) consumer.receive(2000L);

            }
            consumer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void transactionAndAckMode1()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
       // ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://localhost:61616");
        Session session=null;
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
            session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            Queue cc = session.createQueue("cc");
            //MessageProducer producer = session.createProducer(null);
            MessageProducer producer = session.createProducer(aa);
            for (int i=0;i<3;i++)
            {
                TextMessage textMessage = session.createTextMessage("自定义字段" + i);
                textMessage.setStringProperty("name","vip");
                textMessage.setJMSMessageID(UUID.randomUUID().toString().substring(1,8));
                System.out.println(textMessage.getJMSMessageID());

                producer.send(textMessage);

            }
            int k=10/0;
            session.commit();
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e ) {
            e.printStackTrace();
            try {
                session.commit();
                System.out.println("错误提交");
            } catch (JMSException jmsException) {
                jmsException.printStackTrace();
            }

        }
        catch (Exception g)
        {
            try {
                session.commit();
                System.out.println("错误提交");
            } catch (JMSException jmsException) {
                jmsException.printStackTrace();
            }
        }
    }

    @Test
    public void transactionAndAckMode2() {

        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory("tcp://1.14.157.131:61616");
        //ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        Connection connection = null;
        Session session = null;
        try {
            connection = activeMQConnectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageConsumer consumer = session.createConsumer(aa);
            TextMessage receive = (TextMessage) consumer.receive(2000L);
            while (null != receive) {
                System.out.println(receive.getText());
                System.out.println("信息属性:" + receive.getStringProperty("name"));
                System.out.println("消息ID:"+receive.getJMSMessageID());
                receive.acknowledge();//开启手动签收后,客户端收到消息需要收到签收
                receive = (TextMessage) consumer.receive(2000L);

            }
            //session.commit();
            consumer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    //************************************强制开启生产者异步发送到MQ********************************************
    @Test
    public void test11()
    {

        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "nio://1.14.157.131:61608");
        activeMQConnectionFactory.setUseAsyncSend(true);
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            ActiveMQMessageProducer producer = (ActiveMQMessageProducer) session.createProducer(aa);
            //MessageProducer producer = session.createProducer(aa);
            TextMessage textMessage = session.createTextMessage("自定义字段" );
            for (int i=0;i<3;i++)
            {
                textMessage.setText("自定义字段:"+i*20);
                textMessage.setJMSMessageID(UUID.randomUUID().toString().substring(1,6)+i);//设置消息的ID
                String jmsMessageID = textMessage.getJMSMessageID();
                textMessage.setStringProperty("name","vip");
                producer.send(textMessage, new AsyncCallback() {
                    @Override
                    public void onSuccess() {
                        System.out.println("ID:"+jmsMessageID+"  发送成功");
                    }

                    @Override
                    public void onException(JMSException exception) {
                        System.out.println("ID:"+jmsMessageID+"  发送失败");
                    }
                });
            }
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    //****************************************延迟消息投递*****************************************
    @Test
    public void test12()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "nio://1.14.157.131:61608");
        try {
            Connection connection = activeMQConnectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageProducer producer = session.createProducer(aa);
            //MessageProducer producer = session.createProducer(aa);
            TextMessage textMessage = session.createTextMessage("自定义字段" );
            for (int i=0;i<3;i++)
            {
                textMessage.setText("自定义字段:"+i*20);
                textMessage.setJMSMessageID(UUID.randomUUID().toString().substring(1,6)+i);//设置消息的ID
                textMessage.setJMSCorrelationID("自定义ID");
                String jmsMessageID = textMessage.getJMSMessageID();
                textMessage.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY,3*1000);
                textMessage.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_PERIOD,4*1000);
                textMessage.setIntProperty(ScheduledMessage.AMQ_SCHEDULED_REPEAT,5);
                textMessage.setStringProperty("name","vip");
                producer.send(textMessage);
            }
            producer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    //*************************88消息重发和死信队列*******************************
    @Test
    public void test13()
    {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory( "tcp://1.14.157.131:61616");
        Connection connection = null;
        Session session=null;
        RedeliveryPolicy topicPolicy = new RedeliveryPolicy();
        topicPolicy.setRedeliveryDelay(1000);
        topicPolicy.setMaximumRedeliveries(3);
        activeMQConnectionFactory.setRedeliveryPolicy(topicPolicy);
        try {
            connection = activeMQConnectionFactory.createConnection();
            connection.start();
            session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            Queue aa = session.createQueue("aa");
            MessageConsumer consumer = session.createConsumer(aa);
            TextMessage receive = (TextMessage) consumer.receive(1000L);
            while (null!=receive) {
                System.out.println(receive.getText());
                System.out.println("信息属性:" + receive.getStringProperty("name"));
                //receive.acknowledge();
                receive = (TextMessage) consumer.receive(1000L);

            }
            //session.commit();//如果消费者开启事务如果没真正提交就可以重复消费，提交后才能真正完成消费
            consumer.close();
            session.close();
            connection.close();
            System.out.println("message send to all");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

}
