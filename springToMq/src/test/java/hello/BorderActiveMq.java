package hello;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;

public class BorderActiveMq {


    public static void main(String[] args) throws Exception {


        BrokerService brokerService = new BrokerService();
        brokerService.setUseJmx(true);
        TransportConnector transportConnector = brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
        System.in.read();

    }
}
