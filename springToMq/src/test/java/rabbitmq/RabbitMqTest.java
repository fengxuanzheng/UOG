package rabbitmq;

import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.concurrent.*;
@Slf4j
public class RabbitMqTest {
    private ConnectionFactory connectionFactory;
    private static String queuename="hello";
    private static String defaultExchange="amq.fanout";
    private static String defaultDirectExchange="amq.direct";
    private static String defaultTopicExchange="amq.topic";
    private static String normalQueue="normal-queue";
    private static String normalExchange="normal-Exchange";
    private static String deadQueue="dead-queue";
    private static String deadExchangee="dead-exchange";

    @BeforeEach
    public void init() throws URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("link.mahoushaojiu.top");
        connectionFactory.setUsername("uog");
        connectionFactory.setPassword("820606");
        connectionFactory.setPort(5672);
        this.connectionFactory=connectionFactory;
    }

   @Test
   public void hel1() throws IOException, TimeoutException {
        Connection connection = connectionFactory.newConnection();
       Channel channel = connection.createChannel();
       channel.queueDeclare("hello01", false, false, false, null);
       channel.basicPublish("", "hello01", null, "不要做欧尼酱了".getBytes(StandardCharsets.UTF_8));
   }
    @Test
    public void hel2() throws IOException, TimeoutException {
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.basicConsume("hello01",true,(item,messsgae)->{
            log.info(item);
            log.info(new String(messsgae.getBody()));
        },error->log.error(error));

    }

    @Test
    public void test() throws IOException, TimeoutException {
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(queuename, false, false, false, null);
        channel.exchangeDeclare("first", BuiltinExchangeType.FANOUT,true);
        channel.queueBind("yy","first","");
        int i=0;
        while (true)
        {

            channel.basicPublish("first","",MessageProperties.PERSISTENT_BASIC,"自定义消息喽".getBytes());
            i++;
            if (i==8)
            {
                break;
            }
        }
        System.out.println("消息发送完毕");

        //channel.close();
        //connection.close();


    }
    @Test
    public void test6() throws IOException, TimeoutException {
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.basicConsume("yy",true,(String consumerTag, Delivery message)->{
            System.out.println("消费信息:"+new String(message.getBody()));
        }, System.out::println);

    }

    @Test
    public void test2() throws IOException, TimeoutException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.basicConsume("confirmSelectQueue",false,(String consumerTag, Delivery message)->{
            System.out.println("消费者:"+new String(message.getBody()));
            String s = new String(message.getBody());
            Integer substring = Integer.valueOf(s.substring(10));
            if (substring==999)
            {
                countDownLatch.countDown();
            }
            channel.basicAck(message.getEnvelope().getDeliveryTag(),false);
        }, System.out::println);
        countDownLatch.await();
    }


    //***************************消息持久化(最终措施,发布确认)*********************************

    @Test
    public void test3() throws IOException, TimeoutException, InterruptedException {
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        //开启发布确认
        channel.confirmSelect();
        channel.queueDeclare("confirmSelectQueue", true, false, false, null);
        //单次确认
        for (int i=0;i<1000;i++)
        {
            String messge="messageId:"+i;
            channel.basicPublish("","confirmSelectQueue",MessageProperties.PERSISTENT_BASIC,messge.getBytes());
            boolean b = channel.waitForConfirms();//单个消息马上确认确认
            if (b)
            {
                System.out.println("消息发送完成,ID:" + i);
            }
        }
        //批量确认
        for (int i=0;i<1000;i++)
        {
            String messge="messageId:"+i;
            channel.basicPublish("","confirmSelectQueue",MessageProperties.PERSISTENT_BASIC,messge.getBytes());
        }
        boolean b = channel.waitForConfirms();
        System.out.println("发送完毕");
    }

   @Test
   public void test4() throws IOException, TimeoutException, InterruptedException {
       CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
       channel.queueDeclare("confirmSelectQueue", true, false, false, null);
        //开启发布确认
        channel.confirmSelect();
        /*
        * 线程安全有序的哈希表,用于高并发情况下
        * 1.轻松将序号和消息进行关联
        * 2.批量删除条目
        * 3.支持高并发
        * */
       ConcurrentSkipListMap<Long, String> longStringConcurrentSkipListMap = new ConcurrentSkipListMap<>();
       //异步监听器
        channel.addConfirmListener((long deliveryTag, boolean multiple)->{
            System.out.println("消息发送成功:"+deliveryTag);
            //删除已经发送成功消息,剩下就算未发送成功的
            if (multiple)
            {
                //批量情况下,批量删除
                ConcurrentNavigableMap<Long, String> longStringConcurrentNavigableMap = longStringConcurrentSkipListMap.headMap(deliveryTag+1);
                longStringConcurrentNavigableMap.clear();
            }
            else
            {
                System.out.println(deliveryTag);
                longStringConcurrentSkipListMap.remove(deliveryTag);
            }
        },(long deliveryTag, boolean multiple)->{
            System.out.println("消息发送失败:"+deliveryTag);
        });
        for (int i=0;i<1000;i++)
        {

            String messge="messageId:"+i;
            //记入需要发送消息的总和
            longStringConcurrentSkipListMap.put(channel.getNextPublishSeqNo(),messge);
            channel.basicPublish("","confirmSelectQueue",MessageProperties.PERSISTENT_BASIC,messge.getBytes());

        }
        System.out.println("发送完毕");
        countDownLatch.await(5, TimeUnit.SECONDS);
       System.out.println("未确认消息个数:"+longStringConcurrentSkipListMap.size());
       longStringConcurrentSkipListMap.forEach((key,value)->{
           System.out.println(key+"*********"+value);
       });
   }

   //**********************************交换机及订阅************************************
    @Test
    public void test5() throws IOException, TimeoutException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        String queue = channel.queueDeclare().getQueue();//声明一个临时队列
        channel.queueBind(queue,defaultExchange,"");
        channel.basicConsume(queue,true,(String consumerTag, Delivery message)->{
            System.out.println("消费订阅:"+new String(message.getBody()));
        }, System.out::println);
        countDownLatch.await();
    }
    @Test
    public void test7() throws IOException, TimeoutException {
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();

        for (int i=0;i<100;i++)
        {
            String message="订阅模式:"+i;
            channel.basicPublish(defaultExchange,"",null,message.getBytes());
        }
        System.out.println("消息发送完毕");
    }

    @Test
    public void test8() throws IOException, TimeoutException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue,defaultDirectExchange,"info");
        channel.queueBind(queue,defaultDirectExchange,"waring");

        channel.basicConsume(queue,true,(String consumerTag, Delivery message)->{
            System.out.println("消费订阅:"+new String(message.getBody()));
        }, System.out::println);
        countDownLatch.await();
    }
    @Test
    public void test9() throws IOException, TimeoutException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue,defaultDirectExchange,"error");
        channel.queueBind(queue,defaultDirectExchange,"waring");

        channel.basicConsume(queue,true,(String consumerTag, Delivery message)->{
            System.out.println("消费订阅:"+new String(message.getBody()));
        }, System.out::println);
        countDownLatch.await();
    }

    @Test
    public void test10() throws IOException, TimeoutException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        for (int i=0;i<100;i++)
        {
            String message="直接模式:"+i;
           // channel.basicPublish(defaultDirectExchange,"info",null,message.getBytes());
            channel.basicPublish(defaultDirectExchange,"waring",null,message.getBytes());
        }
        System.out.println("消息发送完毕");
    }

    @Test
    public void test11() throws IOException, TimeoutException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        try {
            Channel channel = connection.createChannel();
            String queue = channel.queueDeclare().getQueue();
            channel.queueBind(queue,defaultTopicExchange,"*.orange.*");

            channel.basicConsume(queue,false,(String consumerTag, Delivery message)->{
                System.out.println("消费主题:"+new String(message.getBody()));
               // channel.basicReject(message.getEnvelope().getDeliveryTag(),false);

            }, System.out::println);
            countDownLatch.await();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void test12() throws IOException, TimeoutException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        try {
            Channel channel = connection.createChannel();
            String queue = channel.queueDeclare().getQueue();
            channel.queueBind(queue,defaultTopicExchange,"*.*.rabbit");
            channel.queueBind(queue,defaultTopicExchange,"lazy.#");
            channel.basicConsume(queue,true,(String consumerTag, Delivery message)->{
                System.out.println("消费主题:"+new String(message.getBody()));
            }, System.out::println);
            countDownLatch.await();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void test13() throws IOException, TimeoutException {
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("quick.orange.rabbit","Q1Q2接收");
        stringStringHashMap.put("lazy.orange.elephant","Q1Q2接收");
        stringStringHashMap.put("quick.orange.fox","Q1接收");
        stringStringHashMap.put("lazy.brown.fox","Q2接收");
        stringStringHashMap.put("lazy.ping.rabbit","Q2接收");
        stringStringHashMap.put("quick.orange.male.rabbit","四个单词不匹配任何绑定会被丢弃");
      stringStringHashMap.forEach((key,value)->{
          String message="直接模式:"+key;
          // channel.basicPublish(defaultDirectExchange,"info",null,message.getBytes());
          try {
              channel.basicPublish(defaultTopicExchange,key,null,value.getBytes());
          } catch (IOException e) {
              e.printStackTrace();
          }
      });


        System.out.println("消息发送完毕");
    }


    //****************************死信**********************************
    @Test
    public void test14() throws IOException, TimeoutException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(normalExchange,BuiltinExchangeType.DIRECT);
        channel.exchangeDeclare(deadExchangee,BuiltinExchangeType.DIRECT);


        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        //stringObjectHashMap.put("x-message-ttl",10000);//设置过期时间,也可以从生产着那边指定
        //stringObjectHashMap.put("x-max-length",6);//设置队列最大长度
        stringObjectHashMap.put("x-dead-letter-exchange",deadExchangee);//设置正常队列死信交换机
        stringObjectHashMap.put("x-dead-letter-routing-key","deadList");//设置死信的routingKey
        channel.queueDeclare(normalQueue,true,false,false,stringObjectHashMap);
        channel.queueDeclare(deadQueue,true,false,false,null);
        //绑定普通交换机和普通队列以及死信交换机和死信队列
        channel.queueBind(normalQueue,normalExchange,"normal");
        channel.queueBind(deadQueue,deadExchangee,"deadList");
        channel.basicConsume(normalQueue,false,(String consumerTag, Delivery message)->{
            System.out.println("消费主题:"+new String(message.getBody()));
            channel.basicReject(message.getEnvelope().getDeliveryTag(),false);
        }, System.out::println);
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test15() throws IOException, TimeoutException {
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        //AMQP.BasicProperties build = new AMQP.BasicProperties().builder().expiration("10000").build();//设置消息过期时间,如果在消费端设置了就不需要
        for (int i=0;i<10;i++)
        {
            String message="死信消息:"+i;
            channel.basicPublish(normalExchange,"normal",null,message.getBytes());
        }
        System.out.println("消息发送完毕");
    }

    @Test
    public void test16() throws IOException, TimeoutException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        //AMQP.BasicProperties build = new AMQP.BasicProperties().builder().expiration("10000").build();//设置消息过期时间,如果在消费端设置了就不需要
        channel.basicConsume(deadQueue,true,(String consumerTag, Delivery message)->{
            System.out.println("消费主题:"+new String(message.getBody()));
        }, System.out::println);
        countDownLatch.await();
    }

}
