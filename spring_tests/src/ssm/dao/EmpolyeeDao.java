package ssm.dao;


import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import ssm.pojo.Empolyee;

import java.util.List;
import java.util.Map;

public interface EmpolyeeDao {
    public Empolyee getEmpbyid(Integer id);
    public Empolyee getEmpbyidandempname(@Param("id") Integer id, @Param("empname") String empname);
    public Empolyee getEmpbyidformap(Map<String,Object> map);
    public int updateEmployee(Empolyee empolyee);
    public  int deleteEmployee(Integer id);
    public int insertEmployee(Empolyee empolyee);
    public int insertEmployee2(Empolyee empolyee);
    public List<Empolyee> getEMps();
    public Map<String,Object> getemptomaps(Integer id);
    public List<Empolyee> getEmpsPage();
    public int addemps(Empolyee empolyee);
    public int addemnu(Empolyee empolyee);
    public int addmapsforlist(@Param("empolyees") List<Empolyee> empolyees);

    @MapKey("id")
    public Map<String,Empolyee> getemptomap();



}
