package ssm.dao;

import ssm.pojo.SecurityDoor;

import java.util.List;

public interface SecurityDoorDAO {
    public List<SecurityDoor> getDoorCurrentStatus();
    public List<SecurityDoor>getDoorStatusList();
}
