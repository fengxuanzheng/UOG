package ssm.dao;

import ssm.pojo.ShiroUsers;

public interface ShiroUsersDAO {

    public void inserteUser(ShiroUsers shiroUsers);
    public ShiroUsers  getUser(ShiroUsers shiroUsers);
}
