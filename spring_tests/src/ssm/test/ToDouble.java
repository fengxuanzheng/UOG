package ssm.test;

import java.text.DecimalFormat;

public class ToDouble {

    public static String getdouble(short a1,short a2, short a3, short a4)
    {
        byte[] bytes = ByteUtils.shortToByte2((short) a1);
        byte[] bytes1 = ByteUtils.shortToByte2((short) a2);

        byte[] bytes2 = ByteUtils.shortToByte2((short) a3);

        byte[] bytes3 = ByteUtils.shortToByte2((short) a4);

        byte[] newbyte = new byte[8];
        newbyte[0]=bytes[0];
        newbyte[1]=bytes[1];
        newbyte[2]=bytes1[0];
        newbyte[3]=bytes1[1];
        newbyte[4]=bytes2[0];
        newbyte[5]=bytes2[1];
        newbyte[6]=bytes3[0];
        newbyte[7]=bytes3[1];

        double v = ByteUtils.byte8ToDouble(newbyte);
        DecimalFormat decimalFormat = new DecimalFormat("###0.00");
        String format = decimalFormat.format(v);
        return format;

    }
}
