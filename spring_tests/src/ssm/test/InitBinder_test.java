package ssm.test;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class InitBinder_test {



    @InitBinder
    public void stringtodate(WebDataBinder webDataBinder)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        CustomDateEditor customDateEditor = new CustomDateEditor(simpleDateFormat, true);
        webDataBinder.registerCustomEditor(Date.class,customDateEditor);
    }


    @ResponseBody
    @RequestMapping(value = "/dates")
    public String dates(Date date)
    {

        DateFormat dateTimeInstance = DateFormat.getDateTimeInstance();
        String format = dateTimeInstance.format(date);
        System.out.println("打印日期"+format);
        return format;
    }


}
