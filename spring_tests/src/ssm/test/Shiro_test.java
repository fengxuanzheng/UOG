package ssm.test;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class Shiro_test {
    public static void main(String[] args) {

        String hashAlgorithmName="MD5";
        Object credentials="123456";
        Object salt=ByteSource.Util.bytes("user");
        int hashIterations=1024;

        SimpleHash simpleHash = new SimpleHash(hashAlgorithmName, credentials, salt, hashIterations);
        System.out.println(simpleHash);


    }
}
