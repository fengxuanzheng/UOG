package ssm.utils;

import org.springframework.beans.factory.annotation.Autowired;
import ssm.dao.Shiro_usernameMapper;

public class GetSalt {

    @Autowired
    private  Shiro_usernameMapper shiro_usernameMapper;

    public  String getsalt(String username)
    {
        String getsalt = shiro_usernameMapper.getsalt(username);
        return getsalt;
    }
}
