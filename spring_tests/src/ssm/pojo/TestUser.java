package ssm.pojo;

public class TestUser {
    private String names;
    private String password;

    public TestUser() {
    }

    public TestUser(String names, String password) {
        this.names = names;
        this.password = password;
    }

    @Override
    public String toString() {
        return "TestUser{" +
                "names='" + names + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
