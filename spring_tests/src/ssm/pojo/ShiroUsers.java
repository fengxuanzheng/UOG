package ssm.pojo;

public class ShiroUsers {
    private Integer id;
    private String username;
    private String password;

    public ShiroUsers(Integer id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public ShiroUsers(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public ShiroUsers() {
    }

    @Override
    public String toString() {
        return "ShiroUsers{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

