package ssm.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class SecurityDoor {
  private Integer id;
  private Integer doorid;
  private String doorname;
  private String doorStatus;
  @JsonFormat(pattern ="YYY-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;

  public SecurityDoor(Integer id, Integer doorid, String doorname, String doorStatus, Date updateTime) {
    this.id = id;
    this.doorid = doorid;
    this.doorname = doorname;
    this.doorStatus = doorStatus;
    updateTime = updateTime;
  }

  public SecurityDoor() {
  }

  @Override
  public String toString() {
    return "SecurityDoor{" +
            "id=" + id +
            ", doorid=" + doorid +
            ", doorname='" + doorname + '\'' +
            ", doorStatus='" + doorStatus + '\'' +
            ", UpdateTime=" + updateTime +
            '}';
  }

  public String getDoorname() {
    return doorname;
  }

  public void setDoorname(String doorname) {
    this.doorname = doorname;
  }

  public String getDoorStatus() {
    return doorStatus;
  }

  public void setDoorStatus(String doorStatus) {
    this.doorStatus = doorStatus;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getDoorid() {
    return doorid;
  }

  public void setDoorid(Integer doorid) {
    this.doorid = doorid;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }
}

