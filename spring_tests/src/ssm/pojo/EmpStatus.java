package ssm.pojo;

public enum EmpStatus {
    LOGIN(100,"用户登入"),
    LOGOUT(200,"用户登出"),
    REMOVE(300,"用户不存在");

    private Integer id;
    private String status;

    private EmpStatus(Integer id, String status) {
        this.id=id;
        this.status=status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static EmpStatus getEMPbyid(String status)
    {
        switch (status)
        {
            case "用户登入":
                return LOGIN;
            case "用户登出":
                return LOGOUT;
            case "用户不存在":
                return REMOVE;
            default:
                return REMOVE;
        }
    }
}
