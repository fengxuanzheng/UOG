package ssm.pojo;

public class Login {
    private Integer id;
    private  String adim_name;
    private Integer passwords;

    public Login() {
    }

    public Login(Integer id, String adim_name, Integer passwords) {
        this.id = id;
        this.adim_name = adim_name;
        this.passwords = passwords;
    }

    @Override
    public String toString() {
        return "Login{" +
                "id=" + id +
                ", adim_name='" + adim_name + '\'' +
                ", passwords=" + passwords +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdim_name() {
        return adim_name;
    }

    public void setAdim_name(String adim_name) {
        this.adim_name = adim_name;
    }

    public Integer getPasswords() {
        return passwords;
    }

    public void setPasswords(Integer passwords) {
        this.passwords = passwords;
    }


}
