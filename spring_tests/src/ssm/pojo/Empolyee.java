package ssm.pojo;

//在批量取别名下位个别类另外取别名
//@Alias("emp")
public class Empolyee {
    private Integer id;
    private String empName;
    private String email;
    private Integer gender;
    private String loginAccount;
    private EmpStatus empStatus;

    public Empolyee() {
    }

    public Empolyee(Integer id, String empName, String email, Integer gender) {
        this.id = id;
        this.empName = empName;
        this.email = email;
        this.gender = gender;
    }

    public Empolyee(Integer id, String empName, String email, Integer gender, String loginAccount) {
        this.id = id;
        this.empName = empName;
        this.email = email;
        this.gender = gender;
        this.loginAccount = loginAccount;
    }

    public EmpStatus getEmpStatus() {
        return empStatus;
    }

    public void setEmpStatus(EmpStatus empStatus) {
        this.empStatus = empStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getLoginAccount() {
        return loginAccount;
    }

    public void setLoginAccount(String loginAccount) {
        this.loginAccount = loginAccount;
    }

    @Override
    public String toString() {
        return "Empolyee{" +
                "id=" + id +
                ", empName='" + empName + '\'' +
                ", email='" + email + '\'' +
                ", gender=" + gender +
                ", loginAccount='" + loginAccount + '\'' +
                ", empStatus=" + empStatus +
                '}';
    }
}
