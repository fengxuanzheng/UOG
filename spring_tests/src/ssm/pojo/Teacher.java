package ssm.pojo;

import java.io.Serializable;
import java.util.Date;

public class Teacher implements Serializable {
   private Integer id;
   private String teacherName;
    private String classname;
    private String  address;
    private Date birthdate;

    public Teacher(Integer id, String teacherName, String classname, String address, Date birthdate) {
        this.id = id;
        this.teacherName = teacherName;
        this.classname = classname;
        this.address = address;
        this.birthdate = birthdate;
    }

    public Teacher() {
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", teacherName='" + teacherName + '\'' +
                ", classname='" + classname + '\'' +
                ", address='" + address + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}
