package ssm.warningdoor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ssm.dao.SecurityDoorDAO;
import ssm.pojo.SecurityDoor;

import java.util.List;

@Controller
public class HeXiSecurityController {

    @Autowired
    private SecurityDoorDAO securityDoorDAO;

    @RequestMapping("/safedoor")
    @ResponseBody
    public List<SecurityDoor> getdoorstatus()
    {
        System.out.println("成功调用..............");
        List<SecurityDoor> getdoorstatus = securityDoorDAO.getDoorStatusList();
        return getdoorstatus;

    }

    @RequestMapping("/cureentsafedoor")
    @ResponseBody
    public List<SecurityDoor> getcureentsafedoor()
    {
        System.out.println("方法以调用");
        List<SecurityDoor> doorCurrentStatus = securityDoorDAO.getDoorCurrentStatus();
        System.out.println(doorCurrentStatus);
        return doorCurrentStatus;
    }
}
