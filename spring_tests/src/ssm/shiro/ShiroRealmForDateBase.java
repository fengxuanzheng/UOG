package ssm.shiro;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import ssm.dao.Shiro_usernameMapper;
import ssm.pojo.Shiro_username;
import ssm.pojo.Shiro_usernameExample;

import java.util.HashSet;
import java.util.List;

public class ShiroRealmForDateBase extends AuthorizingRealm {
    @Autowired
    private Shiro_usernameMapper shiro_usernameMapper;
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = principals.getPrimaryPrincipal().toString();
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("码农");
        HashSet<String> principal = new HashSet<>();
        principal.add("sys:user:list");
        principal.add("sys:user:info");
        principal.add("sys:user:create");
        principal.add("sys:user:delete");
        principal.add("sys:user:updata");
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addRoles(hashSet);
        simpleAuthorizationInfo.addStringPermissions(principal);


        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken=(UsernamePasswordToken)token;
        String username = usernamePasswordToken.getUsername();
        Object credentials = usernamePasswordToken.getCredentials();
        String password=new String((char[])credentials);
        Shiro_username shiroUsers = new Shiro_username();
        shiroUsers.setUsername(username);
        shiroUsers.setPassword(password);
        Shiro_usernameExample shiro_usernameExample = new Shiro_usernameExample();
        shiro_usernameExample.createCriteria().andUsernameEqualTo(username);
        List<Shiro_username> shiro_usernames = shiro_usernameMapper.selectByExample(shiro_usernameExample);
        if(shiro_usernames==null || shiro_usernames.isEmpty())
        {
            throw  new AuthenticationException("认证失败");
        }
        String getsalt = shiro_usernameMapper.getsalt(username);
        ByteSource bytes = ByteSource.Util.bytes(getsalt);

        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(shiro_usernames.get(0).getUsername(), shiro_usernames.get(0).getPassword(),bytes, getName());
        return simpleAuthenticationInfo;
    }
}
