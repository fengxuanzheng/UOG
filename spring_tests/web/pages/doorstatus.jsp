<%--
  Created by IntelliJ IDEA.
  User: UOG
  Date: 2021/3/29
  Time: 9:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("ctp",request.getContextPath());
%>
<html>
<head>
    <title>安全门状态</title>
    <meta charset="UTF-8">
    <script type="text/javascript" src="${ctp}/scripts/jquery-1.7.2.js"></script>
    <script type="text/javascript" src="${ctp}/scripts/echarts.js"></script>
     <script type="text/javascript" src="${ctp}/scripts/gridjs.production.min.js"></script>
    <link href="${ctp}/scripts/mermaid.min.css" rel="stylesheet"/>


</head>
<body>

<div id="wrapper"></div>
<button id="opensafe" style="height: 70px ;width: 70px">手动更新</button><br>


</body>

<script type="text/javascript">
    $(function () {
        tableupdate();
       setInterval(tableupdate,3600*1000);

       $("#opensafe").click(function () {
           location.reload();

       })

    })


    var nameslable=new Array();


    function AutoUpdate() {
        $("#doorsatus").html("");
        $.ajax({
            search: true,
            url:"${ctp}/safedoor",
            type:"POST",
            dataType:"json",
            async:false,
            success:function (data) {
                $.each(data,function (i,n) {
                    var name=n.doorname;
                    var status=n.doorStatus;
                    var nameid=n.doorid;
                    var updatetime=n.updateTime;
                    nameslable[i]=new Array();
                    nameslable[i][0]=nameid;
                    nameslable[i][1]=name;
                    status= status==1?"关闭":"打开";
                    nameslable[i][2]=status;
                    nameslable[i][3]=updatetime;


                })
            }
        })

    }
    function tableupdate() {
       AutoUpdate();
        new gridjs.Grid({
            columns: ["安全门标识","安全门","状态","现在时间"],
             data:nameslable,
            sort: true,
            pagination: {
                enabled: true,
                limit: 20,
                summary: false
            }


        }).render(document.getElementById("wrapper"));
    }

</script>

</html>
