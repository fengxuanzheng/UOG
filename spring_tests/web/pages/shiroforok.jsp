<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: UOG
  Date: 2021/3/21
  Time: 20:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%
        pageContext.setAttribute("ctp",request.getContextPath());
    %>
</head>
<body>
登入成功:欢迎<shiro:principal></shiro:principal><br/>
<shiro:user>
<a href="${ctp}/logout">登出</a><br/>
</shiro:user>
<shiro:hasRole name="admin">
    <a href="${ctp}/pages/admin.jsp">用户登入后</a><br/>
</shiro:hasRole>
<shiro:hasRole name="user">

    <a href="${ctp}/pages/user.jsp">用户个人界面</a><br/>
</shiro:hasRole>


<a href="${ctp}/howtoadmin">注解权限测试</a>

</body>
</html>
