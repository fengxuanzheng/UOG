<%--
  Created by IntelliJ IDEA.
  User: UOG
  Date: 2021/3/29
  Time: 9:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("ctp",request.getContextPath());
%>
<html>
<head>
    <title>安全门状态</title>
    <meta charset="UTF-8">
    <script type="text/javascript" src="${ctp}/scripts/jquery-1.7.2.js"></script>
     <script type="text/javascript" src="${ctp}/scripts/jsgrid.min.js"></script>
    <link href="${ctp}/scripts/jsgrid-theme.min.css" />
    <link href="${ctp}/scripts/jsgrid.min.css"/>


</head>
<body>

<div id="jsGrid"></div>
<a id="opensafe" href="javasrcrip:" style="height: 70px ;width: 70px">手动更新</a><br>
<a href="${ctp}/pages/doorstatus.jsp" style="height: 70px ;width: 70px">查看历史记录</a>


</body>

<script type="text/javascript">

    var nameslable=new Array();
    var tableobje;

    function AutoUpdate() {
        $.ajax({
            url:"${ctp}/cureentsafedoor",
            type:"POST",
            dataType:"json",
            async:false,
            success:function (data) {
                $.each(data,function (i,n) {
                    var name=n.doorname;
                    var status=n.doorStatus;
                    var nameid=n.doorid;
                    var updatetime=n.updateTime;
                     nameslable[i]=new Array();
                     nameslable[i][0]=nameid;
                     nameslable[i][1]=name;
                    status= status==1?"关闭":"打开";
                     nameslable[i][2]=status;
                     nameslable[i][3]=updatetime;

                })
            }
        })
    }

    var clients = [
        { "Name": "Otto Clay", "Age": 25, "Country": 1, "Address": "Ap #897-1459 Quam Avenue", "Married": false },
        { "Name": "Connor Johnston", "Age": 45, "Country": 2, "Address": "Ap #370-4647 Dis Av.", "Married": true },
        { "Name": "Lacey Hess", "Age": 29, "Country": 3, "Address": "Ap #365-8835 Integer St.", "Married": false },
        { "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
        { "Name": "Ramona Benton", "Age": 32, "Country": 3, "Address": "Ap #614-689 Vehicula Street", "Married": false }
    ];

    var countries = [
        { Name: "", Id: 0 },
        { Name: "United States", Id: 1 },
        { Name: "Canada", Id: 2 },
        { Name: "United Kingdom", Id: 3 }
    ];

    function newtable() {

        $("#jsGrid").jsGrid({
            width: "100%",
            height: "400px",

            inserting: true,
            editing: true,
            sorting: true,
            paging: true,

            data: nameslable,

            fields: [
                {name: "安全门标识", type: "text", width: 150, validate: "required"},
                {name: "安全门", type: "text", width: 50},
                {name: "状态", type: "text", width: 200},
                {name: "日期", type: "text", width: 200},
            ]
        });

    }
    function update()
    {
        AutoUpdate();
    }
    $(function () {
        AutoUpdate();
        alert(nameslable)
        $("#jsGrid").jsGrid({
            width: "100%",
            height: "400px",
            data: nameslable,
            fields: [
                {name: "安全门标识", type: "text", width: 150, },
                {name: "安全门", type: "text", width: 50},
                {name: "状态", type: "text", width: 200},
                {name: "日期", type: "text", width: 200},
            ]
        });

        setInterval(update,3600*1000);

        $("#opensafe").click(function () {
            alert("点击")
            updatetable();
            return false;
        })
    })

</script>

</html>
