package com.hikrobotcon;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

@Slf4j
@SpringBootApplication
public class HikRobotConApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(HikRobotConApplication.class, args);


        Socket s = new Socket();
        InetSocketAddress inetSocketAddress = new InetSocketAddress("169.254.110.47", 2002);
        s.connect(inetSocketAddress);
        InputStream input = s.getInputStream();
        OutputStream output = s.getOutputStream();

       // BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(output));
        //bw.write("客户端给服务端发消息测试\n");  //向服务器端发送一条消息
        //bw.flush();

        BufferedReader br = new BufferedReader(new InputStreamReader(input));  //读取服务器返回的消息
        String mess = br.readLine();
        log.info("服务器：" + mess);

     /*   ServerSocket server = null;
        try {
            server = new ServerSocket(10002);
        } catch (IOException e) {
            e.printStackTrace();
        }
            Socket client = server.accept();
            //等待客户端的连接，如果没有获取连接  ,在此步一直等待
            new Thread(new ServerThread(client)).start(); //为每个客户端连接开启一个线程*/

    }

    static class ServerThread extends Thread {

        private Socket client;

        public ServerThread(Socket client) {
            this.client = client;
        }


        @SneakyThrows
        @Override
        public void run() {
            while (true)
            {
                InetAddress inetAddress = client.getInetAddress();
                log.info("客户端:" +inetAddress.getHostAddress() + "已连接到服务器");
                log.info("获得流");
                InputStream inputStream = client.getInputStream();
                System.out.println(inputStream);
                byte[] bytes = inputStream.readAllBytes();
                System.out.println(bytes);
                String s = new String(bytes);
                System.out.println("获得数据"+s);
                //BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
                //读取客户端发送来的消息
                //String mess = br.readLine();

                //BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
                //bw.write(mess + "\n");
                //bw.flush();
            }

        }

    }
}
