package countdownlatch;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchTest {

    private CountDownLatch countDownLatch;


    @Test
    public void test()
    {
        try {
            countDownLatch=new CountDownLatch(1);

            System.out.println("阻塞开始");
            countDownLatch.await();
            System.out.println("阻塞结束");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2()
    {
        countDownLatch.countDown();
    }
}
