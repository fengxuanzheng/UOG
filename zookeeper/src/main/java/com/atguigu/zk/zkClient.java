package com.atguigu.zk;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class zkClient {

    // 注意：逗号左右不能有空格
    //private String connectString = "192.168.0.102:2181,192.168.0.102:2181,192.168.0.102:2181";
    private String connectString = "43.139.36.8:2181";
    private int sessionTimeout = 40000;
    private ZooKeeper zkClient;
    private CountDownLatch countDownLatch=new CountDownLatch(1);

    @Before

    public void init() throws IOException {

        zkClient = new ZooKeeper(connectString, sessionTimeout, new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {

               System.out.println("-------------------------------开始监听状态");
                System.out.println("方法内:"+Thread.currentThread().getName());
                long l = System.currentTimeMillis();
                System.out.println(l);
                List<String> children = null;
                try {
                   children = zkClient.getChildren("/", true);
                    System.out.println(Thread.currentThread().getName());
                    for (String child : children) {
                        System.out.println(child);
                    }

                    System.out.println("-------------------------------结束监听");

                    long l1 = System.currentTimeMillis();
                    System.out.println(l1);
                    System.out.println("方法内:"+Thread.currentThread().getName()+(l1-l));
                } catch (KeeperException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Test
    public void create() throws KeeperException, InterruptedException {
        String nodeCreated = zkClient.create("/bbcd", "ss.avi".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
    }

    @Test
    public void getChildren() throws KeeperException, InterruptedException {
        System.out.println("****************************************");
        System.out.println("监听完成方法"+Thread.currentThread().getName());
        List<String> children = zkClient.getChildren("/", true);
        //countDownLatch.await();
        System.out.println("__________________________________监听完成方法");
        System.out.println("监听完成方法"+Thread.currentThread().getName());
        long l = System.currentTimeMillis();
        System.out.println(l);
        for (String child : children) {
            System.out.println("非监听:"+child);
        }

        long l1 = System.currentTimeMillis();

        System.out.println(l1);
        System.out.println("监听完成方法"+Thread.currentThread().getName()+(l1-l));

        // 延时
        Thread.sleep(Long.MAX_VALUE);
    }

    @Test
    public void exist() throws KeeperException, InterruptedException {

        Stat stat = zkClient.exists("/atguigu", false);

        System.out.println(stat==null? "not exist " : "exist");
    }
}
