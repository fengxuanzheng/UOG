package com.redisandmqwithspringboot;

import com.redisandmqwithspringboot.uitll.Persons;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SpringBootTest
class RedisAndMQwithSpringBootApplicationTests {

    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    void contextLoads() {
    }

    @Test
    public void test()
    {
        StringBuffer stringBuffer = new StringBuffer();
        Random random = new Random();
        for (int i=0;i<6;i++)
        {
            int i1 = random.nextInt(10);
            stringBuffer.append(i1);
        }

        System.out.println(stringBuffer);
    }

    @Test
    public void test2()
    {
        ListOperations listOperations = redisTemplate.opsForList();
        ArrayList<String> strings = new ArrayList<>();
        strings.add("a");
        strings.add("bb");
        //listOperations.leftPush("k1",strings);
        List k1 = listOperations.range("k1", 0, -1);
        System.out.println(k1.size());
        k1.forEach(item->{
            System.out.println("元素:"+item);
        });
        System.out.println(k1.get(0));
        Object o = k1.get(0);
        System.out.println(o instanceof List);
        ((List)o).forEach(item->{
            System.out.println("元素1:"+item);
        });
        System.out.println(k1);
    }

    @Test
    public void test3()
    {
        Persons persons = Persons.getPersons();
        System.out.println(persons);
        System.out.println(persons.hashCode());
        System.out.println(persons.persons.hashCode());
    }

}
