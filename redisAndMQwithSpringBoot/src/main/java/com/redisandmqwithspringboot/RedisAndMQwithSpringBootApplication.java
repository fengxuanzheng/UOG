package com.redisandmqwithspringboot;

import com.redisandmqwithspringboot.redis.RedisMain;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RedisAndMQwithSpringBootApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(RedisAndMQwithSpringBootApplication.class, args);
        RedisMain redisMain = run.getBean("redisMain", RedisMain.class);
        System.out.println(redisMain);

    }

}
