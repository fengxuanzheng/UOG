package com.redisandmqwithspringboot.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisMain {

    @Autowired
    private RedisTemplate redisTemplate;

    private  RedisConnection redisConnection;

    public RedisMain()
    {
       /* ListOperations listOperations = redisTemplate.opsForList();

        //System.out.println(redisConnection);*/
    }

    public RedisConnection getRedisConnection() {
        return redisConnection;
    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public String toString() {
        return "RedisMain{" +
                "redisTemplate=" + redisTemplate +
                ", redisConnection=" + redisConnection +
                '}';
    }
}
