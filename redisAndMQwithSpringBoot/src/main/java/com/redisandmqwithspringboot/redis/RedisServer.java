package com.redisandmqwithspringboot.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisServer {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void redisconfig()
    {
        redisTemplate.opsForHash().put("ads","ssd",500);
        redisTemplate.opsForValue().set("k485","789");
        Object k485 = redisTemplate.opsForValue().get("k485");
        System.out.println(k485);
        System.out.println(redisTemplate.opsForHash().get("ads", "ssd"));
    }
}
