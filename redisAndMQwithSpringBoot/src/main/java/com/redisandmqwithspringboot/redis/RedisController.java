package com.redisandmqwithspringboot.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

@RestController
public class RedisController {

    @Autowired
    private RedisServer redisServer;

    @GetMapping("/hello")
    //@Stateless("ss")
    public String redishell()
    {
        redisServer.redisconfig();
        ConcurrentHashMap<String, Object> stringObjectConcurrentHashMap = new ConcurrentHashMap<>();
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectConcurrentHashMap.keySet();
        
        return "success";
    }
}
