package com.redisandmqwithspringboot.uitll;

public class Persons {

    public static Persons persons;
    private String name="acg";

    private Persons(){}

    public static Persons getPersons()
    {
       persons =new Persons();

       return persons;
    }

    public static void setPersons(Persons persons) {
        Persons.persons = persons;
    }

    @Override
    public String toString() {
        return "Persons{" +
                "name='" + name + '\'' +
                '}';
    }
}
