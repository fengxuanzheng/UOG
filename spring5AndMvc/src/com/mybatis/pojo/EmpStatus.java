package com.mybatis.pojo;

public enum EmpStatus {
    LOGIN(100,"用户登入"),LOGOUT(200,"用户登出"),REMOVE(300,"用户移除");
    private Integer integer;
    private String msgname;

    EmpStatus(Integer integer,String name) {
        this.integer=integer;
        this.msgname=name;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public String getMsgname() {
        return msgname;
    }

    public void setMsgname(String msgname) {
        this.msgname = msgname;
    }

    public static EmpStatus getempformsgname(String msgname)
    {
        switch (msgname)
        {
            case "用户登入":
              return  LOGIN;
            case "用户登出":
                return LOGOUT;
            default:
                return REMOVE;

        }
    }
    //注册枚举处理器出来全局外还可以在取值时指定某个参数使用某个处理器
    //比如:在SQL插入修改里 #{empStatus,typeHandler=xxx}
    //在查询中,可以自定义resultMap封装结果集,在封装普通列时候<result column="" property="" typeHandler=""/>
}
