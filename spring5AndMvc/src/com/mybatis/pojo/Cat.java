package com.mybatis.pojo;


public class Cat {

  private Integer id;
  private String cName;
  private long cAge;
  private long cGender;

  public Cat() {
  }

  public Cat(Integer id, String cName, long cAge, long cGender) {
    this.id = id;
    this.cName = cName;
    this.cAge = cAge;
    this.cGender = cGender;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getCName() {
    return cName;
  }

  public void setCName(String cName) {
    this.cName = cName;
  }


  public long getCAge() {
    return cAge;
  }

  public void setCAge(long cAge) {
    this.cAge = cAge;
  }


  public long getCGender() {
    return cGender;
  }

  public void setCGender(long cGender) {
    this.cGender = cGender;
  }

  @Override
  public String toString() {
    return "Car{" +
            "id=" + id +
            ", cName='" + cName + '\'' +
            ", cAge=" + cAge +
            ", cGender=" + cGender +
            '}';
  }
}
