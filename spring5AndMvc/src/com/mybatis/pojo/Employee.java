package com.mybatis.pojo;


public class Employee {

  private Integer id;
  private String empname;
  private long gender;
  private String email;
  private EmpStatus empStatus;

  public Employee(Integer id, String empname, long gender, String email) {
    this.id = id;
    this.empname = empname;
    this.gender = gender;
    this.email = email;
  }

  public Employee() {
  }


  public long getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getEmpname() {
    return empname;
  }

  public void setEmpname(String empname) {
    this.empname = empname;
  }


  public long getGender() {
    return gender;
  }

  public void setGender(long gender) {
    this.gender = gender;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public EmpStatus getEmpStatus() {
    return empStatus;
  }

  public void setEmpStatus(EmpStatus empStatus) {
    this.empStatus = empStatus;
  }

  @Override
  public String toString() {
    return "Employee{" +
            "id=" + id +
            ", empname='" + empname + '\'' +
            ", gender=" + gender +
            ", email='" + email + '\'' +
            ", empStatus=" + empStatus +
            '}';
  }


}
