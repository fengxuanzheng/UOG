package com.mybatis.interception;

import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.util.Properties;
@Intercepts({
        @Signature(type = StatementHandler.class,method = "parameterize",args = {java.sql.Statement.class})
})//插件签名。告诉mybatis这个插件拦截那个对象的那个方法
public class MyFirstPlugin implements Interceptor {
    @Override
    //此方法是拦截目标对象方法执行
    public Object intercept(Invocation invocation) throws Throwable {
        //执行目标方法
        System.out.println("插件代理前执行:"+invocation);
        //拦截到对象
        Object target = invocation.getTarget();
        //获取顺序StatementHandler >ParameterHandler >parameterObject的值
        //那个目标对象元数据
        MetaObject metaObject = SystemMetaObject.forObject(target);
        //根据源码ParameterHandler parameterHandler对象是BaseStatementHandler的属性，PreparedStatementHandler继承了BaseStatementHandler
        metaObject.getValue("parameterHandler.parameterObject");
        //偷梁换柱，修改传入参数值
        metaObject.setValue("parameterHandler.parameterObject",2);
        Object proceed = invocation.proceed();

        return proceed;
    }

    @Override
    //包装目标对象，为目标对象创建一个代理对象
    public Object plugin(Object target) {
        //使用mybatis内置方法快速创建一个代理对象
        System.out.println("为" + target + "创建代理对象");
        Object wrap = Plugin.wrap(target, this);
        return wrap;
    }

    @Override
    //将插件注册时的properties属性设置进来
    public void setProperties(Properties properties) {
        System.out.println("插件内部参数" + properties);
    }
}
