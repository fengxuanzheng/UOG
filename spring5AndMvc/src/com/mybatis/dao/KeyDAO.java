package com.mybatis.dao;

import com.mybatis.pojo.Key;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface KeyDAO {

    public Key getKeyByid(Integer integer);
    public Key getKeyByidForassociation(Integer integer);
    public Key getKeyByidSimple(Integer integer);
    public List<Key> getKeyByLockid(@Param("integer") Integer integer,@Param("name") String name);

}
