package com.mybatis.dao;

import com.mybatis.pojo.Employee;
import org.apache.ibatis.annotations.Select;

public interface EmployeeDAOAnnotation {
    @Select(" select * from t_employee where id = #{id}")
    public Employee getEmployee(Integer integer);
}
