package com.mybatis.dao;

import com.mybatis.pojo.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TeacherDAO {
    public Teacher getTeacher(Integer id);
    public int allupdateTeacher(@Param("teacher") Teacher teacher);
    public List<Teacher> getTeacherCondition(Teacher teacher);
    public List<Teacher> getTeacherConditionforTrim(Teacher teacher);
    public List<Teacher> getTeacherConditionByidIn(@Param("teachers") List<Integer>teachers);
    public List<Teacher> getTeacherConditionforChooose(Teacher teacher);
    public  int updateTeacher(Teacher teacher);
    public  int updateTeacheraddParam(@Param("teacher") Teacher teacher);
    public int addTeahcer(@Param("teacherlist") List<Teacher> teacherlist);
    public int addTeahcers(@Param("teacherlist") List<Teacher> teacherlist);
}
