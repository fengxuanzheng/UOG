package com.mybatis.dao;


import com.mybatis.pojo.Cat;

public interface CatDAO {
    public Cat getcat(Integer integer);
}
