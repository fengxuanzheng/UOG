package com.mybatis.dao;

import com.mybatis.pojo.Lock;

public interface LockDAO {

    public Lock getLockByid(Integer id);
    public Lock getLockByidSimple(Integer id);
    public Lock getLockByStep(Integer id);
}
