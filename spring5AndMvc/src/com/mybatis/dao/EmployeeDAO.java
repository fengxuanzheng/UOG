package com.mybatis.dao;

import com.mybatis.pojo.Employee;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EmployeeDAO {

    public Employee getEmployee(Integer integer);
    public int insert(Employee employee);
    public int insertforNoAuto(Employee employee);
    public Employee getEmployeeforname(@Param("id") Integer integer, @Param("name") String name);
    public List<Employee> getemployeeList();
    public Map<String,Object> getemployeeformap(Integer integer);
    @SuppressWarnings("MybatisMapperMethodInspection")
    @MapKey("id")
    public Map<String,Employee> getemployeeformaps();
}
