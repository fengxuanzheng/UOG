package com.mybatis.typeHandler;

import com.mybatis.pojo.EmpStatus;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//两种方法一种是实现TypeHandler接口,或者是继承BaseTypeHandler
public class MyEnumEmpStatusTypeHandler implements TypeHandler<EmpStatus> {
    @Override
    //定义当前数据如何保存到数据库中
    public void setParameter(PreparedStatement ps, int i, EmpStatus parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i,parameter.getMsgname());
    }

    @Override
    public EmpStatus getResult(ResultSet rs, String columnName) throws SQLException {
        String string = rs.getString(columnName);
        return EmpStatus.getempformsgname(string);
    }

    @Override
    public EmpStatus getResult(ResultSet rs, int columnIndex) throws SQLException {
        String string = rs.getString(columnIndex);
        return EmpStatus.getempformsgname(string);
    }

    @Override
    public EmpStatus getResult(CallableStatement cs, int columnIndex) throws SQLException {
        String string = cs.getString(columnIndex);
        return EmpStatus.getempformsgname(string);
    }


}
