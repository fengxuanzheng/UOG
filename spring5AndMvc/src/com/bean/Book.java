package com.bean;

import org.springframework.beans.factory.InitializingBean;

public class Book implements InitializingBean
{
    private String bookName;
    private String author;

    public Book() {
    }

    public Book(String bookName, String author) {
        this.bookName = bookName;
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        //System.out.println("调用set方法");
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void myinit()
    {
        System.out.println("book的初始化");
    }
    public void destroy()
    {
        System.out.println("book的销毁方法");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("在FacoryBean设置完之后");
    }
}
