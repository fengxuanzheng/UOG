package com.bean;

public class Airplane {
    private String fdj;
    private String  yc;
    private Integer personNum;
    private String gJZnAME;
    private String fjsName;

    public Airplane(String fdj, String yc, Integer personNum, String gJZnAME, String fjsName) {
        this.fdj = fdj;
        this.yc = yc;
        this.personNum = personNum;
        this.gJZnAME = gJZnAME;
        this.fjsName = fjsName;
    }

    public Airplane() {
    }

    public String getFdj() {
        return fdj;
    }

    public void setFdj(String fdj) {
        this.fdj = fdj;
    }

    public String getYc() {
        return yc;
    }

    public void setYc(String yc) {
        this.yc = yc;
    }

    public Integer getPersonNum() {
        return personNum;
    }

    public void setPersonNum(Integer personNum) {
        this.personNum = personNum;
    }

    public String getgJZnAME() {
        return gJZnAME;
    }

    public void setgJZnAME(String gJZnAME) {
        this.gJZnAME = gJZnAME;
    }

    public String getFjsName() {
        return fjsName;
    }

    public void setFjsName(String fjsName) {
        this.fjsName = fjsName;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "fdj='" + fdj + '\'' +
                ", yc='" + yc + '\'' +
                ", personNum=" + personNum +
                ", gJZnAME='" + gJZnAME + '\'' +
                ", fjsName='" + fjsName + '\'' +
                '}';
    }
}
