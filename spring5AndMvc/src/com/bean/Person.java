package com.bean;

import java.util.List;
import java.util.Map;
import java.util.Properties;


public class Person {
    private String lastname;
    private Integer age;
    private String gender;
    private String email;

    private Car car;
    private List<Book> books;
    private Properties properties;
    private Map<String,Object> objectMap;

    public Person(String lastname, Integer age, String gender, String email) {
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
        this.email = email;
    }

    public Person() {
    }

    public Person(String lastname, Integer age, String gender, String email, Car car, List<Book> books) {
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
        this.email = email;
        this.car = car;
        this.books = books;
    }


    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public Map<String, Object> getObjectMap() {
        return objectMap;
    }

    public void setObjectMap(Map<String, Object> objectMap) {
        this.objectMap = objectMap;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }


    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "lastname='" + lastname + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", car=" + car +
                ", books=" + books +
                ", properties=" + properties +
                ", objectMap=" + objectMap +
                '}';
    }
}
