package com.bean.JDBC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookService {

    @Autowired
    private BookDAO bookDAO;



   // 注意只要有事务逻辑(如BookService),容器中保存时有cglb创建出来的业务逻辑代理对象,不是BookService本身
   // timeout 表示超时,当事务超出指定时间后终止并回滚,单位为秒
    //readonly 设置事务为只读,可以加快查询速度,不需要管事务那一堆操作
    //对于异常回滚:对于运行时异常都回滚,但是对于编译时异常不回滚
    //可以使用rollbackFor 让原本不回滚异常(如编译时异常)回滚
    @Transactional(timeout = 3,rollbackFor ={RuntimeException.class} )
    public void checkout(String username, String isbn) throws InterruptedException {
        //减库存
        bookDAO.updatestock(isbn);
        //Thread.sleep(4000);
        int getprice = bookDAO.getprice(isbn);
        //int a=10/0;
        bookDAO.updateBalance(username,getprice);
     }

     @Transactional(readOnly = true,isolation = Isolation.READ_UNCOMMITTED )
     public int getprice(String isbn)
     {
         return bookDAO.getprice(isbn);
     }

    @Transactional(rollbackFor = {RuntimeException.class})
    public void updatePrice(String isbn,int price)
     {
         bookDAO.updatePrice(isbn,price);
     }
}
