package com.bean.JDBC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BookDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void updateBalance(String username,int price)
    {
        String sql="update account set balance=balance-? where username=?";
        jdbcTemplate.update(sql,price,username);
    }
    public int getprice(String isbn)
    {
        String sql="select price from book where isbn=?";
        Integer integer = jdbcTemplate.queryForObject(sql, Integer.class, isbn);
        return integer;
    }

    public void updatestock(String isbn)
    {
        String sql="update book_stock set stock=stock-1 where isbn=?";
        jdbcTemplate.update(sql,isbn);
    }
    public void updatePrice(String isbn,int price)
    {
        String sql="update book set price=? where isbn=?";
        jdbcTemplate.update(sql,price,isbn);
    }
}
