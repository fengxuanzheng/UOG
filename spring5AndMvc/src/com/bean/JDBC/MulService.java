package com.bean.JDBC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MulService {
    @Autowired
    private BookService bookService;
    @Transactional(rollbackFor = {RuntimeException.class})
    public void multx() throws InterruptedException {

        //传播行为来设置这个事务方法是不是和之前大事务共享一个事务(使用同一条连接)
        //REQUIRED:事务属性都是继承于所在大事务,而REQUIRED_NEW可以调整
        //特别注意:本类方法嵌套调用就是一个事务,因为本类嵌套调用方法是本类自己调用,不是通过代理对象调用
        //如:BookService.multx{checkout();updatePrice()}里面两个嵌套调用方法相当于是BookService(this)自己调用,而multx是BookService代理对象
        bookService.checkout("Tom","ISBN-001");
        bookService.updatePrice("ISBN-002",998);
    }
}
