package com.bean.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;
@Component
@Aspect
@Order(10)//改变多切面顺序
public class VaildateApsect {

    @Before("com.bean.aop.LogUtils2.resultpoin()")
    public static void logstart(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        String name = joinPoint.getSignature().getName();
        System.out.println("第二个_________________________"+"方法开始执行:"+"方法参数:"+ Arrays.asList(args)+"方法名:"+name);
    }
    @AfterReturning(value = "execution(public int com.bean.aop.CalculatorIML2.*(..))",returning = "obj")
    public static void logReturn(JoinPoint joinPoint,Object obj) {
        System.out.println("第二个_________________________"+"方法结束"+"返回值:"+obj);
    }

    @AfterThrowing(value = "execution(public int com.bean.aop.CalculatorIML2.*(..))",throwing = "exception")
    public static void logException(Exception exception) {
        System.out.println("第二个_________________________"+"方法异常");
    }
    @After("execution(public int com.bean.aop.CalculatorIML2.*(..))")
    public static void logFinalier() {
        System.out.println("第二个_________________________"+"最终执行");
    }

}
