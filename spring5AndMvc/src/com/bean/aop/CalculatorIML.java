package com.bean.aop;

import org.springframework.stereotype.Service;
@Service
public class CalculatorIML implements Calculator{
    private String caname;
    @Override
    public int add(int i, int j) {
        int result=i+ j;
        return result;
    }

    @Override
    public int sub(int i, int j) {
        int result=i-j;
        return result;
    }

    @Override
    public int mul(int i, int j) {
        int result=i*j;
        return result;
    }
     @Override
    public int div(int i, int j) {
        int result=i/j;
        return result;
    }

    @Override
    public String toString() {
        return "CalculatorIML{" +
                "caname='" + caname + '\'' +
                '}';
    }
}
