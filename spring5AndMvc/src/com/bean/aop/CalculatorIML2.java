package com.bean.aop;

import org.springframework.stereotype.Service;

@Service
public class CalculatorIML2 {

    public int add(int i, int j) {
        System.out.println("原始方法执行");
        int result=i+ j;
        return result;
    }


    public int sub(int i, int j) {
        int result=i-j;
        return result;
    }


    public int mul(int i, int j) {
        int result=i*j;
        return result;
    }


    public int div(int i, int j) {
        int result=i/j;
        return result;
    }
}
