package com.bean.aop;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
/*
try{
    @Before
    method.invoke(obj,arg)
            @AfterReturning
        }cathche(e){
    @AfterThrowing
                }finally{
    @After()
                        }
*/


@Component
@Aspect
public class LogUtils {
    @Before("execution(public int com.bean.aop.CalculatorIML.add(int ,int ))")
    public static void logstart( ) {
        System.out.println("方法开始执行");
    }
   @AfterReturning("execution(public int com.bean.aop.CalculatorIML.add(int ,int ))")
   public static void logReturn() {
        System.out.println("方法结束");
    }

    @AfterThrowing("execution(public int com.bean.aop.CalculatorIML.add(int ,int ))")
    public static void logException() {
        System.out.println("方法异常");
    }
    @After("execution(public int com.bean.aop.CalculatorIML.add(int ,int ))")
    public static void logFinalier() {
        System.out.println("最终执行");
    }
}
