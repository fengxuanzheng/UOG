package com.bean.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class CalculatorProxy {

    public static  Calculator getProxy(Calculator calculator)
    {
        ClassLoader loader=calculator.getClass().getClassLoader();
        Class<?>[] interfaces=calculator.getClass().getInterfaces();
        InvocationHandler invocationHandler=new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("动态代理执行目标方法前"+method+"   "+args);
                Object invoke = method.invoke(calculator, args);
                System.out.println("动态代理执行目标方法后");
                return invoke;
            }
        };
        Object o = Proxy.newProxyInstance(loader, interfaces, invocationHandler);
        return (Calculator) o;
    }
}
