package com.bean.aop;

public class CalculatorTest extends CalculatorIML{
    private String myname;

    public CalculatorTest(String myname) {
        this.myname = myname;
    }

    public CalculatorTest() {
    }

    public String getMyname() {
        return myname;
    }

    public void setMyname(String myname) {
        this.myname = myname;
    }

    @Override
    public String toString() {
        return "CalculatorTest{" +
                "myname='" + myname + '\'' +
                '}';
    }
}
