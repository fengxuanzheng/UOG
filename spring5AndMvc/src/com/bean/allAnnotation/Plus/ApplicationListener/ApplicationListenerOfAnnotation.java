package com.bean.allAnnotation.Plus.ApplicationListener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class ApplicationListenerOfAnnotation {


    @EventListener
    public void listen(ApplicationEvent applicationEvent)
    {
        System.out.println("注解@EventListener监听:"+applicationEvent);
    }
}
