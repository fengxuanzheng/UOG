package com.bean.allAnnotation.Plus;

import com.bean.allAnnotation.utill.MyFactoyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/*
BeanPostProcessor:bean后置处理器,bean创建对象初始化前后进行拦截工作
BeanFactoryPostProcessor: beanFactory的后置处理器;在BeanFactory标准初始化之后调用(所有bean定义已经被保存加载到beanFactory,但是bean实例还有没有创建)
BeanDefinitionRegistryPostProcessor:bean定义信息将要被加载bean实例还未创建
 */
@ComponentScan("com.bean.allAnnotation.Plus")
@Configuration
public class PlusConfig {
    @Bean(initMethod = "init")
    public MyFactoyBean myFactoyBean()
    {
        return new MyFactoyBean();
    }


}
