package com.bean.allAnnotation.Plus.scopeBean;


import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

import java.util.HashMap;
import java.util.Map;

public class MyScope implements Scope {
    private String username;
    private String sex;
    private String addr;
    private ThreadLocal threadLocal=new ThreadLocal(){
        @Override
        protected Object initialValue() {
            return new HashMap<>();
        }
    };

    @Override
    public String toString() {
        return "MyScope{" +
                "username='" + username + '\'' +
                ", sex='" + sex + '\'' +
                ", addr='" + addr + '\'' +","+"hashCode="+hashCode()+
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }


    @Override
    public Object get(String name, ObjectFactory<?> objectFactory) {
        System.out.println("名字是:"+name);
        Object myScope = ((Map)threadLocal.get()).get(name);
        if (myScope==null)
        {
           myScope= objectFactory.getObject();
           ((Map) threadLocal.get()).put(name,myScope);
        }
        System.out.println(myScope);
        return myScope;
    }

    @Override
    public Object remove(String name) {
        System.out.println("删除:"+name);
        return ((Map)threadLocal.get()).get(name);
    }

    @Override
    public void registerDestructionCallback(String name, Runnable callback) {
        System.out.println("执行销毁回调名字:"+name);
    }

    @Override
    public Object resolveContextualObject(String key) {
        System.out.println("key:"+key);
        return null;
    }

    @Override
    public String getConversationId() {
        return null;
    }
}
