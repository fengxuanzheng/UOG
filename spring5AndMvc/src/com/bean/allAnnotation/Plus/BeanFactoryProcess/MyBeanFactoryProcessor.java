package com.bean.allAnnotation.Plus.BeanFactoryProcess;

import com.bean.allAnnotation.Plus.scopeBean.MyScope;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class MyBeanFactoryProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println(beanFactory.getBeanPostProcessorCount());
        String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
        System.out.println("________________________________________________");
        beanFactory.registerScope("thread", new MyScope());//自定义作用域
        Arrays.stream(beanDefinitionNames).forEach(s -> System.out.println("BeanFactoryPostProcessor__________"+s));
    }
}
