package com.bean.allAnnotation.Plus.BeanFactoryProcess;

import com.bean.allAnnotation.pojo.Book;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.stereotype.Component;

import java.util.Arrays;
@Component
public class MyBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
    @Override
    //BeanDefinitionRegistry:是bean定义信息的保存中心,以后BeanFactory就算按照BeanDefinitionRegistry里面保存信息创建每一个bean实例
    //优先于BeanFactoryPostProcessor执行,利用 BeanDefinitionRegistryPostProcessor给容器在额外添加一些组件
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        //RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(Book.class);
        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.rootBeanDefinition(Book.class).getBeanDefinition();
        registry.registerBeanDefinition("registryBook",beanDefinition);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
        System.out.println("************************************");
        Arrays.stream(beanDefinitionNames).forEach(s -> System.out.println("BeanDefinitionRegistry"+s));


    }
}
