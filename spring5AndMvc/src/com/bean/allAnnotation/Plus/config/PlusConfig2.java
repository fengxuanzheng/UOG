package com.bean.allAnnotation.Plus.config;

import com.bean.allAnnotation.Plus.scopeBean.MyScope;
import com.bean.allAnnotation.pojo.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class PlusConfig2 {

    @Bean
    public Person person_plusconfig2()
    {
        return new Person();
    }

    @Bean
    @Scope("thread")
    public MyScope myScope()
    {
        MyScope myScope = new MyScope();
        myScope.setUsername("地表最强");
        return myScope;
    }
}
