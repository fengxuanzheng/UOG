package com.bean.allAnnotation.config;


import com.alibaba.druid.pool.DruidDataSource;
import com.bean.allAnnotation.aop.CalculatorIML;
import com.bean.allAnnotation.aop.LogUtils2;
import com.bean.allAnnotation.condition.MyCondition;
import com.bean.allAnnotation.filter.MyTypeFilter;
import com.bean.allAnnotation.pojo.Apples;
import com.bean.allAnnotation.pojo.Cat;
import com.bean.allAnnotation.pojo.Person;
import com.bean.allAnnotation.pojo.Sister;
import com.bean.allAnnotation.server.TestServer;
import com.bean.allAnnotation.utill.MyBeanPostProcessor;
import com.bean.allAnnotation.utill.MyFactoyBean;
import com.bean.allAnnotation.utill.MyImportBeanDefinitionRegistrar;
import com.bean.allAnnotation.utill.MyImportSelector;
import org.springframework.stereotype.Repository ;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StringValueResolver;

import javax.sql.DataSource;

@Configuration
@PropertySource({"classpath:/jdbc.properties","classpath:/jdbcforsqlserver.properties"})
@ComponentScan(value = {"com.bean.allAnnotation"},includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = Controller.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,classes = TestServer.class),
        @ComponentScan.Filter(type = FilterType.CUSTOM,classes = MyTypeFilter.class),
        @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = Service.class),
        @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = Repository.class)
},useDefaultFilters = false)
@Import({MyImportSelector.class, MyImportBeanDefinitionRegistrar.class})

@EnableAspectJAutoProxy//开启全注解AOP模式
@EnableTransactionManagement //开启事务注解模式
public class MyConfig implements EmbeddedValueResolverAware{
    private StringValueResolver stringValueResolver;
    /*
    关于自动装配:
    @Autowired默认到容器找对于类型组件,如果找到多个相同类型组件就在按照属性名称作为id去容器中查找
    如果要手动指定要装配组件的id可以使用@Qualifier注解手动指定要装配id不要使用属性名称
    @Primary注解可以让spring自动装配时候首选作为Bean(此注解标注在组件上后,如果有相同类型组件也会自动装配标注了此注解的组件,前提是不能指定@Qualifier,指定后@Primary无效)

    @Resource和@Autowired一样具备自动装配功能属于java规范,此注解默认按照组件名称进行装配,不能支持@Primary以及@Autowired(reqiured=false)功能
    @Inject属于JSR330规范需要导入javax.inject包,和@Autowired功能一样,但是没有required=false功能


    @Autowired还可以标注在 方法 构造器 参数位置
    标注在方法上时候比如set方法spring会给标注方法的参数从IOC容器中获取后赋值给参数,之后调用此方法完成自动注入
    标注到构造器也一样:1.默认加载IOC容器中组件,容器启动后会调用无参构造器创建对象在进行初始化等赋值操作
    2.标注在构造器上和方法一样,但是如果只有一个有参构造器@Autowired可以不要,参数位置组件还是可以自动从容器中获取
    标注在参数和标注在方法上区别不大

    @Bean标注方法创建对象时候,方法参数值也是从容器中获取

    以后如果要给自定义组件添加spring自己原生底层组件就实现xxxAware接口
     */

    @Bean
    //@Scope("singleton")//默认单实例,对于单实例ioc容器启动后会调用Bean方法创建对象放在ioc容器中
    //对于多实例,ioc容器启动并不会直接去调用方法创建对象放在ioc容器中,而是每次获取时候才调用方法创建对象,获取多少此就调用几次方法创建对象
    @Lazy //懒加载值针对单实例,懒加载下容器启动不会直接创建对象,是在第一次获取对象时候才创建此单实例对象加入到ioc容器中
    public Person person()
    {
        Person person = new Person();
        person.setName("欧尼酱");
        return person;
    }

    @Bean
    @Conditional(MyCondition.class)
    public Sister sister()
    {
        Sister sister = new Sister();
        sister.setName("琉璃");
        sister.setName("欧尼酱");
        return sister;
    }

    @Bean
    //特别注释:根据BeanFactory接口的String FACTORY_BEAN_PREFIX = "&";
    //在getBean()时候工厂的id加上&就可以获得工厂本身而不是通过工厂创建出来对象
    public MyFactoyBean myFactoyBean()
    {
        return new MyFactoyBean();
    }

    @Bean(initMethod = "init",destroyMethod ="destory" )
    //@Scope("prototype")
    //多实例bean:容器不会完全管理多实例bean,容器创建完成后交给你,容器不会调用销毁方法
    //spring底层对BeanPostProcessor使用:bean赋值 注入其他组件 @AutoWired 生命周期注解 @Async
    public Cat myCat()
    {
        Cat cat = new Cat();
        cat.setPrice(648);
        return cat;
    }

    @Bean
    public MyBeanPostProcessor myBeanPostProcessor()
    {
        return new MyBeanPostProcessor();
    }

    @Bean
    public Apples apples()
    {
        return new Apples();
    }

   @Profile("test")
   @Bean
   //加了环境标识的bean只有这个环境生效才能加入到容器中,默认是default
   //写在配置类三,只有是指定环境时候整个配置类里面所有配置才开始生效
   //没有标注环境标识的bean任何环境下都会加载
    public DataSource dataSourceForMysql(@Value("${mysql.username}") String username, @Value("${mysql.password}") String password)
    {
        String url = stringValueResolver.resolveStringValue("${mysql.url}");
        String driver = stringValueResolver.resolveStringValue("${mysql.driver}");
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUrl(url);
        druidDataSource.setPassword(password);
        druidDataSource.setUsername(username);
        druidDataSource.setDriverClassName(driver);
        return druidDataSource;
    }
    @Profile("prof")
    @Bean
    public DataSource dataSourceForSqlserver(@Value("${sqlserver.username}") String username, @Value("${sqlserver.password}") String password)
    {
        String url = stringValueResolver.resolveStringValue("${sqlserver.url}");
        String driver = stringValueResolver.resolveStringValue("${sqlserver.driver}");
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUrl(url);
        druidDataSource.setPassword(password);
        druidDataSource.setUsername(username);
        druidDataSource.setDriverClassName(driver);
        return druidDataSource;
    }

    @Override
    public void setEmbeddedValueResolver(StringValueResolver resolver) {
        stringValueResolver=resolver;
    }


    //********************************************************************

    @Bean
    public CalculatorIML calculatorIML()
    {
        return new CalculatorIML();
    }

    @Bean
    public LogUtils2 logUtils2()
    {
        return new LogUtils2();
    }

    //***************************************************
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource)
    {
       return new JdbcTemplate(dataSource);
    }
    @Bean //配置事务管理器
    public PlatformTransactionManager platformTransactionManager(DataSource dataSource)
    {
        return new DataSourceTransactionManager(dataSource);
    }
}
