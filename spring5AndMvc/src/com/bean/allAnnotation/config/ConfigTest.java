package com.bean.allAnnotation.config;

import com.bean.allAnnotation.pojo.MyLife;
import com.bean.allAnnotation.pojo.MySmarkLife;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ComponentScan(value = "com.bean.allAnnotation")
public class ConfigTest {

    @Bean
    public MyLife myLife()
    {
        return new MyLife();
    }

    @Bean
    public MySmarkLife mySmarkLife()
    {
        return new MySmarkLife();
    }
}
