package com.bean.allAnnotation.utill;

import com.bean.allAnnotation.pojo.Dogs;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    //AnnotationMetadata importingClassMetadata 当前类元注解信息
    //BeanDefinitionRegistry registry ioc里面注册类可以通过它给ioc注入组件
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        boolean dogs = registry.containsBeanDefinition("dogs");
        if (!dogs)
        {
            RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(Dogs.class);

            registry.registerBeanDefinition("myDogs",rootBeanDefinition);
        }
    }
}
