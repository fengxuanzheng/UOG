package com.bean.allAnnotation.utill;

import com.bean.allAnnotation.pojo.Sister;
import org.springframework.beans.factory.FactoryBean;

public class MyFactoyBean implements FactoryBean<Sister> {
    @Override
    public Sister getObject() throws Exception {
        System.out.println("工厂启动___________________________________");
        return new Sister();
    }

    @Override
    public Class<?> getObjectType() {
        return Sister.class;
    }

    @Override
    public boolean isSingleton() {
        return FactoryBean.super.isSingleton();
    }

    public void init()
    {
        System.out.println("MyFactoyBean初始化方法");
    }
}
