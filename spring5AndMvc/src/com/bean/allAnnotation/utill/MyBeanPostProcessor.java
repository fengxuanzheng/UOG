package com.bean.allAnnotation.utill;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("初始化处理器之前:"+bean+"_____"+beanName);
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException
        {
            System.out.println("初始化处理器之后:"+bean+"_____"+beanName);
            return bean;
        }


}
