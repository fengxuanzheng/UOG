package com.bean.allAnnotation.utill;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Set;

public class MyImportSelector implements ImportSelector {
    @Override
    //返回需要导入组件全类名
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        Set<String> annotationTypes = importingClassMetadata.getAnnotationTypes();
        annotationTypes.forEach(item->{
            System.out.println("importSelect:"+item);
        });
        return new String[]{"com.bean.allAnnotation.pojo.Pets"};
    }

}
