package com.bean.allAnnotation.condition;


import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Arrays;
import java.util.Map;


public class MyCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Map<String, Object> annotationAttributes = metadata.getAnnotationAttributes("org.springframework.context.annotation.Bean");
        System.out.println(annotationAttributes);
        BeanDefinitionRegistry registry = context.getRegistry();
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
        String[] beanDefinitionNames = registry.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(item->{
            System.out.println("beandingyi:"+item);
        });

        return false;
    }
}
