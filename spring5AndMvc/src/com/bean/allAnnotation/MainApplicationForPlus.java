package com.bean.allAnnotation;

import com.bean.allAnnotation.Plus.BeanFactoryProcess.MyBeanFactoryProcessor;
import com.bean.allAnnotation.Plus.PlusConfig;
import com.bean.allAnnotation.Plus.scopeBean.MyScope;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class MainApplicationForPlus {
    private AnnotationConfigApplicationContext applicationContext;


    @BeforeEach
    public void maintest()
    {
        //在虚拟机参数位置写入:-Dspring.profiles.active=xxx环境


        applicationContext = new AnnotationConfigApplicationContext();

        applicationContext.getEnvironment().setActiveProfiles("test");
        applicationContext.register(PlusConfig.class);
        applicationContext.refresh();
        //发布自定义事件后MyApplicationListener就能监听到
        applicationContext.publishEvent(new ApplicationEvent(new String("自定义事件")) {

        });
    }

    @Test
    public void test()
    {
        MyBeanFactoryProcessor bean = applicationContext.getBean(MyBeanFactoryProcessor.class);
        Map<String,Object> systemProperties = (Map<String, Object>) applicationContext.getBean("systemProperties");
        System.out.println("******************************************************************");
        System.out.println(applicationContext.getBeanDefinitionCount());
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(System.out::println);
        System.out.println("************************************________________________________");
        System.out.println(applicationContext.containsBean("myFactoyBean"));
        System.out.println(applicationContext.containsBean("&myFactoyBean"));
        // Object myFactoyBean = applicationContext.getBean("myFactoyBean", Sister.class);
       // System.out.println(myFactoyBean.getClass());
        // System.out.println(systemProperties);

    }
@Test
    public void test4()
{
    for (int i=0;i<2;i++)
    {
        new Thread(()->{
            MyScope bean = applicationContext.getBean(MyScope.class);
            MyScope bean1 = applicationContext.getBean(MyScope.class);
            Logger.getGlobal().info(bean.toString());
            Logger.getGlobal().info(bean1.toString());
            Logger.getGlobal().info(String.valueOf(bean==bean1));
        }).start();
        try {
            TimeUnit.SECONDS.sleep(2L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


}
