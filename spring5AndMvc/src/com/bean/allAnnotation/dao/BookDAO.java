package com.bean.allAnnotation.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public class BookDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    public void interBook()
    {
        String id= UUID.randomUUID().toString().substring(1,8);
        jdbcTemplate.update("insert into book value (?,?,?)",id,"自定义",648);
    }
}
