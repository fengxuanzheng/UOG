package com.bean.allAnnotation.pojo;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

public class Cat implements InitializingBean, DisposableBean  {
    private String solid;
    @Resource
    private Person person;
    @Value("998")
    /*
    1.基本数值
    2.可以写SpEL表达式,#{}如#{20-1}
    3.可以写${}取出配置文件中的值(在运行环境,因为配置文件中值最终都放在IOC容器那个环境变量里面)
     */
    private Integer price;

    public Cat() {
        System.out.println("Cat构造器");
    }

    public void init()
    {
        System.out.println("Cat初始化了");
    }

   @PostConstruct
   public void initPostConstruct()
    {
        System.out.println("标注了初始化注解初始化");
    }

    @PreDestroy
    public void destroyOfPre()
    {
        System.out.println("标注了销毁注解初始化");
    }
    public void destory()
    {
        System.out.println("Cat被干掉了");
    }

    @Override
    public String toString() {
        return "Cat{" +
                "solid='" + solid + '\'' +
                ", price=" + price +
                '}';
    }


    public String getSolid() {
        return solid;
    }

    public void setSolid(String solid) {
        this.solid = solid;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        System.out.println("开始设置属性");
        this.price = price;
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("销毁接口销毁方法");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("初始化接口初始化方法");
    }
}
