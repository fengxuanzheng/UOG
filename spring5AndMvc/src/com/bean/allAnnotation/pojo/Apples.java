package com.bean.allAnnotation.pojo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.util.StringValueResolver;

public class Apples  implements ApplicationContextAware, BeanNameAware, EmbeddedValueResolverAware {
    //这个接口能为组件加入Ioc容器,因为实现了这个接口后ApplicationContextAwareProcessor的后置处理器就调用setApplicationContext()方法传入IOC容器
    private String name;
    private Integer prices;
    private ApplicationContext applicationContext;

    @Override
    public String toString() {
        return "Apples{" +
                "name='" + name + '\'' +
                ", prices=" + prices +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrices() {
        return prices;
    }

    public void setPrices(Integer prices) {
        this.prices = prices;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("当前bean名字:"+name);
    }

    @Override
    public void setEmbeddedValueResolver(StringValueResolver resolver) {
        //StringValueResolver这个类是spring底层用来解析各种占位符
        String s = resolver.resolveStringValue("你好${os.name}我是#{4398+1}");
        System.out.println("解析字符串"+s);
    }
}
