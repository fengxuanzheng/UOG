package com.bean.allAnnotation.pojo;

public class Dogs {
    private String dogName;
    private Integer dogAge;

    @Override
    public String toString() {
        return "Dogs{" +
                "dogName='" + dogName + '\'' +
                ", dogAge=" + dogAge +
                '}';
    }

    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public Integer getDogAge() {
        return dogAge;
    }

    public void setDogAge(Integer dogAge) {
        this.dogAge = dogAge;
    }
}
