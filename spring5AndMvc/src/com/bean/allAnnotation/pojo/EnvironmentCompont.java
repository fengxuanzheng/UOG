package com.bean.allAnnotation.pojo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class EnvironmentCompont  {

    @Autowired
    private Map<String,Object> systemProperties;



    @Override
    public String toString() {
        return "EnvironmentCompont{" +
                "systemProperties=" + systemProperties +
                '}';
    }
}
