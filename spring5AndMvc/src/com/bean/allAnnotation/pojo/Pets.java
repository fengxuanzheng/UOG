package com.bean.allAnnotation.pojo;

public class Pets {
    private String pName;
    private Integer pAge;

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public Integer getpAge() {
        return pAge;
    }

    public void setpAge(Integer pAge) {
        this.pAge = pAge;
    }

    @Override
    public String toString() {
        return "Pets{" +
                "pName='" + pName + '\'' +
                ", pAge=" + pAge +
                '}';
    }
}
