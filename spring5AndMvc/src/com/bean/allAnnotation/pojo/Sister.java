package com.bean.allAnnotation.pojo;

import org.springframework.beans.factory.InitializingBean;

public class Sister implements InitializingBean {
    private String name;
    private String brother;

    @Override
    public String toString() {
        return "Sister{" +
                "name='" + name + '\'' +
                ", brother='" + brother + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrother() {
        return brother;
    }

    public void setBrother(String brother) {
        this.brother = brother;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("______________________被工厂创建bean初始化方法_________________________");
    }
}
