package com.bean.allAnnotation.pojo;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.SmartLifecycle;

public class MySmarkLife implements InitializingBean, DisposableBean, SmartLifecycle {
    private volatile Boolean isRunning=false;
    @Override
    public void destroy() throws Exception {
        System.out.println("执行MySmarkLife的销毁方法");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("执行MySmarkLife的初始化方法");
    }

    @Override
    public void start() {
        System.out.println("执行MySmarkLife的容器启动方法");
        isRunning=true;
    }

    @Override
    public void stop() {
        System.out.println("执行MySmarkLife的容器关闭方法");
        isRunning=false;
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }
}
