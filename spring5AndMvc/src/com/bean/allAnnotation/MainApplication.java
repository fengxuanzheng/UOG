package com.bean.allAnnotation;

import com.bean.allAnnotation.aop.CalculatorIML;
import com.bean.allAnnotation.config.MyConfig;
import com.bean.allAnnotation.pojo.Cat;
import com.bean.allAnnotation.pojo.Person;
import com.bean.allAnnotation.pojo.Sister;
import com.bean.allAnnotation.server.BookServer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;

public class MainApplication {
    private AnnotationConfigApplicationContext applicationContext;

    @Test
    @BeforeEach
    public void maintest()
    {
        //在虚拟机参数位置写入:-Dspring.profiles.active=xxx环境


        applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.getEnvironment().setActiveProfiles("test");
        applicationContext.register(MyConfig.class);
        applicationContext.refresh();
    }

    @Test
    public void test()
    {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(System.out::println);
        Map<String, Person> beansOfType = applicationContext.getBeansOfType(Person.class);
        System.out.println(beansOfType);
        Environment environment = applicationContext.getEnvironment();
        System.out.println(environment.getProperty("os.name"));
        Sister myFactoyBean = applicationContext.getBean("myFactoyBean", Sister.class);
        System.out.println("工厂"+myFactoyBean);
        Cat cat = applicationContext.getBean("myCat", Cat.class);
        Cat cat1 = applicationContext.getBean("myCat", Cat.class);
        System.out.println(cat);
        System.out.println(cat==cat1);
        applicationContext.close();

    }
    @Test
    public void test2() throws SQLException {
        String[] beanNamesForType = applicationContext.getBeanNamesForType(DataSource.class);
        Arrays.stream(beanNamesForType).forEach(System.out::println);
        DataSource bean = applicationContext.getBean(DataSource.class);
        System.out.println(bean.getConnection());

    }

    @Test
    public void test3()
    {
        CalculatorIML bean = applicationContext.getBean(CalculatorIML.class);
        bean.add(10,10);
    }

    @Test
    public void test4()
    {
        BookServer bean = applicationContext.getBean(BookServer.class);
        bean.updata();
    }
    @Test
    public void test5()
    {
        Cat bean = applicationContext.getBean(Cat.class);
        System.out.println(bean);
    }

}
