package com.bean.allAnnotation;

import com.bean.allAnnotation.config.ConfigTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainApplicationTest {
    private AnnotationConfigApplicationContext applicationContext;
    @BeforeEach
    public void maintest()
    {
        //在虚拟机参数位置写入:-Dspring.profiles.active=xxx环境
        applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.getEnvironment().setActiveProfiles("test");
        applicationContext.register(ConfigTest.class);
        applicationContext.refresh();
        //applicationContext.start();//如果要执行bean的Lifecycle接口方法需要显示调用而SmartLifecycle不需要
        applicationContext.stop();
        applicationContext.close();
    }

    @Test
    public void test()
    {

    }
}
