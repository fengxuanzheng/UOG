package com.bean.allAnnotation.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
/*
try{
    @Before
    method.invoke(obj,arg)
            @AfterReturning
        }cathche(e){
    @AfterThrowing
                }finally{
    @After()
                        }
*/
/*execution(访问权限符 返回值类型 方法全类名(参数表))
通配符:
      *:1.匹配一个或多个字符:execution(public int com.bean.aop.Calcu*.add(int ,int ))
         2.匹配任意一个参数:execution(public int com.bean.aop.CalculatorIML2.add(int ,*)),只匹配两个参数
        3.如果放在路径位置只能匹配一层路径:execution(public int com.bean.*.CalculatorIML2.add(int ,int ))
        4.权限配置*不能写,权限位置不写就代表任意
        5.如果以*开头就可怕匹配任意层
      ..:1.匹配任意多个参数或任意类型参数:execution(public int com.bean.aop.CalculatorIML2.*(.. ))匹配这个类所以方法
          2.匹配多层路径:execution(public int com.bean..CalculatorIML2.add(int ,int ))
最模糊:execution(* *.*(..))

同时切入点表达式支持 && || !

***********************************************************

通知方法执行顺序:
正常执行:before>after>afterreturning
异常执行:before>after>aterthrowing


**********************************************
通知方法参数列表上可以传入一个参数JoinPoint,这个参数封装了方法详细信息
对于afterretrun通知方法可以在注解上通过注解参数returning指定形参列表中用作接受返回值形参
对于afterthrows通知方法可以在注解上通过注解参数throwing指定形参列表中用作接受返回值形参
Exception指定接受那些异常,如果范围过小会接受不到,afterretrun同理,如果实际返回类型大于要接受类型也接受不到

***************************************************************
spring对通知方法不应该,通知方法返回值类型乃至权限没有一个要求,但是参数列表不能乱写,
因为spring是通过反射调用方法,所以sprig必须指定参数列表上每一个值
特别注意如果需要写JoinPoint joinPoint,这个参数一定要在第一位否则报错"formal unbound in pointcut"
*/


@Aspect
@Order(1)
public class LogUtils2 {

    /*抽取可重用切入点表达式*/
    @Pointcut("execution(public int com.bean.allAnnotation.aop.CalculatorIML.*(..))")
    public void resultpoin(){

    }


    @Before("resultpoin()")
    public static void logstart(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        String name = joinPoint.getSignature().getName();
        System.out.println("方法开始执行:"+"方法参数:"+ Arrays.asList(args)+"方法名:"+name);
    }
   @AfterReturning(value = "execution(public int com.bean.allAnnotation.aop.CalculatorIML.*(..))",returning = "obj")
   public static void logReturn(JoinPoint joinPoint,Object obj) {
        System.out.println("方法结束"+"返回值:"+obj);
    }

    @AfterThrowing(value = "execution(public int com.bean.allAnnotation.aop.CalculatorIML.*(..))",throwing = "exception")
    public static void logException(Exception exception) {
        System.out.println("方法异常");
    }
    @After("execution(public int com.bean.allAnnotation.aop.CalculatorIML.*(..))")
    public static void logFinalier() {
        System.out.println("最终执行");
    }



    /*四合一就是环绕通知
    环绕中有一个参数 ProceedingJoinPoint*/

    /*环绕通知和普通通知执行顺序
    普通前置
    {
        环绕前置
        环绕执行目标方法
        环绕返回/环绕异常
         环绕最终
    }
    普通最终
    普通返回/普通异常
    注意对于普通前置和环绕前置,注解下默认环绕前置,但是XML配置下取决于配置顺序先后
            对于异常抛出,环绕异常需要外抛出,才能让普通异常捕捉*/
    @Around("resultpoin()")
    public Object myaround(org.aspectj.lang.ProceedingJoinPoint proceedingJoinPoint)  {

        Object[] args = proceedingJoinPoint.getArgs();
        /*此方法调用才执行目标方法相当于method.invoke*/
        Object proceed=null;
        try {
            System.out.println("环绕前置方法");
            proceed= proceedingJoinPoint.proceed(args);
            System.out.println("环绕返回通知");
        } catch (Throwable throwable) {
            System.out.println("环绕异常通知");
            throwable.printStackTrace();
            throw new RuntimeException(throwable);
        }
        finally {
            System.out.println("环绕最终通知");
        }
        return proceed;

    }
}
