package com.bean.allAnnotation.aop;


public class CalculatorIML  {
    private String caname;
    //@Override
    public int add(int i, int j) {
        int result=i+ j;
        System.out.println("方法开始执行:add()");
        return result;
    }

   // @Override
    public int sub(int i, int j) {
        int result=i-j;
        return result;
    }

   // @Override
    public int mul(int i, int j) {
        int result=i*j;
        return result;
    }

   // @Override
    public int div(int i, int j) {
        int result=i/j;
        return result;
    }

    @Override
    public String toString() {
        return "CalculatorIML{" +
                "caname='" + caname + '\'' +
                '}';
    }
}
