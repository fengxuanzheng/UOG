package com.bean.allAnnotation.filter;

import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;

public class MyTypeFilter implements TypeFilter {
    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
        //获取当前类注解详细
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        //获取当前正在扫描的类信息
        Resource resource = metadataReader.getResource();
        //获取当前类的资源
        String className = classMetadata.getClassName();
        System.out.println("当前类:"+className);
        return false;
    }
}
