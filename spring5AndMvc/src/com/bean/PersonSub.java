package com.bean;


public class PersonSub extends Person{
    private String addr;
    private Integer price;

    @Override
    public String toString() {
        return "PersonSub{" +
                "addr='" + addr + '\'' +
                ", price=" + price +
                '}'+super.toString();
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
