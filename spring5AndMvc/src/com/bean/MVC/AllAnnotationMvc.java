package com.bean.MVC;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AllAnnotationMvc extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * 获取根容器配置类,(spring配置文件) 父容器
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfig.class};
    }

    /**
     * 获取web容器配置类(springmvc配置文件)子容器
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebConfig.class};
    }

    /**
     * 获取DispatcherServlet映射信息
     * / 拦截所有请求包括静态资源,但不包括所有*.jsp
     * /* 拦截所有请求包括*.jsp
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
