package com.bean.MVC.controller;

import com.bean.MVC.addCompomt.MyFilter;
import com.bean.MVC.addCompomt.MyListener;
import com.bean.MVC.addCompomt.MyServlet;

import javax.servlet.*;
import javax.servlet.annotation.HandlesTypes;
import java.util.EnumSet;
import java.util.Set;


/**
 * servlet容器启动使用会扫描每个jar包的META-INF/services/javax.servlet.ServletContainerInitializer中注明的ServletContainerInitializer实现类
 * 的全类名
 *
 * //@HandlesTypes:容器启动时候会将@HandlesTypes指定这个类型下面子类(实现类,接口等传递过来)
 */
@HandlesTypes({})
public class MyServletContainerInitializer implements ServletContainerInitializer {
    /**
     * @param c 接受@HandlesTypes传递过来类型
     * @param ctx 代表当前web引用的ServletContext,一个web引用对应一个ServletContext
     *
     */
    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {

        /*
        * 可以在项目启动时候给ServletContext里面添加组件,注意必须要在项目启动前时候添加才有效
        * 1.可以使用本方式的ServletContainerInitializer
        * 2.可以在ServletContextListener监听器初始化方法上获得的ServletContext对象也可以添加组件
        */
        //注册servlet
        ServletRegistration.Dynamic myserlet = ctx.addServlet("myserlet", MyServlet.class);
        //配置映射信息
        myserlet.addMapping("/myservlet");

        //注册Listener
        ctx.addListener(MyListener.class);

        //注册拦截器
        FilterRegistration.Dynamic myfilter = ctx.addFilter("myfilter", MyFilter.class);

        myfilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),true,"/*");

    }
}
