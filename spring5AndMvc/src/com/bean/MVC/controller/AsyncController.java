package com.bean.MVC.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.Callable;

@RestController
public class AsyncController {

    /**
     * 1.控制器返回Callable
     * 2.Spring异步处理,将Callable提交到TaskExecutor使用一个隔离线程进行执行
     * 3.DispatcherServlet和所有Filter退出web容器线程,但是response保持打开状态
     * 4.Callable返回结果,SpringMvc将请求重新派发给容器,恢复之前处理
     * 5.根据Callable返回结果.springMVc继续进行视图渲染等流程(从收到请求-视图渲染)
     */
    /*
     *异步拦截器:
     * 1.原生API:AsyncListener
     * 2.SpringMvc,实现AsyncHandlerInterceptor
     */
    @GetMapping("/myAsyncController")
    public Callable<String> myAsyncController()
    {
        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "测试";
            }
        };
        return callable;
    }


    @GetMapping("/deffre")
    public DeferredResult<Object> pushDeferredResult()
    {
        DeferredResult<Object> deferredResult = new DeferredResult<>((long) 3000, "超时");
        AsyncConllectionUtill.save(deferredResult);
        return deferredResult;
    }

    @GetMapping("/sultDeffred")
    public String sultDeffred()
    {
        DeferredResult<Object> deferredResult = AsyncConllectionUtill.getDeferredResult();
        deferredResult.setResult("异步回调");
        return "success";
    }
}
