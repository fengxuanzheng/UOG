package com.bean.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/home")//在这里标注相当于给当前类所以方法指定一个基础路径,同时这个基础路径默认映射标注@RequestMapping的没有指定映射路径方法
public class HelloContrller {
//@RequestMapping地址:可以不写/,不写也是默认从当前项目开始
    @RequestMapping("/hello")
    public String hello()
    {
        return "success";
    }

   @RequestMapping
   public String home()
    {
        return "success";
    }
}
