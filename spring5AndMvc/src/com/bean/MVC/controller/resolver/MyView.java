package com.bean.MVC.controller.resolver;

import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

public class MyView implements View {
    @Override
    public String getContentType() {
        return "text/html";
    }

    @Override
    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html;charset=utf-8");
        model.forEach((Object key,Object value)->{

            System.out.println("键:"+key);
            System.out.println("值:"+value);
        });
        PrintWriter writer = response.getWriter();
        writer.write("自定义视图页面");
    }
}
