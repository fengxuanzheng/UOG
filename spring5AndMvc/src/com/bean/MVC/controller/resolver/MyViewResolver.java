package com.bean.MVC.controller.resolver;

import org.springframework.core.Ordered;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import java.util.Locale;

public class MyViewResolver implements ViewResolver, Ordered {

    @Override
    public View resolveViewName(String viewName, Locale locale) throws Exception {
        if (viewName.startsWith("myview:"))
        {
            return new MyView();
        }
        else{

            return null;
        }
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
