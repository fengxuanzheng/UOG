package com.bean.MVC.controller;

import com.bean.MVC.controller.exceprion.MyResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.DefaultRequestToViewNameTranslator;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
public class TestController {
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/handler2",method = RequestMethod.POST)
    public String handler2()
    {
        System.out.println("handler2****");
        return "success";
    }
    @RequestMapping(value = "/handler3",params ={"name"})
    //params:规定请求参数
    //params和headers支持简单表达式:
    //param1:表示请求必须包含参数名为param1的请求参数
    //!param1:表示不能包含这样请求参数
    //param=value表示请求参数必须为指定参数名==指定值如果是param!=value相反
    //param={xxx,xxx}表示必须满足以上规则
    public String handler3()
    {
        return "success";
    }
    //***************************************************************************
    //consumes:只接受内容类型是那种请求,规定请求同中的Content-TYype
    //produce:告诉浏览器返回内容类型,给响应头加上Content-Type

    //*************************************************************************
    /*
    URL地址模糊匹配:
    ?:能代替任意一个字符,0个多个多不行
    *:能替代任意多个字符,和一层路径
    **:能替代多次路径,哪怕不写也可以

    */
    @RequestMapping(value = "/ant?")
    public String antest1()
    {
        return "success";
    }

    @RequestMapping(value = "/party/{id}")
    public String part(@PathVariable("id") String id, HttpServletRequest httpServletRequest)
    {
        Locale locale = httpServletRequest.getLocale();
        return "success";
    }

    @RequestMapping("/models")
    public String getmodbf(@ModelAttribute("msg")String msg)
    {
        System.out.println(msg);
        return "success";
    }

    @ModelAttribute
    public void getmod(Model model)
    {
        model.addAttribute("msg","提前保存信息");
        System.out.println("提前运行");
    }
    @RequestMapping("/sourcez")
    public String getsource()
    {
        System.out.println("源码调试");
        int i=10/0;
        return "success";
    }
    @RequestMapping("/i18ns")
    public String i18n(String username,String password,Locale locale,HttpServletRequest httpServletRequest)
    {

        //手动获取国际化区域信息
        String username1 = messageSource.getMessage("username", null, locale);

        System.out.println("国际化:"+username1);
        return "success";
    }

    @RequestMapping("/myview")
    public String myview(Model model)
    {
        model.addAttribute("mykes","你懂的");
        model.addAttribute("one2","小黄书");
        return "myview:success";
    }

    //告诉MVC这个方法装配处理这个类异常
    /*
    1.可以给方法上加上Exception用来接受发生异常
    2.要携带异常信息不能在参数位置写Model,因为这个方法只接受Exception这个参数,其他参数不认识
    3.可以返回ModelAndView
    4.全局异常和本类异常同时存在时,本类优先(哪怕是全局更精确)*/

    @ExceptionHandler(Exception.class)
   public ModelAndView handlerExceprion(Exception exception)
   {
       ModelAndView myerror = new ModelAndView("myerror");
       myerror.addObject("error",exception);
       return myerror;
   }

    @RequestMapping("/responsetException")
    public String responseExceprion(String name)
    {
        if (!name.equals("username"))
        {
            throw new MyResponseStatus();
        }
        System.out.println("自定义异常错误");
        return "success";

    }

    @RequestMapping("/retrunmap")
    public Map<String,String> retrunMap()
    {
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("test","测试数据");
        DefaultRequestToViewNameTranslator defaultRequestToViewNameTranslator = new DefaultRequestToViewNameTranslator();

        return stringStringHashMap;
    }
}
    //***********************************************************************'
   // 关于REST风格高版本TOMCAT可能不兼容问题,解决办法开启JSP九代内置对象中的exception(在<%@t头里面加上 isErrorPage=true)

    //*******************************************************************************
    //对于@RequestParam,可以自动为一个POJO自动赋值,将POJO的每个属性从request参数中尝试获取出来封装即可;
    //同时还支持级联属性封装

    //******************************************************
    //对于数据带到页面可以用 Map Model ModelMap,这些参数都会将数据保存到请求域中,而且这三个在MVC里面实际都是BingdingAwareModelMap在工作,
    //相当于它保存数据都是放在请求域中
    //同时还可以返回ModelAndView类型,而且数据也是放在请求域中
    //********************************************************
    //对于在会话域中保存数据可以用@SessionAttributes,而且只能标在类上
    //Value属性表示给BingdingAwareModelMap或ModelAndView保存的数据同时也给session中保存一份,value指定要保存数据key
    //types属性指定要保存数据类型.其他同value
     //****************************************************************
      //@ModelAttribute标注到方法上后这个方法会先于目标方法先运行


//***************************************************************
//forward:前缀转到到页面,此前缀视图解析器不会拼窜,转发到当前项目下,一定要加/,如果不加就是相对路径转发
//redirect:转发到相应路径,不会拼串,同时MVC下默认是项目路径,重定向时不用想原生JAVAWEB那样加上项目名,springMVC默认就是当前项目下
//特别补充,重定向,响应头会带有Location的字段


//***************************************************************
/*关于spring表达标签
比如<form:form>
表单项属性path对于原始表单的name属性，有两种作用，一是作为原始name项，二是自动回显隐含模型中某个对象对应的属性值
对于表单标签的select项，属性iteam表示要遍历集合；iteamLabel=属性名，指定遍历出对象那个属性作为option标签体的值
iteamValue=属性，表示遍历出来对象那个属性作为要提交的value值

使用spring表单标签，springmvc认为表单中数据每一项都要回显，path指定的是一个属性，这个属性从隐含模型中（请求域中取出某个对象属性值），
所以path指定的属性，请求域（隐含模型）中必须有一个对象有这个属性
，默认从隐含模型中去command对象的属性
如果要修改默认指定隐含模型中的key，在spring标签体上使用modelAttribute=“key”,重新指定key从指定key的模型中去数据
对于数据校验结果显示可以使用error标签，path写需要显示校验表单项属性就可以*/


//*********************************************
/*
对于拦截器：
preHandle:方法是在目标方法之前调用，如果返回true则放行类似于（chain.doFilter放行）
postHandle:在目标方法运行之后调用，类似于目标方法调用之后
afterCompletion:在整个请求完成之后，来到目标页面之后(此时页面已经输出打印)；类似于chain.doFilter放行，资源响应之后
总体流程：preHandler>目标方法>postHandler>页面>afterCompletion

具体细节：
preHandler如果不放行后面都没有流程
只要放行了,afterCompletion一定会执行,多拦截器情况下，已经放行的也一定会执行*/

//*******************************************
/*对于异常处理*/
/*
使用EXceprionHandlerExceptionResolver异常解析需要标注@ExceptionHandler
ResponseStatus.......需要@ResponseStatus
DefaultHander:判断是否mvc默认自带异常*/

/*
对于@ResponseStatus注解是标注在自定义异常类上的,如果标注在正常方法上,哪怕没错误也会抛出一个异常去到错误页面
正常用法是自定义一个异常类,在异常类上标注该注解,后在方法上选择抛出该异常

而外增加:可以自定义异常解析器,不够一般用现成的,一般选用SimpleMappingExceptionResolver,在IOC容器注册*/



