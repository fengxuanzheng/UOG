package com.bean.MVC.controller.exceprion;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class MyExceptionResolver {
    @ExceptionHandler(Exception.class)
    public ModelAndView handlerGorp(Exception exception)
    {
        ModelAndView error = new ModelAndView("error");
        error.addObject("gerror",exception);
        return error;
    }
}
