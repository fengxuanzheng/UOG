package com.bean.MVC.controller.exceprion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(reason ="反正就是错了",value = HttpStatus.NOT_FOUND)
public class MyResponseStatus extends RuntimeException{
    private static final long serialVersionUID=545645656L;
}
