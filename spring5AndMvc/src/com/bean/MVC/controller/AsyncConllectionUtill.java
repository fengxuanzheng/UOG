package com.bean.MVC.controller;

import org.springframework.web.context.request.async.DeferredResult;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class AsyncConllectionUtill {

    private static Queue<DeferredResult<Object>> deferredResults=new ConcurrentLinkedQueue<>();

    public static void save(DeferredResult<Object> deferredResult)
    {

        deferredResults.add(deferredResult);
    }

    public static DeferredResult<Object> getDeferredResult()
    {
        return deferredResults.poll();
    }


}
