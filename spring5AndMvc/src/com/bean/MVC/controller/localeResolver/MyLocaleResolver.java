package com.bean.MVC.controller.localeResolver;

import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public class MyLocaleResolver implements LocaleResolver {
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        Locale newlocale=null;
        String locale = request.getParameter("locale");
        if (locale!=null&&"".equals(locale))
        {
            newlocale=new Locale(locale.split("_")[0],locale.split("_")[1]);

        }
        else
        {
            newlocale=request.getLocale();
        }
        return newlocale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}
