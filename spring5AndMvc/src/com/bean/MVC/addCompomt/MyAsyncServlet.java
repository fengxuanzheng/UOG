package com.bean.MVC.addCompomt;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/async",asyncSupported = true)
public class MyAsyncServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AsyncContext asyncContext = req.startAsync();

        System.out.println("主线程开启"+Thread.currentThread().getName());
        asyncContext.start(()->{
            System.out.println("异步开始"+Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
                ServletResponse response = asyncContext.getResponse();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("异步结束"+Thread.currentThread().getName());
        });
        System.out.println("主线程结束"+Thread.currentThread().getName());
    }
}
