package com.bean.annotation;

import org.springframework.stereotype.Component;

@Component
public class Students extends Schools{

    private String stuname;

    public Students()
    {

    }
    public Students(String stuname)
    {
        this.stuname=stuname;
    }

    public String getStuname() {
        return stuname;
    }

    public void setStuname(String stuname) {
        this.stuname = stuname;
    }

    @Override
    public String toString() {
        return "Students{" +
                "stuname='" + stuname + '\'' +
                '}';
    }
}
