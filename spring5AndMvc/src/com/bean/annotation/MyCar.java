package com.bean.annotation;


public class MyCar {

    private String carName;
    private Integer price;

    public MyCar(String carName, Integer price) {
        this.carName = carName;
        this.price = price;
    }

    public MyCar() {
    }

    @Override
    public String toString() {
        return "MyCar{" +
                "carName='" + carName + '\'' +
                ", price=" + price +
                '}';
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
