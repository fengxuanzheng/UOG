package com.bean.annotation;

import org.springframework.stereotype.Service;

@Service
public class Schools {
    private String name;
    private Integer bigs;

    public Schools(String name, Integer bigs) {
        this.name = name;
        this.bigs = bigs;
    }

    public Schools() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBigs() {
        return bigs;
    }

    public void setBigs(Integer bigs) {
        this.bigs = bigs;
    }

    @Override
    public String toString() {
        return "Schools{" +
                "name='" + name + '\'' +
                ", bigs=" + bigs +
                '}';
    }
}
