package com.bean.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("mypersonto")//修改默认id
@Scope(value = "singleton")//修改作用域默认为单例
public class Myperson {
    private String name;
    private Integer age;
    private String lives;
    @Autowired
    @Qualifier("myCar2")
    //1.先按照属性类型去IOC容器中找对于组件,如果没找到就报错
    //2.如果找到多个情况下
    //2.1:按照属性名作为ID继续匹配,如果还没有报错
    //3.Autowired也可以标注在方法上,标注后方法上参数都会自动注入,而且此方法会在IOC容器启动前自动运行
 /*   @Qualifier("mycar01")此注解可以指定一个ID,让spring不使用默认变量名作为ID进行匹配,同时这个注解也可以写在方法形参上*/

    /*@Resource 此注解和Autowired有相同作用同时也是Autowired和Qualifier整合可以指定自定义ID*/
    private MyCar myCar;

    @Override
    public String toString() {
        return "Myperson{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", lives='" + lives + '\'' +
                ", myCar=" + myCar +
                '}';
    }

    public Myperson() {
    }

    @Autowired
    public void getcar(@Qualifier("myCar2") MyCar myCar)
    {
        System.out.println("自动运行"+myCar);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getLives() {
        return lives;
    }

    public void setLives(String lives) {
        this.lives = lives;
    }

}
