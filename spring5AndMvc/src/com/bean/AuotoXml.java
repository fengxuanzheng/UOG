package com.bean;

public class AuotoXml {

    private Car car;
    private Book book;

    public AuotoXml() {
    }

    public AuotoXml(Book book) {
        this.book = book;
    }

    public AuotoXml(Car car, Book book) {
        this.car = car;
        this.book = book;
    }

    public AuotoXml(Car car) {
        this.car = car;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "AuotoXml{" +
                "car=" + car +
                ", book=" + book +
                '}';
    }
}
