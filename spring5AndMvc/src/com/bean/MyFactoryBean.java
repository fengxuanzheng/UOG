package com.bean;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

public class MyFactoryBean implements FactoryBean<Person>, InitializingBean {
    @Override
    public Person getObject() throws Exception {
        Person person = new Person();
        person.setAge(14);
        person.setLastname("奈奈");
        return person;
    }

    @Override
    public Class<?> getObjectType() {
        return Person.class;
    }

    @Override
    public boolean isSingleton() {
        return FactoryBean.super.isSingleton();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("在FacoryBean设置完之后");
    }
}
