package com.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(beanName + ":这个bean要初始化." + "bean对象具体是:" + bean);
        //这个bean对象一定要返回,这个bean就是要初始化bean
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(beanName + ":这个bean已经初始化完了." + "bean对象具体是:" + bean);
        //初始化后返回的bean,返回的是什么容器中保存的就是什么
        return bean;
    }
}
