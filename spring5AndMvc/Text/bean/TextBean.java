package bean;

import com.bean.Car;
import com.bean.Person;
import com.bean.annotation.Schools;
import com.bean.annotation.Students;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

public class TextBean {
   private ApplicationContext applicationContext;

    @BeforeEach
    public  void beforeinnit()
    {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        applicationContext=classPathXmlApplicationContext;
    }
    @Test
    public void test()
    {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        Person person = classPathXmlApplicationContext.getBean("person", Person.class);
        System.out.println(person);
    }

    @Test
    public void test15()
    {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        Person person = classPathXmlApplicationContext.getBean("person40", Person.class);
        System.out.println("abfdfg:"+person);
    }

    @Test
    public void test2()
    {
        System.out.println(applicationContext.getBean("person2", Person.class));
        System.out.println(applicationContext.getBean("person3", Person.class));
        System.out.println(applicationContext.getBean("person5", Person.class));
        Person person6 = applicationContext.getBean("person6", Person.class);
        Map<String, Object> objectMap = person6.getObjectMap();
        System.out.println( person6);
        System.out.println(objectMap.getClass());
    }

    @Test
    public void test3() {
        Person person7 = applicationContext.getBean("person7", Person.class);
        System.out.println(person7);

    }
    @Test
    public void test4() {
        Car car01 = applicationContext.getBean("car01", Car.class);
        System.out.println(car01);

    }
    @Test
    public void test5() {
        System.out.println(applicationContext.getBean("person10"));

    }
    @Test
    public void test16()
    {
        Object person11 = applicationContext.getBean("person11");
        Object person12 = applicationContext.getBean("person11");
        System.out.println(person11 == person12);
    }
    @Test
    public void test17()
    {
        Person person11 = (Person) applicationContext.getBean("person110");
        Car car110 = (Car) applicationContext.getBean("car110");
        System.out.println("比较结果:"+(person11.getCar() == car110));
    }

    @Test
    public void test6() {
        System.out.println(applicationContext.getBean("airplane1"));
        System.out.println(applicationContext.getBean("airplane2"));

    }
    @Test
    public void test7() {
        System.out.println(applicationContext.getBean("myfactorybean"));

    }

    @Test
    public void test8() {
        System.out.println(applicationContext.getBean("initbook"));

    }

    @Test
    public void test9() {
        System.out.println(applicationContext.getBean("car11"));

    }
    @Test
    public void test10() {
        System.out.println(applicationContext.getBean("person13"));

    }
    @Test
    public void test11() {
        System.out.println(applicationContext.getBean("mypersonto"));

    }

    @Test
    public void test12() {
        Schools bean = applicationContext.getBean(Schools.class);
        System.out.println(bean);
        System.out.println(bean.getClass());
        Students bean1 = applicationContext.getBean(Students.class);
        System.out.println(bean1);
        System.out.println(bean == bean1);

    }

}
