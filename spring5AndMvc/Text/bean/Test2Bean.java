package bean;

import com.bean.Person;
import com.bean.SisterP;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test2Bean {
    private ApplicationContext applicationContext;

    @BeforeEach
    public  void beforeinnit()
    {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContextTest.xml");
        applicationContext=classPathXmlApplicationContext;
    }

    @Test
    public void test()
    {
        Person bean = applicationContext.getBean("person2",Person.class);
        System.out.println(bean);
        bean.setLastname("一抹多");
        Person bean1 = applicationContext.getBean("person2",Person.class);
        System.out.println(bean1);
    }
    @Test
    public void test2()
    {
        Person person3 = applicationContext.getBean("person3", Person.class);
        System.out.println(person3);
    }

    @Test
    public void test3()
    {
        SisterP bean = applicationContext.getBean(SisterP.class);
        Person person = bean.getPerson();
        System.out.println(person);
        Person person1 = bean.getPerson();
        System.out.println(person1);
        System.out.println(person == person1);
        System.out.println(bean);
    }
    @Test
    public void test4()
    {
      //  Person person10 = applicationContext.getBean("person10", Person.class);
      //  System.out.println(person10);
        Person personSb = applicationContext.getBean( Person.class);
        System.out.println(personSb);
    }
}
