package Jdbc_Tx;

import com.bean.JDBC.BookService;
import com.bean.aop.Calculator;
import com.bean.aop.CalculatorIML;
import com.bean.aop.CalculatorTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcTest {
    private ApplicationContext applicationContext;
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public  void beforeinnit()
    {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        applicationContext=classPathXmlApplicationContext;
        JdbcTemplate bean = classPathXmlApplicationContext.getBean(JdbcTemplate.class);
        jdbcTemplate=bean;
    }

    @Test
    public void test() {
        String sql="update employee set salary=? where emp_id=?";
        int update = jdbcTemplate.update(sql, 5000, 1);
        System.out.println(update);
    }
    
    @Test
    public void test2() throws InterruptedException {
        BookService bean = applicationContext.getBean(BookService.class);
        System.out.println(bean.getClass());//class com.bean.JDBC.BookService$$EnhancerBySpringCGLIB$$2fb0a5e2 是代理对象不是BookServer本身
        bean.checkout("Jerry","ISBN-001");

    }
    
    @Test
    public void test3() {

        Calculator calculatorIML = new CalculatorIML();
        System.out.println(calculatorIML);
        CalculatorIML calculatorTest = new CalculatorTest();
        System.out.println(calculatorTest);
    }


}
