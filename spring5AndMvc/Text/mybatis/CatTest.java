package mybatis;

import com.mybatis.dao.CatDAO;
import com.mybatis.pojo.Cat;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class CatTest {
    private SqlSessionFactory sqlSessionFactory;

    @BeforeEach
    public void before() throws IOException { String resource = "file:D:\\JAVA\\UOG\\out\\production\\spring5AndMvc\\mybatis-config.xml";
        //InputStream inputStream = Resources.getResourceAsStream(resource);
        InputStream urlAsStream = Resources.getUrlAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(urlAsStream);
    }
    @Test
    public void test() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        CatDAO mapper = sqlSession.getMapper(CatDAO.class);
        Cat getcat = mapper.getcat(1);
        System.out.println(getcat);


    }

}
