package mybatis;

import com.mybatis.dao.TeacherDAO;
import com.mybatis.pojo.Teacher;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TeacherTest {
    private SqlSessionFactory sqlSessionFactory;

    @BeforeEach
    public void before() throws IOException { String resource = "file:D:\\JAVA\\UOG\\out\\production\\spring5AndMvc\\mybatis-config.xml";
        //InputStream inputStream = Resources.getResourceAsStream(resource);
        InputStream urlAsStream = Resources.getUrlAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(urlAsStream);
    }
    @Test
    public void test () {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = mapper.getTeacher(1);
        System.out.println(teacher);
        /*Assert.assertArrayEquals();*/
    }
    @Test
    public void test2() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher();
        teacher.setId(1);
        teacher.setTeacherName("s1");
       // teacher.setAddress("柳南");
        List<Teacher> teacherCondition = mapper.getTeacherCondition(teacher);
        System.out.println(teacherCondition);

    }
    @Test
    public void test3() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher();
        teacher.setId(1);
        teacher.setTeacherName("A");
        // teacher.setAddress("柳南");
        List<Teacher> teacherConditionforTrim = mapper.getTeacherConditionforTrim(teacher);
        System.out.println(teacherConditionforTrim);

    }
    @Test
    public void test4() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        //integers.add(3);
        List<Teacher> teacherConditionByidIn = mapper.getTeacherConditionByidIn(integers);
        System.out.println(teacherConditionByidIn);

    }
    @Test
    public void test5() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher();
        //teacher.setId(1);
        //teacher.setTeacherName("s1");
        //teacher.setAddress("柳南");
        List<Teacher> teacherConditionforChooose = mapper.getTeacherConditionforChooose(teacher);
        System.out.println(teacherConditionforChooose);

    }
    @Test
    public void test6() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher();
        teacher.setId(1);
        teacher.setTeacherName("A班");
        teacher.setAddress("圣芙蕾雅学园");
        int i = mapper.updateTeacher(teacher);
        System.out.println(i);
    }
    @Test
    public void test7() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher(1,"G98","逆商","天命",new Date());
        int i = mapper.allupdateTeacher(teacher);
        System.out.println(i);
    }
    @Test
    public void test8() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher();
        teacher.setId(3);
        teacher.setTeacherName("D班");
        teacher.setAddress("天空群岛");
        int i = mapper.updateTeacheraddParam(teacher);
        System.out.println(i);

    }
    @Test
    public void test9() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher(null, "gcd", "cccssd", "sddfe", new Date());
        Teacher teacher1 = new Teacher(null, "dcd", "cccssd", "sddfe", new Date());
        Teacher teacher2 = new Teacher(null, "zcd", "cccssd", "sddfe", new Date());
        ArrayList<Teacher> teachers = new ArrayList<>();
        teachers.add(teacher1);
        teachers.add(teacher2);
        teachers.add(teacher);
        int i = mapper.addTeahcer(teachers);
        System.out.println(i);
    }
    @Test
    public void test10() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher(null, "gcd", "cccssd", "sddfe", new Date());
        Teacher teacher1 = new Teacher(null, "dcd", "cccssd", "sddfe", new Date());
        Teacher teacher2 = new Teacher(null, "zcd", "cccssd", "sddfe", new Date());
        ArrayList<Teacher> teachers = new ArrayList<>();
        teachers.add(teacher1);
        teachers.add(teacher2);
        teachers.add(teacher);
        System.out.println(mapper.addTeahcers(teachers));

    }

    //下面开启批量执行器执行批量操作
    @Test
    public void test11() {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = new Teacher(null, "gcd", "cccssd", "sddfe", null);
        ArrayList<Teacher> teachers = new ArrayList<>();
        teachers.add(teacher);
        int a=0;
        for (int i=0;i<300;i++)
        {
            a=mapper.addTeahcer(teachers);
        }
        sqlSession.commit();
        sqlSession.close();
        System.out.println(a);


    }

    //%%%%%%*****************************************************************
    //缓存测试
    @Test
    public void test12(){
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TeacherDAO mapper = sqlSession.getMapper(TeacherDAO.class);
        Teacher teacher = mapper.getTeacher(1);
        System.out.println("111111"+teacher);
      /*  Teacher teacher2 = mapper.getTeacher(1);
        System.out.println("222222222222"+teacher2);*/
        sqlSession.close();
        SqlSession sqlSession1 = sqlSessionFactory.openSession();
        TeacherDAO mapper1 = sqlSession1.getMapper(TeacherDAO.class);
        Teacher teacher1 = mapper1.getTeacher(1);
        System.out.println("33333333333333333"+teacher1);
        sqlSession1.close();
     /*   Teacher teacher3 = mapper1.getTeacher(1);
        System.out.println("4444444444444444444444444"+teacher3);*/
    }
}
