package mybatis;

import com.mybatis.dao.KeyDAO;
import com.mybatis.pojo.Key;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class KeyTest {
    private SqlSessionFactory sqlSessionFactory;

    @BeforeEach
    public void before() throws IOException { String resource = "file:D:\\JAVA\\UOG\\out\\production\\spring5AndMvc\\mybatis-config.xml";
        //InputStream inputStream = Resources.getResourceAsStream(resource);
        InputStream urlAsStream = Resources.getUrlAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(urlAsStream);
    }


    @Test
    public void test() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        KeyDAO mapper = sqlSession.getMapper(KeyDAO.class);
        Key keyByid = mapper.getKeyByid(1);
        System.out.println(keyByid);

    }
    @Test
    public void test2() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        KeyDAO mapper = sqlSession.getMapper(KeyDAO.class);
        Key keyByidForassociation = mapper.getKeyByidForassociation(2);
        System.out.println(keyByidForassociation);
    }
    @Test
    public void test3() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        KeyDAO mapper = sqlSession.getMapper(KeyDAO.class);
        Key keyByidSimple = mapper.getKeyByidSimple(2);
        System.out.println(keyByidSimple.getKeyName());
        System.out.println(keyByidSimple.getLock());

    }

}
