package mybatis;

import com.mybatis.dao.EmployeeDAO;
import com.mybatis.pojo.EmpStatus;
import com.mybatis.pojo.Employee;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class EmployeeTest {
    private SqlSessionFactory sqlSessionFactory;

    @BeforeEach
    public void before() throws IOException { String resource = "file:D:\\JAVA\\UOG\\out\\production\\spring5AndMvc\\mybatis-config.xml";
        //InputStream inputStream = Resources.getResourceAsStream(resource);
        InputStream urlAsStream = Resources.getUrlAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(urlAsStream);}



    @Test
    public void test() throws IOException {

        SqlSession sqlSession = sqlSessionFactory.openSession();
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);
        System.out.println(mapper.getEmployee(12));


    }
    @Test
    public void test2() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);
        Employee employee = new Employee(null, "fsef", 1, "fffff");
        int insert = mapper.insert(employee);
        System.out.println(employee);
        sqlSession.close();
        System.out.println(employee);


    }

    @Test
    public void test3() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);
        Employee employee = new Employee(null, "fsef", 1, "fffff");
        int insert = mapper.insertforNoAuto(employee);
        sqlSession.close();
        System.out.println(employee);

    }
    @Test
    public void test4() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);

        Employee qwe = mapper.getEmployeeforname(1, "qwe");
        System.out.println(qwe);

    }
    @Test
    public void test5() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);
        List<Employee> employees = mapper.getemployeeList();
        System.out.println(employees);

    }
    @Test
    public void test6() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);
        Map<String, Object> getemployeeformap = mapper.getemployeeformap(1);
        System.out.println(getemployeeformap);

    }
    @Test
    public void test7() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);
        Map<String, Employee> getemployeeformaps = mapper.getemployeeformaps();
        System.out.println(getemployeeformaps);

    }

    //扩展，枚举测试
    @Test
    public void test8() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);
        Employee emps = new Employee(null, "七海", 14, "76124@qq.com");
        emps.setEmpStatus(EmpStatus.LOGIN);
        EmpStatus login = EmpStatus.LOGIN;
        System.out.println(login.ordinal());
        System.out.println(login.name());
        System.out.println(mapper.insert(emps));

    }




}
