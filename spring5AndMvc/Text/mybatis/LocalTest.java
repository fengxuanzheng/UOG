package mybatis;

import com.mybatis.dao.LockDAO;
import com.mybatis.pojo.Lock;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class LocalTest {
    private SqlSessionFactory sqlSessionFactory;

    @BeforeEach
    public void before() throws IOException { String resource = "file:D:\\JAVA\\UOG\\out\\production\\spring5AndMvc\\mybatis-config.xml";
        //InputStream inputStream = Resources.getResourceAsStream(resource);
        InputStream urlAsStream = Resources.getUrlAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(urlAsStream);
    }

    @Test
    public void test() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        LockDAO mapper = sqlSession.getMapper(LockDAO.class);
        Lock lockByid = mapper.getLockByid(3);
        System.out.println(lockByid);

    }
    @Test
    public void test2() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        LockDAO mapper = sqlSession.getMapper(LockDAO.class);
        Lock lockByStep = mapper.getLockByStep(3);
        System.out.println(lockByStep);

    }

}
