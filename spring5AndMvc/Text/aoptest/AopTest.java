package aoptest;

import com.bean.aop.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AopTest {
    private ApplicationContext applicationContext;

    @BeforeEach
    public  void beforeinnit()
    {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        applicationContext=classPathXmlApplicationContext;
    }

    @Test
    public void test() {
        CalculatorIML calculatorIML = new CalculatorIML();
        Calculator proxy = CalculatorProxy.getProxy(calculatorIML);
        System.out.println(proxy.add(10, 20));
    }

    @Test
    public void test2() {
        Calculator calculatorIML = applicationContext.getBean( Calculator.class);
/*
        从IOC获取被代理对象需要使用接口获取,使用代理之后IOC创建的是代理对象(代理对象实现了接口,需要使用接口获取,IOC没有创建被代理对象),没代理才能正常返回被代理对象*/
        System.out.println(calculatorIML.add(50, 80));

        //也可以通过ID拿,不够通过ID得到的是代理对象,不是被代理类,需要用接口引用
        Calculator calculatorIML1 = (Calculator) applicationContext.getBean("calculatorIML");

        //******************************************************************************
        //特别说明:spring可以不需要接口也可以实现动态代理,在被代理类没有实现接口下,可以通过被代理类获取,只是cglib帮我们创建好的代理对象
        //通过ID获取也是一样,不过是转化为被代理类类型
        System.out.println("**********************************************");
        CalculatorIML2 bean = applicationContext.getBean(CalculatorIML2.class);
        System.out.println(bean.add(15, 10));
    }

    @Test
    public void test3() {
        CalculatorIML_XML calculatorIMLXml = applicationContext.getBean("calculatorIMLXml", CalculatorIML_XML.class);
        System.out.println(calculatorIMLXml.add(54, 100));

    }

    @Test
    public void test4()
    {
        Calculator proxy = CalculatorProxy.getProxy(new CalculatorIML());
        System.out.println(proxy.add(10, 10));
        System.out.println(proxy.div(100, 81));
    }



    }

