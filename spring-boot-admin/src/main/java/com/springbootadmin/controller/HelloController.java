package com.springbootadmin.controller;

import com.springbootadmin.pojo.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    private Person person;

    @GetMapping("/person")
    public Person getPerson()
    {
        return person;
    }
}
