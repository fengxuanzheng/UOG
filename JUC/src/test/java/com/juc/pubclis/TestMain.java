package com.juc.pubclis;

import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.ReentrantLock;

public class TestMain {
    public static void main(String[] args) {
        PubSb pubSb = new PubSb();
        pubSb.met1();
        System.out.println("********************************");
        pubSb.met2();
    }

    @Test
    public void test()
    {
        ReentrantLock reentrantLock = new ReentrantLock();
        new Thread(()->{
            reentrantLock.lock();
            try {
                System.out.println("加锁成功:"+Thread.currentThread().getName());
            }
            finally {
                reentrantLock.unlock();
            }
    },"A").start();
        new Thread(()->{
            reentrantLock.lock();
            try {
                System.out.println("加锁成功:"+Thread.currentThread().getName());
            }
            finally {
                reentrantLock.unlock();
            }
        },"B").start();

    }
}
