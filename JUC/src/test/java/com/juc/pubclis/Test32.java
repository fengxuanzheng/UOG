package com.juc.pubclis;

import lombok.extern.slf4j.Slf4j;

import static com.juc.Sleeper.sleep;


@Slf4j(topic = "c.Test32")
public class Test32 {
    // 易变
    static boolean run = true;
    private final Object lock=new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            while(true){
                //sleep(1);
                System.out.println("退出了");
              //  synchronized (log) {
                    if (!run) {

                        break;
                    }
              //  }
            }
        });
        t.start();

        sleep(5);
        log.debug("主线程让你停下来");
            run = false; // 线程t不会如预想的停下来
    }
}
