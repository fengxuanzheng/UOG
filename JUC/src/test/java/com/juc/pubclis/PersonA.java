package com.juc.pubclis;

public class PersonA {
    private String name;
    private Integer age;

    public PersonA(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public PersonA() {
    }

    @Override
    public String toString() {
        return "PersonA{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }
}
