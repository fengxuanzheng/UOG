package com.juc.pubclis;

import com.juc.Sleeper;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j(topic = "c.Test35")
public class Test35 {
    public static void main(String[] args) {
        DecimalAccountCas decimalAccountCas = new DecimalAccountCas(new GarbageBag("原始状态"));
        decimalAccountCas.withdraw();
    }
}

@Slf4j
class DecimalAccountCas {
    private AtomicReference<GarbageBag> balance;

    public DecimalAccountCas(GarbageBag garbageBag) {
//        this.balance = balance;
        this.balance = new AtomicReference<>(garbageBag);
    }


    public GarbageBag getBalance() {
        return balance.get();
    }


    public void withdraw() {

            GarbageBag prev = balance.get();
        System.out.println(prev);


        new Thread(() -> {
            log.debug("start...");
            GarbageBag prev1 = balance.get();
            log.debug(String.valueOf(prev==prev1));
            System.out.println(prev1);
            prev1.setDesc("保洁安逸修改");
         log.debug(String.valueOf(balance.compareAndSet(prev1, new GarbageBag("另外的"))));
        },"保洁阿姨").start();
        Sleeper.sleep(2);
        System.out.println(prev);
        prev.setDesc("已经改过来");
        System.out.println(balance.compareAndSet(prev, prev));
        System.out.println(prev);


    }
}

interface DecimalAccount {
    // 获取余额
    BigDecimal getBalance();

    // 取款
    void withdraw(BigDecimal amount);

    /**
     * 方法内会启动 1000 个线程，每个线程做 -10 元 的操作
     * 如果初始余额为 10000 那么正确的结果应当是 0
     */
    static void demo(DecimalAccount account) {
        List<Thread> ts = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            ts.add(new Thread(() -> {
                account.withdraw(BigDecimal.TEN);
            }));
        }
        ts.forEach(Thread::start);
        ts.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println(account.getBalance());
    }


}


