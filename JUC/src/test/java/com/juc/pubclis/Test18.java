package com.juc.pubclis;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.Test18")
public class Test18 {
    static final Object lock = new Object();
    public static void main(String[] args) {

        new Thread(()->{
            log.debug("开始睡眠");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.debug("睡眠结束开始阻塞");
            synchronized (lock) {
                try {

                    lock.wait();
                    log.debug("阻塞恢复");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        },"t1").start();

        new Thread(()->{
            synchronized (lock) {
                log.debug("换醒啦所以线程");
                lock.notifyAll();

            }
        },"t2").start();

        new Thread(()->{
            try {
                Thread.sleep(7000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lock) {
               log.debug("最后唤醒");
                lock.notifyAll();

            }
        },"t3").start();

    }
}
