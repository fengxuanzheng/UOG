package com.juc.pubclis;

public class AutoIncre {

    private static int i=1;

    public static int incore()
    {
        return ++i;
    }

    public static void main(String[] args) {
        System.out.println(AutoIncre.incore());
    }
}
