package com.juc.pubclis;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

public class Tests {

    @Test
    public void test()
    {
        HashMap<String, PersonA> stringPersonAHashMap = new HashMap<>();
        stringPersonAHashMap.put("a",new PersonA("欧尼酱",25));
        stringPersonAHashMap.put("b",new PersonA("七海",16));
        System.out.println(stringPersonAHashMap);
        PersonA a = stringPersonAHashMap.get("a");
        a.setAge(18);
        a.setName("欧尼酱偶");
        System.out.println(stringPersonAHashMap);
    }
}
