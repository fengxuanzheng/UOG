package com.juc;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.LockSupport;

import static java.lang.Thread.sleep;

@Slf4j
public class Hello {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
          /* log.info("线程一睡眠");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("重新起床了");*/

            while (true)
            {
                log.debug("runing_________");
                boolean interrupted = Thread.currentThread().isInterrupted();
                log.debug(String.valueOf(interrupted));
            }
        });
        thread.start();
        log.info("主线程休眠");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //log.info("主线程叫你起床");
        log.info("主线程打断");

        thread.interrupt();

    }
  @Test
  public void test2() throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                log.debug("park...");
                Thread.interrupted();
                log.debug("打断状态：{}", Thread.currentThread().isInterrupted());
                LockSupport.park();
                log.debug("打断状态：{}", Thread.currentThread().isInterrupted());
            }
        });
        t1.start();
        log.debug("开始");
        sleep(1000);
        t1.interrupt();
        Thread.sleep(100000);
    }
}
