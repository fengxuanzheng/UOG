package com.juc.theres;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class Hello {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            Thread thread1 = Thread.currentThread();
            try {
                new ReentrantLock().newCondition().await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.debug("开始睡眠");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                log.debug("睡眠被打断");
                e.printStackTrace();
            }
            log.debug("打断后恢复运行");
        });
        thread.start();

        Thread.sleep(2000);
        log.debug("主线程打断");
       // thread.interrupt();

    }
}
