package com.juc;

import static java.lang.Thread.sleep;

/**
 * @Author:圣代桥冰织
 * @CreateTime:2023-07-03 18:04:16
 */
public class Test11 {
    static boolean run = true;
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            while(run){
                try {
                    sleep(1500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t.start();
        sleep(1000);
        run = false; // 线程t不会如预想的停下来
    }
}
