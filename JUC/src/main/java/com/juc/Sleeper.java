package com.juc;

public class Sleeper {

    public static void sleep(long time)
    {
        try {
            Thread.sleep(time*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
