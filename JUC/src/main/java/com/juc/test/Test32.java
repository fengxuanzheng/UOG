package com.juc.test;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.YearMonth;

import static com.juc.n2.util.Sleeper.sleep;


@Slf4j(topic = "c.Test32")
public class Test32 implements Serializable {
    // 易变
    static boolean run = true;

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            while(true){
                    if(!run) {
                        break;
                    }
            }
        });
        t.start();

        sleep(1);
            run = false; // 线程t不会如预想的停下来

        YearMonth now = YearMonth.now();
        System.out.println(now.atEndOfMonth());
        LocalDate of = LocalDate.of(2022, 1, 2);
        LocalDate of1 = LocalDate.of(2022, 2, 3);
        Period between = Period.between(of, of1);
        System.out.println(between.getDays());
        Duration duration = Duration.between(of.atStartOfDay(), of1.atStartOfDay());
        System.out.println(duration.toDays());
    }
    public Object readResole()
    {
        return null;
    }
}
