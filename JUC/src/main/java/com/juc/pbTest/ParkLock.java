package com.juc.pbTest;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

@Slf4j
public class ParkLock {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {

                log.debug("start...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                log.debug("park...");
                LockSupport.park();
            synchronized (ParkLock.class) {
                log.debug("resume...");
            }
        },"t1");
        t1.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        synchronized (ParkLock.class) {
            log.debug("unpark...");
            LockSupport.unpark(t1);
        }
    }
}
