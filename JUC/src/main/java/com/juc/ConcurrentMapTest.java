package com.juc;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentMapTest {

    public static void main(String[] args) {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        new Thread(
                ()->{

                    concurrentHashMap.put("ss","dd");
                }
        ).start();
        new Thread(
                ()->{
                    concurrentHashMap.put("ss","dd");
                }
        ).start();
    }
}
