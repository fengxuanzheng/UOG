package com.atguigu.kafka.service;

import org.apache.kafka.clients.admin.*;
import org.apache.kafka.common.KafkaFuture;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class AdminConfig {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Properties properties = new Properties();
        properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "172.20.247.207:9092");
        Admin admin = Admin.create(properties);
        NewTopic seeks = new NewTopic("fenq", 2, (short) 1);
        CreateTopicsResult topics = admin.createTopics(List.of(seeks));
        Map<String, KafkaFuture<Void>> values = topics.values();
        values.forEach((key,value)->{
            try {
                Void unused = value.get();
                System.out.println(key);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        });
        ListTopicsResult listTopicsResult = admin.listTopics();
        KafkaFuture<Collection<TopicListing>> listings = listTopicsResult.listings();
        for (TopicListing topicListing : listings.get()) {
            System.out.println(topicListing.name());
        }


    }
}
