package com.atguigu.kafka.consumer;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

public class CustomConsumerByHandSync {

    public static void main(String[] args) {

        // 0 配置
        Properties properties = new Properties();

        // 连接 bootstrap.servers
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"43.139.36.8:9092");

        // 反序列化
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // 配置消费者组id
        properties.put(ConsumerConfig.GROUP_ID_CONFIG,"test");

        // 手动提交
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,false);

        // 1 创建一个消费者  "", "hello"
        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties);

        // 2 订阅主题 first
        ArrayList<String> topics = new ArrayList<>();
        topics.add("first");
        kafkaConsumer.subscribe(topics);

        // 3 消费数据
        while (true){

            ConsumerRecords<String, String> consumerRecords = kafkaConsumer.poll(Duration.ofSeconds(1));

            for (ConsumerRecord<String, String> consumerRecord : consumerRecords) {
                System.out.println(consumerRecord);
            }

            // 手动提交offset//
            //kafkaConsumer.commitSync();
            kafkaConsumer.commitAsync((Map< TopicPartition, OffsetAndMetadata > offsets, Exception exception)->{
                if (exception == null) {
                    for (Map.Entry<TopicPartition, OffsetAndMetadata> topicPartitionOffsetAndMetadataEntry : offsets.entrySet()) {
                        TopicPartition key = topicPartitionOffsetAndMetadataEntry.getKey();
                        OffsetAndMetadata value = topicPartitionOffsetAndMetadataEntry.getValue();
                        System.out.printf("主题:{},分区:{};分区元数据:{}---{}",key.topic(),key.partition(),value.metadata(),value.offset());
                    }


                }
            });
            //使用偏移量精准提交
           /* Map currentOffsets = new HashMap<>();
            int count = 0;
            while (true) {
                ConsumerRecords records = kafkaConsumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord record : records) {
                    System.out.println(record.value());// 偏移量加1
                    currentOffsets.put(new TopicPartition(record.topic(), record.partition()), new OffsetAndMetadata(record.offset() + 1));
                    if (count % 1000 == 0) {
                        kafkaConsumer.commitAsync(currentOffsets, null);
                    }        count++;    }}*/
        }
    }
}
