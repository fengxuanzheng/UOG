package com.mybatisPlus.controller;


import com.mybatisPlus.pojo.Zone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 魔法少女
 * @since 2021-06-02
 */
@Controller
//@RequestMapping("/tbl_employee")
public class Tbl_employeeController {

    @Autowired
    private Zone zone;

    @GetMapping("/tests")
    @ResponseBody
    public Zone getZone()
    {
        System.out.println(zone.getClass());
        return zone;
    }
}

