package com.mybatisPlus.juc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Component
public class TestAsyncBean {
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Async
    public Future<String> sayHello1() throws InterruptedException {
        System.out.println(Thread.currentThread().getName());
        int thinking = 2;
        //Thread.sleep(thinking * 1000);//网络连接中 。。。消息发送中。。。
        System.out.println("我爱你啊!");
        return new AsyncResult<String>("发送消息用了"+thinking+"秒");
    }


    public void sayHello2() throws InterruptedException, ExecutionException {
         threadPoolTaskExecutor.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
                int thinking = 2;
                //Thread.sleep(thinking * 1000);//网络连接中 。。。消息发送中。。。
                System.out.println("我爱你啊!");

            }
        });


    }
}
