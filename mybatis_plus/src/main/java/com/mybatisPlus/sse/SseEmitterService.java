package com.mybatisPlus.sse;

import com.mybatisPlus.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SseEmitterService {
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    private static Integer integer=0;
    @Async
    public void sends(String id, ConcurrentHashMap<String,SseEmitter> concurrentHashMap)
    {
        System.out.println(threadPoolTaskExecutor.getActiveCount());
        System.out.println(Thread.currentThread().getName());
        Employee employee = new Employee();
        employee.setEmail("自定义邮箱");
        employee.setAge(12);
        employee.setGender('0');
        SseEmitter sseEmitter = concurrentHashMap.get(id);
        try {
            sseEmitter.send(SseEmitter.event().id(Integer.toString(integer)).data(employee));
            //sseEmitter.send(SseEmitter.event().id(Integer.toString(integer)).name("message").data(employee));
            integer+=1;
            //sseEmitter.send(employee, MediaType.APPLICATION_JSON);

        } catch (IOException e) {
            e.printStackTrace();
            sseEmitter.completeWithError(e);
        }
    }


}
