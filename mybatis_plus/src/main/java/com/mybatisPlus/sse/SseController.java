package com.mybatisPlus.sse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@Slf4j
public class SseController {
    @Autowired
    @Qualifier("poolTaskExecutor")
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    private ConcurrentHashMap<String,SseEmitter> concurrentHashMap=new ConcurrentHashMap<>();
    @Autowired
    private SseEmitterService sseEmitterService;

    private List<DeferredResult<String>> deferredResultList = new ArrayList<>();
    @RequestMapping(value="/push",produces="text/event-stream;charset=utf-8")
    @ResponseBody
    public String push() {
        Random r = new Random();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "data:后端返回数据" + r.nextInt() +"\n\n";
    }
    @RequestMapping(value = "/sseTest", method = RequestMethod.GET)
    public String getSSEView (HttpServletRequest httpServletRequest)
    {
        AsyncContext asyncContext = httpServletRequest.startAsync();
        return "sseTest";
    }

   @RequestMapping(value = "/newsseemitter/{clientid}")
   public SseEmitter newgetSseEmitter(@PathVariable(value = "clientid",required = false) String id) throws IOException, InterruptedException, ExecutionException {
       Future<SseEmitter> submit = threadPoolTaskExecutor.submit(() -> {
           log.info("客户端开始连接");
           System.out.println(threadPoolTaskExecutor.getActiveCount());
           System.out.println(Thread.currentThread().getName());
           if (!concurrentHashMap.containsKey(id)) {
               log.info("新建连接对象");
               SseEmitter sseEmitter = new SseEmitter(0L);
               System.out.println(Thread.currentThread().getName());

               sseEmitter.onCompletion(() -> {
                   System.out.println("异步返回运行结束");
                   concurrentHashMap.remove(id);
               });
               sseEmitter.onError((throwable) -> {
                   System.out.println("发送错误异常:" + throwable);
               });
               sseEmitter.onTimeout(() -> {
                   System.out.println("连接超时");

                   concurrentHashMap.remove(id);
               });

               concurrentHashMap.put(id, sseEmitter);
               return sseEmitter;
           } else {
               return concurrentHashMap.get(id);
           }
       });
       return submit.get();

   }

    @RequestMapping("/send/{clientid}")
    public void sen(@PathVariable("clientid") String id)
    {
        System.out.println("开始"+Thread.currentThread().getName());
        sseEmitterService.sends(id,concurrentHashMap);
        System.out.println("结束"+Thread.currentThread().getName());
    }

    @RequestMapping ("/close/{clientid}")
    public void close(@PathVariable("clientid") String id)
    {
        System.out.println("调用关闭方法");
        SseEmitter sseEmitter = concurrentHashMap.get(id);
        sseEmitter.complete();
        concurrentHashMap.remove(id);
    }


    //****************************************************************************************

    @GetMapping(value = "/email/send")
    public void servletReq(HttpServletRequest request) {
        AsyncContext asyncContext = request.startAsync();
        // 设置监听器:可设置其开始、完成、异常、超时等事件的回调处理
        asyncContext.addListener(new AsyncListener() {
            @Override
            public void onTimeout(AsyncEvent event) {
                System.out.println("处理超时了...");
            }

            @Override
            public void onStartAsync(AsyncEvent event) {
                System.out.println("线程开始执行");
            }

            @Override
            public void onError(AsyncEvent event) {
                System.out.println("执行过程中发生错误：" + event.getThrowable().getMessage());
            }

            @Override
            public void onComplete(AsyncEvent event) {
                System.out.println("执行完成，释放资源");
            }
        });
        //设置超时时间
        asyncContext.setTimeout(6000);
        asyncContext.start(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    System.out.println("内部线程：" + Thread.currentThread().getName());
                    asyncContext.getResponse().getWriter().println("async processing");
                } catch (Exception e) {
                    System.out.println("异步处理发生异常：" + e.getMessage());
                }
                // 异步请求完成通知，整个请求完成
               // asyncContext.complete();
            }
        });
        //此时request的线程连接已经释放了
        System.out.println("主线程：" + Thread.currentThread().getName());
    }

    @GetMapping("/hello")
    public DeferredResult<String> helloGet() throws Exception {
        DeferredResult<String> deferredResult = new DeferredResult<>();

        //先存起来，等待触发
        deferredResultList.add(deferredResult);
        return deferredResult;
    }

    @ResponseBody
    @GetMapping("/setHelloToAll")
    public void helloSet(HttpServletResponse httpServletResponse) throws Exception {
        // 让所有hold住的请求给与响应
        log.info("调用此方法:"+"helloSet");
        DeferredResult<String> stringDeferredResult = deferredResultList.get(0);
        stringDeferredResult.setResult("正确响应");

    }

    @GetMapping("/quotes")
    public DeferredResult<String> quotes() {
        //指定超时时间，及出错时返回的值
        DeferredResult<String> result = new DeferredResult(0L,"error");
        deferredResultList.add(result);
        return result;
    }

    /**
     * 另外一个请求(新的线程)设置值
     *
     * @throws InterruptedException
     */

    @GetMapping("take")
    public void take() throws InterruptedException {
        deferredResultList.forEach((value)->{
            value.setResult("route");
        });

    }

    @GetMapping("/email")
    public Callable<String> order() {
        System.out.println("主线程开始：" + Thread.currentThread().getName());
        Callable<String> result = () -> {
            System.out.println("副线程开始：" + Thread.currentThread().getName());
            Thread.sleep(1000);
            System.out.println("副线程返回：" + Thread.currentThread().getName());
            return "success";
        };

        System.out.println("主线程返回：" + Thread.currentThread().getName());
        return result;
    }
    @RequestMapping("/gettime")
    public String gettime()
    {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String format = dateTimeFormatter.format(now);
        return format;
    }





}
