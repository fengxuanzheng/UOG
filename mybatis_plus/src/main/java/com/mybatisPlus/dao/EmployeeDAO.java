package com.mybatisPlus.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mybatisPlus.pojo.Employee;

import java.util.List;
//基于MP实现接口只需要继承BaseMapper接口就可以,所继承的接口泛型就算指定当前Mapper接口所要操作的实体类类型
/*
MP基本原理:
SqlSessionFactory > Configuration >MapprdStatements
每一个MapprdStatements都表示Mapper接口中的一个方法与Mapper映射文件中的一个SQL
MP启动时候会挨个分析XXXXMapper中的方法,并将对于SQL语句处理好,保存在Configuration对象的MapprdStatements中
部分核心细节:
通过MapperBuilderAssistant将每一个mappedStatement对象添加到configuration中的mappendstatements.
MapperBuilderAssistant是用于缓存,SQL参数,查询结果集处理等
*/

/*
对于元对象:metaobject,
是mybatis提供的一个更加方便更加优雅访问对象属性,给对象属性设置值的一个对象,还会包装对象,支持
对Object Map Collection等对象进行包装
本质上获取对象属性值或设置对象属性值,最终通过反射获取到属性对应的方法的invoker
*/
public interface EmployeeDAO extends  BaseMapper<Employee>{

    public List<Employee> getEmployee();
    public List<Employee> getpageEmployee();



}
