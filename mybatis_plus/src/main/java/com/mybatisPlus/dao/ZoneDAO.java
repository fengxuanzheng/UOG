package com.mybatisPlus.dao;

import com.mybatisPlus.pojo.Zone;

import java.util.List;
import java.util.Map;

public interface ZoneDAO {

    public Zone getZone(Zone zone);
    public List<List<Zone>> getzonelist(Map<String,Object> intoZone);
    public List<List<Zone>> getzonelistForDay(Map<String,Object> intoZone);
}
