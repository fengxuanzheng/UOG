package com.mybatisPlus.dao;

import com.mybatisPlus.pojo.Tbl_employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 魔法少女
 * @since 2021-06-02
 */
public interface Tbl_employeeMapper extends BaseMapper<Tbl_employee> {

}
