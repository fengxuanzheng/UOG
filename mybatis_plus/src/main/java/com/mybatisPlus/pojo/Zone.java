package com.mybatisPlus.pojo;


import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class Zone {

  private Integer id;
  private Integer eid;
  private String tName;
  private Integer tValue;
  private Date tTime;
  private Date startTime;
  private Integer evalue;

  public Zone(Integer id, Integer eid, String tName, Integer tValue, Date tTime, Date startTime, Integer evalue) {
    this.id = id;
    this.eid = eid;
    this.tName = tName;
    this.tValue = tValue;
    this.tTime = tTime;
    this.startTime = startTime;
    this.evalue = evalue;
  }

  public Zone() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getEid() {
    return eid;
  }

  public void setEid(Integer eid) {
    this.eid = eid;
  }

  public String gettName() {
    return tName;
  }

  public void settName(String tName) {
    this.tName = tName;
  }

  public Integer gettValue() {
    return tValue;
  }

  public void settValue(Integer tValue) {
    this.tValue = tValue;
  }

  public Date gettTime() {
    return tTime;
  }

  public void settTime(Date tTime) {
    this.tTime = tTime;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Integer getEvalue() {
    return evalue;
  }

  public void setEvalue(Integer evalue) {
    this.evalue = evalue;
  }

  @Override
  public String toString() {
    return "Zone{" +
            "id=" + id +
            ", eid=" + eid +
            ", tName='" + tName + '\'' +
            ", tValue=" + tValue +
            ", tTime=" + tTime +
            ", startTime=" + startTime +
            ", evalue=" + evalue +
            '}';
  }
}
