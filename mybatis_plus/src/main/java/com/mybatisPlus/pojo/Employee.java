package com.mybatisPlus.pojo;


import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import org.springframework.stereotype.Component;

//默认情况下mybatisplus默认会使用实体类类名到数据库中找对于表
//聚成MODEL抽象类就可以实现AR,最新版本已经不用实现pkVal()方法,老版本这个方法要返回当前实体类的主键属性
@Component
@TableName(value = "tbl_employee")
public class Employee extends Model<Employee> {

  //@TableId:
  //value:指定表中的主键列列名,如果实体属性名和列名一直,可以省略不指定
  //指定主键策略
  @TableId(type = IdType.AUTO)
  private Integer id;
  @TableField(value = "last_name")
  private String lastName;
  @TableField(fill = FieldFill.INSERT_UPDATE)
  private String email;
  private Character gender;
  private Integer age;

  public Employee(Integer id, String lastName, String email, Character gender, Integer age) {
    this.id = id;
    this.lastName = lastName;
    this.email = email;
    this.gender = gender;
    this.age = age;
  }

  public Employee() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public Character getGender() {
    return gender;
  }

  public void setGender(Character gender) {
    this.gender = gender;
  }


  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "Employee{" +
            "id=" + id +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", gender=" + gender +
            ", age=" + age +
            '}';
  }
}
