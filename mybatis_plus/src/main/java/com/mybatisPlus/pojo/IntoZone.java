package com.mybatisPlus.pojo;

import java.util.Date;

public class IntoZone {
    private Date starttime;
    private Integer addtime;
    private Integer eid;
    private Integer sizes;
    private String addtimetype;
    private Date nulltime;

    public IntoZone(Date starttime, Integer addtime, Integer eid, Integer sizes, String addtimetype, Date nulltime) {
        this.starttime = starttime;
        this.addtime = addtime;
        this.eid = eid;
        this.sizes = sizes;
        this.addtimetype = addtimetype;
        this.nulltime = nulltime;
    }

    public IntoZone() {
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Integer getAddtime() {
        return addtime;
    }

    public void setAddtime(Integer addtime) {
        this.addtime = addtime;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Integer getSizes() {
        return sizes;
    }

    public void setSizes(Integer sizes) {
        this.sizes = sizes;
    }

    public String getAddtimetype() {
        return addtimetype;
    }

    public void setAddtimetype(String addtimetype) {
        this.addtimetype = addtimetype;
    }

    public Date getNulltime() {
        return nulltime;
    }

    public void setNulltime(Date nulltime) {
        this.nulltime = nulltime;
    }

    @Override
    public String toString() {
        return "IntoZone{" +
                "starttime=" + starttime +
                ", addtime=" + addtime +
                ", eid=" + eid +
                ", sizes=" + sizes +
                ", addtimetype='" + addtimetype + '\'' +
                ", nulltime=" + nulltime +
                '}';
    }
}
