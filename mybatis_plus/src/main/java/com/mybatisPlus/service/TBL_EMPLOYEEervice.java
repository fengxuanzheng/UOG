package com.mybatisPlus.service;

import com.mybatisPlus.pojo.Tbl_employee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 魔法少女
 * @since 2021-06-02
 */
public interface TBL_EMPLOYEEervice extends IService<Tbl_employee> {

}
