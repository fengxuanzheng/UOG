package com.mybatisPlus.service.impl;

import com.mybatisPlus.pojo.Tbl_employee;
import com.mybatisPlus.dao.Tbl_employeeMapper;
import com.mybatisPlus.service.TBL_EMPLOYEEervice;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 魔法少女
 * @since 2021-06-02
 */
@Service
public class Tbl_employeeServiceImpl extends ServiceImpl<Tbl_employeeMapper, Tbl_employee> implements TBL_EMPLOYEEervice {



}
