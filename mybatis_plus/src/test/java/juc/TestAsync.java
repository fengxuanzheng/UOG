package juc;

import com.mybatisPlus.juc.TestAsyncBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.concurrent.ExecutionException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/applicationContext.xml"})
public class TestAsync {

    @Autowired
    private TestAsyncBean testAsyncBean;
    @Test
    public void test_sayHello2() throws InterruptedException, ExecutionException {
        System.out.println(Thread.currentThread().getName()+Thread.currentThread().getId());
        System.out.println("你不爱我了么?");
        System.out.println("异步方法2"+testAsyncBean.sayHello1().get());
        System.out.println("异步方法1"+testAsyncBean.sayHello1().get());
        System.out.println("你说的啥? 我们还是分手吧。。。");
        System.out.println(Thread.currentThread().getName()+Thread.currentThread().getId());


        //Thread.sleep(3 * 1000);// 不让主进程过早结束
    }

    @Test
    public void test_sayHello3() throws InterruptedException, ExecutionException {
        System.out.println(Thread.currentThread().getName()+Thread.currentThread().getId());

        System.out.println("你不爱我了么?");
        testAsyncBean.sayHello2();
        System.out.println("你说的啥? 我们还是分手吧。。。");
       // System.out.println(Thread.currentThread().getName()+Thread.currentThread().getId());


        System.out.println("主线程结束");

        //Thread.sleep(3 * 1000);// 不让主进程过早结束
    }
    @Test
    public void test() {
        Date date = new Date();

        System.out.println(date.getTime());

    }

}
