package mybatisPlus;


import com.mybatisPlus.dao.EmployeeDAO;
import com.mybatisPlus.pojo.Employee;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class PublictTest {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;


    @Test
    public void test(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        EmployeeDAO mapper = sqlSession.getMapper(EmployeeDAO.class);
        List<Employee> employee = mapper.getEmployee();
        System.out.println("第一次:         "+employee);
        List<Employee> employee1 = mapper.getEmployee();
        System.out.println("第二次:         "+employee1);

        sqlSession.close();
        SqlSession sqlSession1 = sqlSessionFactory.openSession();
        EmployeeDAO mapper1 = sqlSession1.getMapper(EmployeeDAO.class);
        List<Employee> employees = mapper1.getpageEmployee();
        List<Employee> employees2 = mapper1.getpageEmployee();
        List<Employee> employees3 = mapper1.getpageEmployee();
        List<Employee> employees4 = mapper1.getpageEmployee();
        System.out.println("第三次:         "+employees);
        System.out.println("第四次:         "+employees2);
        System.out.println("第五次:         "+employees3);
        System.out.println("第六次:         "+employees4);
        sqlSession1.close();
    }
}
