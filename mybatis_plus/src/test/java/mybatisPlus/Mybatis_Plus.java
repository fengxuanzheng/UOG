package mybatisPlus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mybatisPlus.dao.EmployeeDAO;
import com.mybatisPlus.dao.ZoneDAO;
import com.mybatisPlus.pojo.Employee;
import com.mybatisPlus.pojo.Zone;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

public class Mybatis_Plus {

    private ApplicationContext applicationContext;


    @Before
    public void beforeinit()
    {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        applicationContext=classPathXmlApplicationContext;
    }

    @Test
    public void test() {


        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        List<Employee> employee = bean.getEmployee();
        List<Employee> employees = bean.getpageEmployee();
        System.out.println(employee.getClass());
        System.out.println(employee);

    }
    @Test
    //insert,在使用使用默认进行动态SQL根据插入对象属性判断插入到数据库字段,如果想插入全部就使用insertAllColumn
    public void test2() {
        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        Employee employee = new Employee(null, "mmee", "dggrr", '0', 18);
        int insert = bean.insert(employee);
        System.out.println(insert);

    }
    @Test
    public void test3() {
        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        Employee employee = new Employee(1, "七海", "dggrr", '1', 14);
        System.out.println(bean.updateById(employee));


    }
    @Test
    public void test4() {
        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        Employee employee = bean.selectById(1);
        System.out.println(employee);

    }
    @Test
    public void test5() {
        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        List<Employee> employees = bean.selectBatchIds(integers);
        System.out.println(employees);

    }

    @Test
    public void test6() {
        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        QueryWrapper<Employee> employeeQueryWrapper = new QueryWrapper<>();
        employeeQueryWrapper.between("age",5,9);
        Page page = bean.selectMapsPage(new Page(1, 2), employeeQueryWrapper);
        List records = page.getRecords();
        System.out.println(records);
        System.out.println(employeeQueryWrapper.getTargetSql());
        System.out.println(employeeQueryWrapper.getSqlComment());
        System.out.println(employeeQueryWrapper.getSqlSelect());
        System.out.println(employeeQueryWrapper.getSqlSegment());
        System.out.println(employeeQueryWrapper.getCustomSqlSegment());
        System.out.println(employeeQueryWrapper.getParamNameValuePairs());
    }

    //***********************************************************************
    //以下是AR操作
    @Test
    public void test7() {
        Employee employee = new Employee(null,"在原七海","fsefes",'1',15);
        System.out.println(employee.insert());

    }
    @Test
    public void test8() {
        Employee employee = new Employee(9,"在原七海","761210140",'1',15);
        System.out.println(employee.updateById());

    }
    @Test
    public void test9() {
        Employee bean = applicationContext.getBean(Employee.class);
        System.out.println(bean.deleteById(1));

    }

    //************************************************
    //以下是分页测试
    @Test
    public void test10() {
        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        Page<Map<String, Object>> mapPage = bean.selectMapsPage(new Page<>(1, 1), null);
        System.out.println(mapPage.getRecords());

    }
    @Test
    public void test11() {
        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        Page<Employee> employeePage = new Page<>(1,2);
        List<Employee> employees = bean.getpageEmployee();
       // List<Employee> records = employeeIPage.getRecords();
        System.out.println(employees);
        System.out.println(employeePage.getRecords());
        System.out.println(employeePage.getSize());
        System.out.println(employeePage.getTotal());


    }

    //************************************************************************
    //以下是自动填充
    @Test
    public void test12() {
        EmployeeDAO bean = applicationContext.getBean(EmployeeDAO.class);
        Employee employee = new Employee();
        System.out.println(bean.insert(employee));

    }
    //***********************************************************
    //非练习
    @Test
    public void test13() {
        ZoneDAO bean = applicationContext.getBean(ZoneDAO.class);
        Zone zone = new Zone();
        zone.setId(1);
        zone.setEid(1);
        zone.settTime(new Date());
        //zone.setStartTime(new Date());
        //zone.setEvalue(22);
          bean.getZone(zone);
        System.out.println(zone);
        Zone zone1=zone;
          bean.getZone(zone1);
        System.out.println(zone1);
    }
    @Test
    public void test14() {
    /*    ZoneDAO bean = applicationContext.getBean(ZoneDAO.class);
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("starttime",new Date());
        stringObjectHashMap.put("eid",1);
        stringObjectHashMap.put("sizes",1);
        stringObjectHashMap.put("addtime",1);
        long tarttime=System.currentTimeMillis();
        List<Zone> getzonelist = bean.getzonelist(stringObjectHashMap);
        long end=System.currentTimeMillis()-tarttime;
        System.out.println(end);
        System.out.println(getzonelist);*/
    }
    @Test
    public void test15() {
        /*SqlSessionFactory bean1 = applicationContext.getBean(SqlSessionFactory.class);
        SqlSession sqlSession = bean1.openSession(ExecutorType.BATCH);
        ZoneDAO mapper = sqlSession.getMapper(ZoneDAO.class);
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("starttime",new Date());
        stringObjectHashMap.put("eid",1);
        stringObjectHashMap.put("sizes",1);
        stringObjectHashMap.put("addtime",1);
        //stringObjectHashMap.put("nulltime",null);
        ArrayList<List<Zone>> zones = new ArrayList<List<Zone>>();
        long tarttime=System.currentTimeMillis();
        for (int i=0;i<15;i++)
        {
        List<List<?>> getzonelist = mapper.getzonelist(stringObjectHashMap);
            System.out.println(stringObjectHashMap);
            List<?> objects = getzonelist.get(getzonelist.size() - 1);
            Zone zone=null;
            ArrayList arrayList=null;
            Object zoneobj=null;
            if (objects.isEmpty())
            {
                for (int j=2;j<getzonelist.size();j++)
                {
                    objects= getzonelist.get(getzonelist.size()-j);
                    zoneobj =  objects.get(0);

                    if (zoneobj instanceof Zone)
                    {
                        zone=(Zone) zoneobj;
                    }
                    else
                    {
                        arrayList=(ArrayList) zoneobj;
                    }

                }
            }
            else
            {
                 zoneobj = objects.get(0);
                if (zoneobj instanceof Zone)
                {
                    zone=(Zone) zoneobj;
                }
                else
                {
                    arrayList=(ArrayList) zoneobj;
                }

            }
            stringObjectHashMap.put("starttime",zone.gettTime());
            zones.addAll(getzonelist);
            List<List<Zone>> getzonelist1 = mapper.getzonelist(stringObjectHashMap);
            zones.addAll(getzonelist1);
            Zone zone1=null;
            List<Zone> zones1 = getzonelist1.get(getzonelist1.size() - 1);
            if (zones1.isEmpty())
            {
                for (int k=2;k<getzonelist1.size();k++)
                {
                    System.out.println(stringObjectHashMap);
                    zones1= getzonelist1.get(getzonelist1.size()-k);
                    zone1 = zones1.get(0);
                }
            }
            else
            {
                zone1 = zones1.get(0);
            }
            stringObjectHashMap.put("starttime",zone1.gettTime());
        }
        long end=System.currentTimeMillis()-tarttime;
        System.out.println(end);
        System.out.println(zones);
        System.out.println(zones.size());
        System.out.println(stringObjectHashMap);
        Date date = zones.get(zones.size() - 1).get(0).gettTime();
        DateFormat dateTimeInstance = DateFormat.getDateTimeInstance(1, 2, new Locale("zh", "CN"));
        System.out.println(dateTimeInstance.format(date));*/

    }
    @Test
    public void test16() {
      /*  SqlSessionFactory bean1 = applicationContext.getBean(SqlSessionFactory.class);
        SqlSession sqlSession = bean1.openSession(ExecutorType.BATCH);
        ZoneDAO mapper = sqlSession.getMapper(ZoneDAO.class);
        IntoZone intoZone = new IntoZone();
        ArrayList<List<Zone>> zones = new ArrayList<List<Zone>>();
        intoZone.setAddtime(1);
        intoZone.setEid(1);
        intoZone.setSizes(1);
        intoZone.setStarttime(new Date());
        long tarttime=System.currentTimeMillis();
        for (int i=0;i<15;i++)
        {
            List<List<Zone>> getzonelist = mapper.getzonelist(intoZone);
            List<Zone> objects = getzonelist.get(getzonelist.size() - 1);
            Zone zone=null;
            if (objects.isEmpty())
            {
                for (int j=2;j<getzonelist.size();j++)
                {
                    objects= getzonelist.get(getzonelist.size()-j);
                    zone = objects.get(0);
                }
            }
            else
            {
                zone = objects.get(0);
            }
            intoZone.setStarttime(zone.gettTime());
            zones.addAll(getzonelist);
            List<List<Zone>> getzonelist1 = mapper.getzonelist(intoZone);
            zones.addAll(getzonelist1);
            Zone zone1=null;
            List<Zone> zones1 = getzonelist1.get(getzonelist1.size() - 1);
            if (zones1.isEmpty())
            {
                for (int k=2;k<getzonelist1.size();k++)
                {
                    System.out.println(intoZone);
                    zones1= getzonelist1.get(getzonelist1.size()-k);
                    zone1 = zones1.get(0);
                }
            }
            else
            {
                zone1 = zones1.get(0);
            }
            intoZone.setStarttime(zone1.gettTime());
        }
        long end=System.currentTimeMillis()-tarttime;
        System.out.println(end);
        System.out.println(zones);
        System.out.println(zones.size());
        Date date = zones.get(zones.size() - 1).get(0).gettTime();
        DateFormat dateTimeInstance = DateFormat.getDateTimeInstance(1, 2, new Locale("zh", "CN"));
        System.out.println(dateTimeInstance.format(date));
*/
    }
    @Test
    public void finall() throws ParseException {

        DateFormat dateTimeInstance = DateFormat.getDateTimeInstance(1, 2, new Locale("zh", "CN"));
        Date parse = dateTimeInstance.parse("2021年4月7日 下午6:00:33");
        SqlSessionFactory bean1 = applicationContext.getBean(SqlSessionFactory.class);
        SqlSession sqlSession = bean1.openSession(ExecutorType.BATCH);
        ZoneDAO mapper = sqlSession.getMapper(ZoneDAO.class);
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("starttime", new Date());
        stringObjectHashMap.put("eid", 1);
        stringObjectHashMap.put("sizes", 1);
        stringObjectHashMap.put("addtime", 1);
        ArrayList<Zone> retrunzone = new ArrayList<>();
        long tarttime = System.currentTimeMillis();
        for (int i = 0; i < 30; i++)
        {
            for (int j = 0; j < 2; j++) {
                //System.out.println("输出:" + "外层:" + i + "   " + "内层:" + j);
                long l = System.currentTimeMillis();
                List<List<Zone>> getzonelist = mapper.getzonelist(stringObjectHashMap);
                System.out.println(System.currentTimeMillis()-l);
                Zone zone=null;
                if (getzonelist.size()==1)
                {
                    zone =(Zone) getzonelist.get(0);
                    retrunzone.add(zone);
                }
                else {
                    List<Zone> objects = getzonelist.get(getzonelist.size() - 1);
                    zone = objects.get(0);
                    for (int g = 0; g < getzonelist.size(); g++) {
                        retrunzone.add(getzonelist.get(g).get(0));
                    }
                }


                    if (getzonelist!=null&&getzonelist.size()!=1)
                    {
                        stringObjectHashMap.put("starttime", zone.gettTime());
                    }
                    else
                    {

                        Date nulltime =(Date) stringObjectHashMap.get("nulltime");
                        stringObjectHashMap.put("starttime", nulltime);
                    }


            }


        }
        long end = System.currentTimeMillis() - tarttime;
        System.out.println(end);
        System.out.println(stringObjectHashMap);
       // System.out.println(retrunzone);
        Date date = retrunzone.get(retrunzone.size() - 1).gettTime();
        System.out.println(dateTimeInstance.format(date));

    }

    @Test
    public void test18() throws ParseException {
        DateFormat dateTimeInstance = DateFormat.getDateTimeInstance(1, 2, new Locale("zh", "CN"));
        Date parse = dateTimeInstance.parse("2021年4月7日 下午6:00:33");
        System.out.println(parse);

    }

    @Test
    public void test19() throws ParseException {
        DateFormat dateTimeInstance = DateFormat.getDateTimeInstance(1, 2, new Locale("zh", "CN"));
        Date parse = dateTimeInstance.parse("2021年4月7日 下午6:00:33");
        SqlSessionFactory bean1 = applicationContext.getBean(SqlSessionFactory.class);
        SqlSession sqlSession = bean1.openSession(ExecutorType.BATCH);
        ZoneDAO mapper = sqlSession.getMapper(ZoneDAO.class);
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("starttime", new Date());
        stringObjectHashMap.put("eid", 1);
        stringObjectHashMap.put("sizes", 3);
        stringObjectHashMap.put("addtime", 1);
        ArrayList<List<Zone>> retrunzone = new ArrayList<>();
        long tarttime = System.currentTimeMillis();
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 1; j++) {
                //System.out.println("输出:" + "外层:" + i + "   " + "内层:" + j);
                long l = System.currentTimeMillis();
                List<List<Zone>> getzonelist = mapper.getzonelistForDay(stringObjectHashMap);
                System.out.println(System.currentTimeMillis()-l);
                Zone zone=null;
                if (getzonelist.size()==1)
                {
                    zone =(Zone) getzonelist.get(0);
                    ArrayList<Zone> zones = new ArrayList<>();
                    zones.add(zone);
                    retrunzone.add(zones);
                }
                else {
                    List<Zone> objects = getzonelist.get(getzonelist.size() - 1);
                    zone = objects.get(0);

                        retrunzone.addAll(getzonelist);

                }


                if (getzonelist!=null&&getzonelist.size()!=1)
                {
                    Date date = zone.gettTime();
                    Instant instant = date.toInstant();
                    ZoneId zoneId = ZoneId.systemDefault();
                    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zoneId);
                    LocalDateTime newlocalDateTime = localDateTime.plusDays(1);
                    stringObjectHashMap.put("starttime", newlocalDateTime);


                }
                else
                {

                    Date nulltime =(Date) stringObjectHashMap.get("nulltime");
                    Instant instant = nulltime.toInstant();
                    ZoneId zoneId = ZoneId.systemDefault();
                    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zoneId);
                    LocalDateTime newlocalDateTime = localDateTime.plusDays(1);
                    stringObjectHashMap.put("starttime", newlocalDateTime);
                }


            }


        }
        long end = System.currentTimeMillis() - tarttime;
        System.out.println(end);
        System.out.println(stringObjectHashMap);
        // System.out.println(retrunzone);
        Date date = retrunzone.get(retrunzone.size() - 1).get(0).gettTime();
        System.out.println(dateTimeInstance.format(date));

    }

}
