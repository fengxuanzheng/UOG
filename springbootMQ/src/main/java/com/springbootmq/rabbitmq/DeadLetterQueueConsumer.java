package com.springbootmq.rabbitmq;

import com.rabbitmq.client.Channel;
import com.springbootmq.config.ConfirmConfig;
import com.springbootmq.config.TtlQueueConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
public class DeadLetterQueueConsumer {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @RabbitListener(queues = {"ttlQueue"})
    public void receiverTtl(Message message, Channel channel)
    {
        String msg=new String(message.getBody());
        log.info("当前时间：{},收到死信队列信息{}", LocalDateTime.now(), msg);
    }

    @RabbitListener(queues = {TtlQueueConfig.delayQueue})
    public void receiverDelay(Message message, Channel channel)
    {
        String msg=new String(message.getBody());
        log.info("当前时间：{},收到交换机延迟队列信息{}", LocalDateTime.now(), msg);
    }

    @RabbitListener(queues = {ConfirmConfig.confirmQueue})
    public void receiverConfirm(Message message, Channel channel)
    {
        String msg=new String(message.getBody());
        log.info("当前时间：{},收到确认队列信息{}", LocalDateTime.now(), msg);
    }

    @RabbitListener(queues = {ConfirmConfig.waringQueue})
    public void receiverConfirmForBackup(Message message, Channel channel)
    {
        String msg=new String(message.getBody());
        log.info("当前时间：{},收到由备份交换机转发队列信息{}", LocalDateTime.now(), msg);
    }

    //@RabbitListener(queues = {ConfirmConfig.priorityQueue})
    public void receiverpriority(Message message, Channel channel)
    {
        String msg=new String(message.getBody());
        log.info("当前时间：{},收到由优先队列信息{}", LocalDateTime.now(), msg);
    }
    public void receiverpriorityOfMannal()
    {
        while (true)
        {
            Object o = rabbitTemplate.receiveAndConvert(ConfirmConfig.priorityQueue, 5000);
            System.out.println(o);
            if (o==null)
            {
                break;
            }
        }
    }
}
