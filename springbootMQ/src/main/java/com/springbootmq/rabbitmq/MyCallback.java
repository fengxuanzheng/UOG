package com.springbootmq.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.SettableListenableFuture;

@Component
@Slf4j
public class MyCallback implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnsCallback {

    /*@Autowired
    private RabbitTemplate rabbitTemplate;

    @PostConstruct
    public void initRabbitTemplate()
    {
        rabbitTemplate.setConfirmCallback(this);
    }*/

    @Autowired
    public void initRabbitTemplate( RabbitTemplate rabbitTemplate)
    {
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnsCallback(this);
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        ReturnedMessage returned = correlationData.getReturned();
        SettableListenableFuture<CorrelationData.Confirm> future = correlationData.getFuture();
        if (ack)
        {
         log.info("消息发送成功,ID为{}",correlationData.getId());
        }
        else
        {
            log.info("消息发送失败,Id为{},原因:{}",correlationData.getId(),cause);
        }
    }

    @Override
    //只有不可达队列时候才返回,成功就不回调
    public void returnedMessage(ReturnedMessage returned) {
        log.info("消息发送失败,,原因:{},消息:{},路由KEY:{},路由名称:{}",returned.getReplyText(),returned.getMessage(),returned.getRoutingKey(),returned.getExchange());
    }


}
