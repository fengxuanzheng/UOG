package com.springbootmq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpoint;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.stereotype.Component;

import javax.jms.Topic;

@Component
public class MyJmsListenerContainerFactory implements JmsListenerContainerFactory {

    @Autowired
    @Qualifier("createTopic")
    private Topic topic;

    @Override
    public MessageListenerContainer createListenerContainer(JmsListenerEndpoint endpoint) {
        DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
        //defaultMessageListenerContainer.setConnectionFactory();
        defaultMessageListenerContainer.setDestination(topic);
        endpoint.setupListenerContainer(defaultMessageListenerContainer);
        return defaultMessageListenerContainer;
    }
}
