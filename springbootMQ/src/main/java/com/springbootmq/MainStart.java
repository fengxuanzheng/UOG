package com.springbootmq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.TextMessage;

@Component
public class MainStart {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    @Qualifier("myQueue")
    private Destination destination;

    @Autowired
    @Qualifier("createTopic")
    private Destination topic;

   //@Scheduled(fixedDelay = 3000)
   public void scholedMq()
    {
        System.out.println("定时发送");
        jmsMessagingTemplate.convertAndSend(destination,"自动发送消息");

    }

   // @JmsListener(destination = "spring-boot-queue")
    public void listenerMq(TextMessage textMessage) throws JMSException {
        String text = textMessage.getText();
        System.out.println("自动监听到消息:"+text);
    }

    //@Scheduled(fixedDelay = 3000)
    public void sendTopic()
    {
        jmsMessagingTemplate.convertAndSend(topic,"自定义队列");
    }

    //@JmsListener(destination = "spring-boot-topic")
    public void listenerTopic(TextMessage message) throws JMSException {
        String text = message.getText();
        System.out.println("自动监听主题:"+text);
    }
}
