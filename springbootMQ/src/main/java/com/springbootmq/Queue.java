package com.springbootmq;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Destination;

@Configuration
public class Queue {


    @Bean
    public Destination myQueue()
    {
       return new ActiveMQQueue("spring-boot-queue");
    }

}
