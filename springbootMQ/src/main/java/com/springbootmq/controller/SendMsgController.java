package com.springbootmq.controller;

import com.springbootmq.config.ConfirmConfig;
import com.springbootmq.config.TtlQueueConfig;
import com.springbootmq.rabbitmq.DeadLetterQueueConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/ttl")
public class SendMsgController {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private DeadLetterQueueConsumer deadLetterQueueConsumer;


    @GetMapping("/send/{myMessage}")
    public void getMessage(@PathVariable("myMessage") String sendMessage)
    {
        log.info("当前时间：{},发送一条信息给两个 TTL 队列:{}", LocalDateTime.now(), sendMessage);
        rabbitTemplate.convertAndSend(TtlQueueConfig.normalExchange,"normalA","延迟10S的消息队列"+sendMessage);
        rabbitTemplate.convertAndSend(TtlQueueConfig.normalExchange,"normalB","延迟40S的消息队列"+sendMessage);
    }
    @GetMapping("/send/{myMessage}/{a}")
    public void getMessage(@PathVariable("myMessage") String sendMessage,@PathVariable("a") String conut)
    {
        log.info("当前时间：{},发送一条信息给两个 TTL 队列:{},数量为{}", LocalDateTime.now(), sendMessage,conut);
        rabbitTemplate.convertAndSend(TtlQueueConfig.normalExchange,"normalC","延迟20S的消息队列"+sendMessage,msg->{
            MessageProperties messageProperties = msg.getMessageProperties();
            messageProperties.setExpiration("20000");
            return msg;});
        rabbitTemplate.convertAndSend(TtlQueueConfig.normalExchange,"normalC","延迟5S的消息队列"+sendMessage, msg->{
            MessageProperties messageProperties = msg.getMessageProperties();
            messageProperties.setExpiration("5000");
            return msg;}
        );

    }

    @GetMapping("/sendDelay/{myMessage}/{a}")
    public void sendMessage(@PathVariable("myMessage") String sendMessage,@PathVariable("a") String conut)
    {
        log.info("当前时间：{},发送一条信息给两个 TTL 队列:{},数量为{}", LocalDateTime.now(), sendMessage,conut);
        rabbitTemplate.convertAndSend(TtlQueueConfig.xDelayExchange,"delayQueueToDelay","交换机延迟20S的消息队列"+sendMessage,msg->{
            MessageProperties messageProperties = msg.getMessageProperties();
            messageProperties.setDelay(20000);
            return msg;});
        rabbitTemplate.convertAndSend(TtlQueueConfig.xDelayExchange,"delayQueueToDelay","交换机延迟5S的消息队列"+sendMessage, msg->{
            MessageProperties messageProperties = msg.getMessageProperties();
            messageProperties.setDelay(5000);
            return msg;}
        );

    }

    @GetMapping("/sendConfirm/{myMessage}")
    public void sendMessage(@PathVariable("myMessage") String sendMessage)
    {
        log.info("当前时间：{},发送一条信息给两个 TTL 队列:{}", LocalDateTime.now(), sendMessage);
        //高级发布确认核心配置,决定了回调接口源数据,如果不配就没有
        CorrelationData correlationData = new CorrelationData();
        correlationData.setId("1");
        //correlationData.setReturnedMessage(new Message(sendMessage.getBytes()));
        rabbitTemplate.convertAndSend(ConfirmConfig.confirmExchange,ConfirmConfig.confirmBindKey,sendMessage,correlationData);
        rabbitTemplate.convertAndSend(ConfirmConfig.confirmExchange,ConfirmConfig.confirmBindKey+1,sendMessage,correlationData);

    }

    @GetMapping("/sendpriority")
    public void sendpriorityMessage()
    {
        for (int i=0;i<10;i++)
        {
            String msg="优先队列消息:"+i;
            int finalI = i;
            rabbitTemplate.convertAndSend(ConfirmConfig.confirmExchange,ConfirmConfig.priorityBindKey,msg, message -> {
                MessageProperties messageProperties = message.getMessageProperties();
                if (finalI ==8)
                {
                    messageProperties.setPriority(9);
                }
                return message;
            });
        }
        deadLetterQueueConsumer.receiverpriorityOfMannal();
    }
}
