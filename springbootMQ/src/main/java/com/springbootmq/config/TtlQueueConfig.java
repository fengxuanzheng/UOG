package com.springbootmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class TtlQueueConfig {
    public static final String normalExchange="normalExchange";
    public static final String normalQueueA="normalQueueA";
    public static final String normalQueueB="normalQueueB";
    public static final String normalQueueC="normalQueueC";
    public static final String ttlExchange="ttlExchange";
    public static final String ttlQueue="ttlQueue";
    public static final String delayQueue="delayQueue";
    public static final String xDelayExchange="xDelayExchange";

    @Bean
    public DirectExchange normalExchange()
    {
      return   new DirectExchange(normalExchange);
    }
    @Bean
    public DirectExchange ttlExchange()
    {
        DirectExchange build = ExchangeBuilder.directExchange("1").build();
        return   new DirectExchange(ttlExchange);
    }

    @Bean
    public CustomExchange xDelayExchange()
    {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("x-delayed-type", "direct");
        return new CustomExchange(xDelayExchange,"x-delayed-message",true,false,stringObjectHashMap);
    }

    @Bean
    public Queue delayQueue()
    {
        return QueueBuilder.durable(delayQueue).build();
    }

    @Bean
    public Queue normalQueueA()
    {
        Map<String, Object> args = new HashMap<>(3);
        //声明当前队列绑定的死信交换机
        args.put("x-dead-letter-exchange", ttlExchange);
         //声明当前队列的死信路由 key
        args.put("x-dead-letter-routing-key", "ttl");
         //声明队列的 TTL
        args.put("x-message-ttl", 10000);
        return QueueBuilder.durable(normalQueueA).withArguments(args).build();

    }
    @Bean
    public Queue normalQueueB()
    {
        Map<String, Object> args = new HashMap<>(3);
        //声明当前队列绑定的死信交换机
        args.put("x-dead-letter-exchange", ttlExchange);
        //声明当前队列的死信路由 key
        args.put("x-dead-letter-routing-key", "ttl");
        //声明队列的 TTL
        args.put("x-message-ttl", 40000);
        return QueueBuilder.durable(normalQueueB).withArguments(args).build();

    }
    @Bean
    public Queue normalQueueC()
    {
        Map<String, Object> args = new HashMap<>(3);
        //声明当前队列绑定的死信交换机
        args.put("x-dead-letter-exchange", ttlExchange);
        //声明当前队列的死信路由 key
        args.put("x-dead-letter-routing-key", "ttl");
        return QueueBuilder.durable(normalQueueC).withArguments(args).build();

    }

    @Bean
    public Queue ttlQueue()
    {
        return QueueBuilder.durable(ttlQueue).build();
    }
    @Bean
    public Binding nqueueABindnExchange(@Qualifier("normalQueueA")Queue queue,@Qualifier("normalExchange") DirectExchange directExchange)
    {
        return BindingBuilder.bind(queue).to(directExchange).with("normalA");
    }
    @Bean
    public Binding nqueueBBindnExchange(@Qualifier("normalQueueB")Queue queue,@Qualifier("normalExchange") DirectExchange directExchange)
    {
        return BindingBuilder.bind(queue).to(directExchange).with("normalB");
    }
    @Bean
    public Binding nqueueCBindnExchange(@Qualifier("normalQueueC")Queue queue,@Qualifier("normalExchange") DirectExchange directExchange)
    {
        return BindingBuilder.bind(queue).to(directExchange).with("normalC");
    }
    @Bean
    public Binding tqueueBindtExchange(@Qualifier("ttlQueue")Queue queue,@Qualifier("ttlExchange") DirectExchange directExchange)
    {
        return BindingBuilder.bind(queue).to(directExchange).with("ttl");
    }

    @Bean
    public Binding delayqueueBindDelayExchange(@Qualifier("delayQueue")Queue queue, CustomExchange directExchange)
    {
        return BindingBuilder.bind(queue).to(directExchange).with("delayQueueToDelay").noargs();
    }
}
