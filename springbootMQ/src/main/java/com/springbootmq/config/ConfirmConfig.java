package com.springbootmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfirmConfig {
    public static final String confirmExchange="confirmExchange";
    public static final String backupExchange="backupExchange";
    public static final String confirmQueue="confirmQueue";
    public static final String waringQueue=" waringQueue";
    public static final String backupQueue="backupQueue";
    public static final String priorityQueue="priorityQueue";
    public static final String confirmBindKey="confirmToQueue";
    public static final String priorityBindKey="priorityToQueue";


    @Bean
    public DirectExchange confirmExchange()
    {
        return  ExchangeBuilder.directExchange(confirmExchange).durable(true).withArgument("alternate-exchange",backupExchange).build();
    }

    @Bean
    public FanoutExchange backupExchange()
    {
        return new FanoutExchange(backupExchange);
    }

    @Bean
    public Queue confirmQueue()
    {
        return QueueBuilder.durable(confirmQueue).build();
    }

    @Bean
    public Queue waringQueue()
    {
        return QueueBuilder.durable(waringQueue).build();
    }
    @Bean
    public Queue backupQueue()
    {
        return QueueBuilder.durable(backupQueue).build();
    }

    @Bean
    public Binding  confirmQBingConfirmE(@Qualifier("confirmExchange") DirectExchange directExchange,@Qualifier("confirmQueue") Queue queue)
    {
        return BindingBuilder.bind(queue).to(directExchange).with(confirmBindKey);
    }
    @Bean
    public Binding  priorityQBingConfirmE(@Qualifier("confirmExchange") DirectExchange directExchange,@Qualifier("priorityQueue") Queue queue)
    {
        return BindingBuilder.bind(queue).to(directExchange).with(priorityBindKey);
    }

    @Bean
    public Queue priorityQueue()
    {
        //定义优先级队列
        return QueueBuilder.durable(priorityQueue).withArgument("x-max-priority",10).build();
    }

    @Bean
    public Binding waringBingBackupEx(@Qualifier("backupExchange") FanoutExchange directExchange,@Qualifier("waringQueue") Queue queue)
    {
        return BindingBuilder.bind(queue).to(directExchange);
    }

    @Bean
    public Binding backupBingBackupEx(@Qualifier("backupExchange") FanoutExchange directExchange,@Qualifier("backupQueue") Queue queue)
    {
        return BindingBuilder.bind(queue).to(directExchange);
    }

}
