package com.springbootmq;

import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Topic;

@Configuration
public class MyTopic {

    @Bean
    public Topic createTopic()
    {
        return new ActiveMQTopic("spring-boot-topic");
    }
}
