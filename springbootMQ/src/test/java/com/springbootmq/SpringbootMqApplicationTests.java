package com.springbootmq;

import org.apache.activemq.command.ActiveMQQueue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;

@SpringBootTest
class SpringbootMqApplicationTests {
    @Autowired(required = false)
    @Qualifier("jmsMessagingTemplate")
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Test
    void contextLoads() {
        ActiveMQQueue activeMQQueue = new ActiveMQQueue("spring-boot-queue");
        jmsMessagingTemplate.convertAndSend(activeMQQueue,"springboot消息");

    }

}
